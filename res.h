/* SPDX-License-Identifier: CC-BY-4.0 */
/*
 * res.h
 *
 * Efficient management of resource bitmaps
 *
 * Copyright (c) 2020-2021 Alexander Zuepke
 * Creative Commons Attribution 4.0 International (CC-BY-4.0) license.
 *
 *
 * Resource bitmaps are arrays of machine-words (unsigned long).
 * You must specify the number of bits before including this files, e.g.:
 *   #define RES_BITS 64
 *   #include <res.h>
 *
 * azuepke, 2021-09-03: from locks.h
 * azuepke, 2021-10-19: CC-BY-4.0 license
 */

#ifndef __RES_H__
#define __RES_H__

#ifndef RES_BITS
#error Define RES_BITS before including
#endif

#define RES_BITS_PER_WORD (sizeof(unsigned long) * 8)
#define RES_NUM_WORDS ((RES_BITS + RES_BITS_PER_WORD - 1) / RES_BITS_PER_WORD)


/** bitmap of resources */
typedef unsigned long res_t[RES_NUM_WORDS];

/* a[pos] */
static inline int res_get(res_t a, unsigned long pos)
{
    unsigned long wrd = pos / RES_BITS_PER_WORD;
    unsigned long bit = pos % RES_BITS_PER_WORD;

    return (a[wrd] >> bit) & 0x1;
}

/* a[pos] = val */
static inline void res_set(res_t a, unsigned long pos, int val)
{
    unsigned long wrd = pos / RES_BITS_PER_WORD;
    unsigned long bit = pos % RES_BITS_PER_WORD;

    a[wrd] &= ~(1ul << bit);
    a[wrd] |= (val & 0x1ul) << bit;
}

/* a[pos] = 0 */
static inline void res_set_zero(res_t a, unsigned long pos)
{
    unsigned long wrd = pos / RES_BITS_PER_WORD;
    unsigned long bit = pos % RES_BITS_PER_WORD;

    a[wrd] &= ~(1ul << bit);
}

/* a[pos] = 1 */
static inline void res_set_one(res_t a, unsigned long pos)
{
    unsigned long wrd = pos / RES_BITS_PER_WORD;
    unsigned long bit = pos % RES_BITS_PER_WORD;

    a[wrd] |= 1ul << bit;
}

/* a = 000... */
static inline void res_clear(res_t a)
{
    for (unsigned long wrd = 0; wrd < RES_NUM_WORDS; wrd++) {
        a[wrd] = 0ul;
    }
}

/* a = 111... */
static inline void res_fill(res_t a)
{
    for (unsigned long wrd = 0; wrd < RES_NUM_WORDS; wrd++) {
        a[wrd] = ~0ul;
    }
}

/* a = b */
static inline void res_assign(res_t a, const res_t b)
{
    for (unsigned long wrd = 0; wrd < RES_NUM_WORDS; wrd++) {
        a[wrd] = b[wrd];
    }
}

/* a = (b | c) */
static inline void res_or_assign(res_t a, const res_t b, const res_t c)
{
    for (unsigned long wrd = 0; wrd < RES_NUM_WORDS; wrd++) {
        a[wrd] = b[wrd] | c[wrd];
    }
}

/* a |= b */
static inline void res_or(res_t a, const res_t b)
{
    for (unsigned long wrd = 0; wrd < RES_NUM_WORDS; wrd++) {
        a[wrd] |= b[wrd];
    }
}

/* a &= ~b */
static inline void res_bic(res_t a, const res_t b)
{
    for (unsigned long wrd = 0; wrd < RES_NUM_WORDS; wrd++) {
        a[wrd] &= ~b[wrd];
    }
}

/* (a & b) != 0 */
static inline int res_and_neqz(const res_t a, const res_t b)
{
    unsigned long bits = 0;
    for (unsigned long wrd = 0; wrd < RES_NUM_WORDS; wrd++) {
        bits |= a[wrd] & b[wrd];
    }
    return bits != 0;
}

/* a == 0 */
static inline int res_eqz(const res_t a)
{
    unsigned long bits = 0;
    for (unsigned long wrd = 0; wrd < RES_NUM_WORDS; wrd++) {
        bits |= a[wrd];
    }
    return bits == 0;
}

/* a == b */
static inline int res_eq(const res_t a, const res_t b)
{
    int eq = 1;
    for (unsigned long wrd = 0; wrd < RES_NUM_WORDS; wrd++) {
        eq &= (a[wrd] == b[wrd]);
    }
    return eq;
}


/* iterate over all resources in "res": "iter" will be the resource bit
 *
 * Both the outer and inner iterator must be used together:
 *
 *    res_for_each_word(res) {
 *        res_for_each_bit(res, i) {
 *            ... use i ...;
 *        }
 *    }
 *
 * Note: __builtin_ctzl() is a compiler builtin to find the last bit set;
 *       the value most not be 0
 */

#define res_for_each_word(res) \
    for (unsigned long _wrd = 0, _wrd_bits = (res)[0]; \
         _wrd < RES_NUM_WORDS; \
         _wrd++, _wrd_bits = (_wrd < RES_NUM_WORDS) ? (res)[_wrd] : 0 \
        )

#define res_for_each_bit(res, iter) \
    for (unsigned long _bit; \
         (_wrd_bits != 0) ? ( \
                             _bit = __builtin_ctzl(_wrd_bits), \
                             _wrd_bits &= ~1ul << _bit, \
                             (iter) = (_wrd * RES_BITS_PER_WORD) + _bit, \
                             1 \
                            ) : 0; \
        )

#endif
