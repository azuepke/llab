/* SPDX-License-Identifier: CC-BY-4.0 */
/*
 * locks.c
 *
 * Implementation of different multi-resource locks
 *
 * Unless attributed otherwise, the lock implementations are
 * Copyright (c) 2020-2021 Alexander Zuepke
 * Creative Commons Attribution 4.0 International (CC-BY-4.0) license
 *
 * MCS locks are from:
 * John M. Mellor-Crummey, Michael L. Scott:
 * "Algorithms for Scalable Synchronization on Shared-Memory Multiprocessors"
 * ACM Trans. Comput. Syst. 9(1), 21-65, 1991
 *
 * Brandenburg's phase-fair ticket-based RW-locks are from:
 * Björn B. Brandenburg, James H. Anderson:
 * "Spin-Based Reader-Writer Synchronization for Multiprocessor Real-Time Systems",
 * Real-Time Systems 46(1), 25-87, 2010
 *
 * Ward's multi-queue version of RNLP is from:
 * Bryan C. Ward, James H. Anderson:
 * "Fine-grained Multiprocessor Real-Time Locking with Improved Blocking",
 * Conference on Real-Time Networks and Systems (RTNS), 67-76, 2013
 *
 * Nemitz's single-queue version of RNLP is from:
 * Catherine E. Nemitz, Tanya Amert, James H. Anderson,
 * "Real-time multiprocessor locks with nesting: optimizing the common case",
 * International Conference on Real Time Networks and Systems (RTNS), 38--47, 2017
 *
 * RW-RNLP is from:
 * Bryan C. Ward, James H. Anderson:
 * "Multi-Resource Real-Time Reader/Writer Locks for Multiprocessors"
 * International Parallel and Distributed Processing Symposium (IPDPS), 177-186, 2014
 *
 * Nemitz and Ward's Uniform C-RNLP is from:
 * Catherine E. Jarrett, Bryan C. Ward, and James H. Anderson:
 * "A Contention-Sensitive Fine-Grained Locking Protocol
 *  for Multiprocessor Real-Time Systems"
 * In: International Conference on Real Time Networks and Systems (RTNS), 3-12, 2015
 *
 * MRLOCK is from:
 * Deli Zhang, Brendan Lynch, Damian Dechev
 * "Queue-Based and Adaptive Lock Algorithms for Scalable Resource Allocation
 *  on Shared-Memory Multiprocessors""
 * Int J Parallel Prog; DOI 10.1007/s10766-014-0317-6, 2014
 *
 *
 * azuepke, 2020-07-24: initial
 * azuepke, 2020-10-01: move lock implementation to own lib
 * azuepke, 2020-10-04: optimize for sparsely populated resource-bitmaps
 * azuepke, 2020-10-11: reader-write variants of SQDGL2/LLAB4
 * azuepke, 2020-10-16: implementation based on pure C11 atomics
 * azuepke, 2020-10-22: cleanup for paper
 * azuepke, 2020-12-30: update memory barriers in LLAB
 * azuepke, 2021-08-27: include C-RNLP
 * azuepke, 2021-09-03: refactor resource bitmaps
 * azuepke, 2021-10-19: CC-BY-4.0 license
 */

#include <stddef.h> /* for NULL */
#include <string.h> /* for memset() */
#include <assert.h>
#include "locks.h"

#pragma GCC diagnostic ignored "-Wdeclaration-after-statement"
//#pragma GCC diagnostic ignored "-Wunused-parameter"
//#pragma GCC diagnostic ignored "-Wunused-function"
//#pragma GCC diagnostic ignored "-Wunused-variable"

////////////////////////////////////////////////////////////////////////////////

/* GCC atomic builtins */
#define atomic_fetch_and_add_relaxed(a, v)  __atomic_fetch_add((a), (v), __ATOMIC_RELAXED)
#define atomic_fetch_and_or_relaxed(a, v)   __atomic_fetch_or((a), (v), __ATOMIC_RELAXED)

#define atomic_add_relaxed(a, v)            __atomic_add_fetch((a), (v), __ATOMIC_RELAXED)
#define atomic_sub_relaxed(a, v)            __atomic_sub_fetch((a), (v), __ATOMIC_RELAXED)

#define atomic_load_relaxed(a)              __atomic_load_n((a), __ATOMIC_RELAXED)
#define atomic_load_acquire(a)              __atomic_load_n((a), __ATOMIC_ACQUIRE)

#define atomic_store_relaxed(a, v)          __atomic_store_n((a), (v), __ATOMIC_RELAXED)
#define atomic_store_release(a, v)          __atomic_store_n((a), (v), __ATOMIC_RELEASE)

#define atomic_swap_acquire(a, v)           __atomic_exchange_n((a), (v), __ATOMIC_ACQUIRE)

#define atomic_or_fetch_relaxed(a, v)       __atomic_or_fetch((a), (v), __ATOMIC_RELAXED)

#define atomic_cas_relaxed(a, o, n) ({  \
    __typeof__(o) __tmp_o = (o);    \
    __atomic_compare_exchange_n((a), &__tmp_o, (n), 0, __ATOMIC_RELAXED, __ATOMIC_RELAXED); \
    })
#define atomic_cas_acquire(a, o, n) ({  \
    __typeof__(o) __tmp_o = (o);    \
    __atomic_compare_exchange_n((a), &__tmp_o, (n), 0, __ATOMIC_ACQUIRE, __ATOMIC_RELAXED); \
    })
#define atomic_cas_release(a, o, n) ({  \
    __typeof__(o) __tmp_o = (o);    \
    __atomic_compare_exchange_n((a), &__tmp_o, (n), 0, __ATOMIC_RELEASE, __ATOMIC_RELAXED); \
    })

#define atomic_xor_fetch_release(a, v)      __atomic_xor_fetch((a), (v), __ATOMIC_RELEASE)
#define atomic_fetch_and_add_acq_rel(a, v)  __atomic_fetch_add((a), (v), __ATOMIC_ACQ_REL)

/* memory barriers */
#define atomic_acquire_barrier()            __atomic_thread_fence(__ATOMIC_ACQUIRE)
#define atomic_release_barrier()            __atomic_thread_fence(__ATOMIC_RELEASE)

#define atomic_load_barrier()               __atomic_thread_fence(__ATOMIC_ACQUIRE)
#define atomic_store_barrier()              __atomic_thread_fence(__ATOMIC_RELEASE)

/* relax core on hyperthreading systems */
#if defined(__i386__) || defined(__x86_64__)
#define atomic_relax()      __asm__ volatile("pause" : : : "memory")
#elif defined(__arm__) || defined(__aarch64__)
#define atomic_relax()      __asm__ volatile("yield" : : : "memory")
#elif defined(__powerpc__) || defined(__powerpc64__)
#define atomic_relax()      __asm__ volatile("or 27, 27, 27" : : : "memory")
#else
#define atomic_relax()      __asm__ volatile("" : : : "memory")
#endif

/* GCC optimizer hint */
#define unlikely(cond)              __builtin_expect((cond) != 0, 0)


#define await(cond) do { \
        while (!(cond)) { \
            /* idle hyperthread */ \
            atomic_relax(); \
        } \
    } while (0)

////////////////////////////////////////////////////////////////////////////////

/// single-resource TAS and TATAS locks using various implementation tricks

/** initialize lock */
void sspin_init(sspin_lock_t *l)
{
    assert(l != NULL);
    *l = 0;
}

/** lock */
void sspin_lock_tas(sspin_lock_t *l)
{
    char *c = (char *)l;

    assert(l != NULL);

again:
    if (unlikely(__atomic_test_and_set(c, __ATOMIC_ACQUIRE) == 1)) {
        /* TAS failed */
        /* idle hyperthread */
        atomic_relax();
        goto again;
    }
    /* implicit acquire barrier */
}

/** lock */
void sspin_lock_swap(sspin_lock_t *l)
{
    assert(l != NULL);

again:
    if (unlikely(atomic_swap_acquire(l, 1) == 1)) {
        /* SWAP failed */
        /* idle hyperthread */
        atomic_relax();
        goto again;
    }
    /* implicit acquire barrier */
}

/** lock */
void sspin_lock_cas(sspin_lock_t *l)
{
    assert(l != NULL);

again:
    if (unlikely(!atomic_cas_acquire(l, 0, 1))) {
        /* CAS failed */
        /* idle hyperthread */
        atomic_relax();
        goto again;
    }
    /* implicit acquire barrier */
}

/** lock */
void sspin_lock_test_tas(sspin_lock_t *l)
{
    char *c = (char *)l;

    assert(l != NULL);

again:
    if (unlikely(atomic_load_relaxed(l) != 0)) {
        /* idle hyperthread */
        atomic_relax();
        goto again;
    }

    if (unlikely(__atomic_test_and_set(c, __ATOMIC_ACQUIRE) == 1)) {
        /* TAS failed */
        /* idle hyperthread */
        atomic_relax();
        goto again;
    }
    /* implicit acquire barrier */
}

/** lock */
void sspin_lock_test_swap(sspin_lock_t *l)
{
    assert(l != NULL);

again:
    if (unlikely(atomic_load_relaxed(l) != 0)) {
        /* idle hyperthread */
        atomic_relax();
        goto again;
    }

    if (unlikely(atomic_swap_acquire(l, 1) == 1)) {
        /* SWAP failed */
        goto again;
    }
    /* implicit acquire barrier */

}

/** lock */
void sspin_lock_test_cas(sspin_lock_t *l)
{
    assert(l != NULL);

again:
    if (unlikely(atomic_load_relaxed(l) != 0)) {
        /* idle hyperthread */
        atomic_relax();
        goto again;
    }

    if (unlikely(!atomic_cas_acquire(l, 0, 1))) {
        /* CAS failed */
        goto again;
    }
    /* implicit acquire barrier */

}

/** unlock */
void sspin_unlock(sspin_lock_t *l)
{
    assert(l != NULL);
    atomic_store_release(l, 0);
}

////////////////////////////////////////////////////////////////////////////////

/// ticket spin locks

static inline void _ticket_spin_init(ticket_spin_t *lock)
{
    assert(lock != NULL);

    lock->lock = 0;
    /* no barrier here -- caller is responsible for sync before first use */
}

#define TICKET_SPIN_INC 0x10000

static inline void _ticket_spin_lock(ticket_spin_t *lock)
{
    ticket_spin_t val;

    assert(lock != NULL);

    val.lock = atomic_fetch_and_add_relaxed(&lock->lock, TICKET_SPIN_INC);

    while (val.s.ticket != val.s.served) {
        atomic_relax();
        val.s.served = atomic_load_relaxed(&lock->s.served);
    }

    atomic_acquire_barrier();
}

static inline void _ticket_spin_unlock(ticket_spin_t *lock)
{
    assert(lock != NULL);

    atomic_store_release(&lock->s.served, lock->s.served + 1);
}

/* exported API */
void ticket_spin_init(ticket_spin_t *lock)
{
    _ticket_spin_init(lock);
}

void ticket_spin_lock(ticket_spin_t *lock)
{
    _ticket_spin_lock(lock);
}

void ticket_spin_unlock(ticket_spin_t *lock)
{
    _ticket_spin_unlock(lock);
}

////////////////////////////////////////////////////////////////////////////////

/// MCS locks
///
/// John M. Mellor-Crummey, Michael L. Scott:
/// "Algorithms for Scalable Synchronization on Shared-Memory Multiprocessors"
/// ACM Trans. Comput. Syst. 9(1), 21-65, 1991

static inline void _mcs_init(mcs_lock_t *lock)
{
    assert(lock != NULL);

    lock->tail = NULL;
}

static inline void _mcs_lock(mcs_lock_t *lock, mcs_node_t *self)
{
    mcs_node_t *prev;

    assert(lock != NULL);
    assert(self != NULL);

    self->next = NULL;
    self->locked = 1;

    prev = atomic_swap_acquire(&lock->tail, self);

    if (prev != NULL) {
        /* lock was not free */
        atomic_store_relaxed(&prev->next, self);
        while (atomic_load_acquire(&self->locked) != 0) {
            /* idle hyperthread */
            atomic_relax();
        }
    }
}

static inline void _mcs_unlock(mcs_lock_t *lock, mcs_node_t *self)
{
    mcs_node_t *next;

    assert(lock != NULL);
    assert(self != NULL);

    next = atomic_load_relaxed(&self->next);
    if (next == NULL) {
        /* no known successor */
        if (atomic_cas_release(&lock->tail, self, NULL)) {
            return;
        }
        /* wait for successor */
        while ((next = atomic_load_relaxed(&self->next)) == NULL) {
            /* idle hyperthread */
            atomic_relax();
        }
    }

    atomic_store_release(&next->locked, 0);
}

/* exported API */
void mcs_init(mcs_lock_t *lock)
{
    _mcs_init(lock);
}

void mcs_lock(mcs_lock_t *lock, mcs_node_t *self)
{
    _mcs_lock(lock, self);
}

void mcs_unlock(mcs_lock_t *lock, mcs_node_t *self)
{
    _mcs_unlock(lock, self);
}

////////////////////////////////////////////////////////////////////////////////

/// Spinning barrier -- all CPUs have to reach the barrier

/** initialize barrier for "num_cpus" parallel CPUs */
void spin_barrier_init(spin_barrier_t *barrier, uint32_t num_cpus)
{
    assert(barrier != NULL);
    assert(num_cpus >= 1);
    assert(num_cpus <= sizeof(long)*8);

    barrier->cpu_mask = (((1ul << (num_cpus - 1)) - 1) << 1) | 1ul;
    barrier->spin_mask = 0;
    /* no barrier here -- caller is responsible for sync before first use */
}

/** wait for all CPUs to reach the barrier, returns 1 on the last CPU */
int spin_barrier_wait(spin_barrier_t *barrier, uint32_t curr_cpu)
{
    unsigned long my_bit;
    unsigned long result;

    assert(barrier != NULL);
    assert(barrier->cpu_mask != 0);
    assert((barrier->cpu_mask & (barrier->cpu_mask + 1)) == 0);

    my_bit = 1ul << curr_cpu;
    assert((barrier->cpu_mask & my_bit) != 0);
    assert((barrier->spin_mask & my_bit) == 0);

    result = atomic_or_fetch_relaxed(&barrier->spin_mask, my_bit);
    if (result == barrier->cpu_mask) {
        atomic_store_relaxed(&barrier->spin_mask, 0);
        return 1;
    }
    while ((atomic_load_relaxed(&barrier->spin_mask) & my_bit) != 0) {
        /* idle hyperthread */
        atomic_relax();
    }
    return 0;
}

////////////////////////////////////////////////////////////////////////////////

/// Brandenburg's phase-fair ticket-based RW-locks, ticket based (PF-T)
///
/// Björn B. Brandenburg, James H. Anderson:
/// "Spin-Based Reader-Writer Synchronization for Multiprocessor Real-Time Systems",
/// Real-Time Systems 46(1), 25-87, 2010
/// https://people.mpi-sws.org/~bbb/papers/pdf/rtsj11.pdf

/* Lock structure:
 * - upper  8 bits:  R_out | guard
 * - mid-up 8 bits:  R_in  | guard
 * - mid-lo 8 bits:  W_in  | guard
 * - lower  8 bits:  W_out | PRES bit -- the LSB of W_out is the PHID bit
 */

#define PFT_RINC        0x100   // reader increment
#define PFT_PRES        0x1     // "present" bit
#define PFT_PHID        0x2     // "phase ID" bit -- LSB of W_out
#define PFT_WBITS       0x3     // mask for PRES and PHID

static inline void _pft_init(pft_lock_t *l)
{
    assert(l != NULL);

    l->rin = 0;
    l->rout = 0;
    l->win = 0;
    l->wout = 0;
    /* no barrier here -- caller is responsible for sync before first use */
}

static inline void _pft_read_lock(pft_lock_t *l)
{
    uint32_t w;

    assert(l != NULL);

    w = atomic_fetch_and_add_relaxed(&l->rin, PFT_RINC) & PFT_WBITS;
    await ((w == 0) || (w != (atomic_load_relaxed(&l->rin) & PFT_WBITS)));

    atomic_acquire_barrier();
}

static inline void _pft_read_unlock(pft_lock_t *l)
{
    assert(l != NULL);

    // no modification, no release barrier
    atomic_add_relaxed(&l->rout, PFT_RINC);
}

static inline void _pft_write_lock(pft_lock_t *l)
{
    uint32_t ticket, w;

    assert(l != NULL);

    ticket = atomic_fetch_and_add_relaxed(&l->win, 1);
    await (ticket == atomic_load_relaxed(&l->wout));
    w = PFT_PRES | (ticket & PFT_PHID);
    ticket = atomic_fetch_and_add_relaxed(&l->rin, w);
    await (ticket == atomic_load_relaxed(&l->rout));

    atomic_acquire_barrier();
}

static inline void _pft_write_unlock(pft_lock_t *l)
{
    assert(l != NULL);

    atomic_release_barrier();
    atomic_store_relaxed(&l->rin_lsb, 0);
    atomic_add_relaxed(&l->wout, 1);
}

/* exported API */
void pft_init(pft_lock_t *l)
{
    _pft_init(l);
}

void pft_read_lock(pft_lock_t *l)
{
    _pft_read_lock(l);
}

void pft_read_unlock(pft_lock_t *l)
{
    _pft_read_unlock(l);
}

void pft_write_lock(pft_lock_t *l)
{
    _pft_write_lock(l);
}

void pft_write_unlock(pft_lock_t *l)
{
    _pft_write_unlock(l);
}

////////////////////////////////////////////////////////////////////////////////

/// Brandenburg's phase-fair ticket-based RW-locks, compact version (PF-C)
///
/// Björn B. Brandenburg, James H. Anderson:
/// "Spin-Based Reader-Writer Synchronization for Multiprocessor Real-Time Systems",
/// Real-Time Systems 46(1), 25-87, 2010
/// https://people.mpi-sws.org/~bbb/papers/pdf/rtsj11.pdf

/* Lock structure:
 * - upper  8 bits:  R_out | guard
 * - mid-up 8 bits:  R_in  | guard
 * - mid-lo 8 bits:  W_in  | guard
 * - lower  8 bits:  W_out | PRES bit -- the LSB of W_out is the PHID bit
 */

#define PFC_ROUT_SHIFT  25      // least-significant bits of R_out
#define PFC_RIN_SHIFT   17      // least-significant bits of R_in
#define PFC_WIN_SHIFT   9       // least-significant bits of W_in
#define PFC_WOUT_SHIFT  1       // least-significant bits of W_out
#define PFC_MASK        0x7f    // mask for the bits
#define PFC_GUARD       0x80    // mask for the overflow guard bit
#define PFC_PRES        0x1     // "present" bit
#define PFC_PHID        0x2     // "phase ID" bit -- LSB of W_out
#define PFC_WBITS       0x3     // mask for PRES and PHID

#define PFC_ROUT_INC    (1 << PFC_ROUT_SHIFT)
#define PFC_RIN_INC     (1 << PFC_RIN_SHIFT)
#define PFC_WIN_INC     (1 << PFC_WIN_SHIFT)
#define PFC_WOUT_INC    (1 << PFC_WOUT_SHIFT)

static inline void _pfc_init(pfc_lock_t *l)
{
    assert(l != NULL);

    *l = 0;
    /* no barrier here -- caller is responsible for sync before first use */
}

static inline void _pfc_read_lock(pfc_lock_t *l)
{
    uint32_t ticket, w;

    assert(l != NULL);

    ticket = atomic_fetch_and_add_relaxed(l, PFC_RIN_INC);
    if (unlikely(((ticket >> PFC_RIN_SHIFT) & PFC_MASK) == PFC_MASK)) {
        /* counter overflow */
        atomic_sub_relaxed(l, PFC_GUARD << PFC_RIN_SHIFT);
    }
    w = ticket & PFC_WBITS;
    await (((w & PFC_PRES) == 0) || (w != (atomic_load_relaxed(l) & PFC_WBITS)));

    atomic_acquire_barrier();
}

static inline void _pfc_read_unlock(pfc_lock_t *l)
{
    assert(l != NULL);

    atomic_release_barrier();
    atomic_add_relaxed(l, PFC_ROUT_INC);
    /* overflow beyond width of uint32 */
}

static inline void _pfc_write_lock(pfc_lock_t *l)
{
    uint32_t ticket;
    assert(l != NULL);

    ticket = atomic_fetch_and_add_relaxed(l, PFC_WIN_INC);
    ticket = (ticket >> PFC_WIN_SHIFT) & PFC_MASK;
    if (unlikely(ticket == PFC_MASK)) {
        /* counter overflow */
        atomic_sub_relaxed(l, PFC_GUARD << PFC_WIN_SHIFT);
    }
    await (ticket == ((atomic_load_relaxed(l) >> PFC_WOUT_SHIFT) & PFC_MASK));
    ticket = atomic_fetch_and_add_relaxed(l, 1);
    ticket = (ticket >> PFC_RIN_SHIFT) & PFC_MASK;
    await (ticket == ((atomic_load_relaxed(l) >> PFC_ROUT_SHIFT) & PFC_MASK));

    atomic_acquire_barrier();
}

static inline void _pfc_write_unlock(pfc_lock_t *l)
{
    assert(l != NULL);

    atomic_release_barrier();

    if (unlikely(((atomic_load_relaxed(l) >> PFC_WOUT_SHIFT) & PFC_MASK) == PFC_MASK)) {
        atomic_sub_relaxed(l, (PFC_GUARD << PFC_WOUT_SHIFT) - 1);
    } else {
        atomic_add_relaxed(l, 1);
    }
}

/* exported API */
void pfc_init(pfc_lock_t *l)
{
    _pfc_init(l);
}

void pfc_read_lock(pfc_lock_t *l)
{
    _pfc_read_lock(l);
}

void pfc_read_unlock(pfc_lock_t *l)
{
    _pfc_read_unlock(l);
}

void pfc_write_lock(pfc_lock_t *l)
{
    _pfc_write_lock(l);
}

void pfc_write_unlock(pfc_lock_t *l)
{
    _pfc_write_unlock(l);
}

////////////////////////////////////////////////////////////////////////////////

/// SQDGL -- single queue dynamic group locks usign an internal ticket lock

/** initialize lock */
void sqdgl1_init(sqdgl1_lock_t *l)
{
    assert(l != NULL);

    l->tail = NULL;
    _ticket_spin_init(&l->lock);
}

/** lock resources "r" */
void sqdgl1_lock(sqdgl1_lock_t *l, uint32_t cpu, const res_t resources)
{
    sqdgl1_node_t *s;
    sqdgl1_node_t *n;

    assert(l != NULL);
    assert(cpu < MAX_CPUS);

    s = &l->c[cpu];
    s->conflicts = 0;
    res_assign(s->resources, resources);

    _ticket_spin_lock(&l->lock);

    /* insert at tail */
    s->next = l->tail;
    l->tail = s;

    /* scan older nodes for conflicts */
    n = s->next;
    while (n != NULL) {
        if (res_and_neqz(n->resources, s->resources) != 0) {
            ++s->conflicts;
        }

        n = n->next;
    }

    _ticket_spin_unlock(&l->lock);

    /* spin ... */
    while (atomic_load_acquire(&s->conflicts) != 0) {
        /* idle hyperthread */
        atomic_relax();
    }
}

/** unlock */
void sqdgl1_unlock(sqdgl1_lock_t *l, uint32_t cpu)
{
    sqdgl1_node_t *s;
    sqdgl1_node_t **np;
    sqdgl1_node_t *n;

    assert(l != NULL);
    assert(cpu < MAX_CPUS);

    s = &l->c[cpu];
    assert(s->conflicts == 0);

    atomic_release_barrier();

    _ticket_spin_lock(&l->lock);

    /* tell newer nodes that conflicts are resolved */
    np = &l->tail;
    n = *np;
    while (n != s) {
        if (res_and_neqz(n->resources, s->resources) != 0) {
              --n->conflicts;
        }

        np = &n->next;
        n = *np;
    }

    /* unlink from queue */
    *np = s->next;

    _ticket_spin_unlock(&l->lock);
}

////////////////////////////////////////////////////////////////////////////////

/// SQDGL -- single queue dynamic group locks usign an internal MCS lock

/** initialize lock */
void sqdgl2_init(sqdgl2_lock_t *l)
{
    assert(l != NULL);

    l->tail = NULL;
    _mcs_init(&l->lock);
}

/** lock resources "r" */
void sqdgl2_lock(sqdgl2_lock_t *l, uint32_t cpu, const res_t resources)
{
    mcs_node_t lock_node;
    sqdgl2_node_t *s;
    sqdgl2_node_t *n;

    assert(l != NULL);
    assert(cpu < MAX_CPUS);

    s = &l->c[cpu];
    s->conflicts = 0;
    res_assign(s->resources, resources);

    _mcs_lock(&l->lock, &lock_node);

    /* insert at tail */
    s->next = l->tail;
    l->tail = s;

    /* scan older nodes for conflicts */
    n = s->next;
    while (n != NULL) {
        if (res_and_neqz(n->resources, s->resources) != 0) {
            ++s->conflicts;
        }

        n = n->next;
    }

    _mcs_unlock(&l->lock, &lock_node);

    /* spin ... */
    while (atomic_load_acquire(&s->conflicts) != 0) {
        /* idle hyperthread */
        atomic_relax();
    }
}

/** unlock */
void sqdgl2_unlock(sqdgl2_lock_t *l, uint32_t cpu)
{
    mcs_node_t lock_node;
    sqdgl2_node_t *s;
    sqdgl2_node_t **np;
    sqdgl2_node_t *n;

    assert(l != NULL);
    assert(cpu < MAX_CPUS);

    s = &l->c[cpu];
    assert(s->conflicts == 0);

    atomic_release_barrier();

    _mcs_lock(&l->lock, &lock_node);

    /* tell newer nodes that conflicts are resolved */
    np = &l->tail;
    n = *np;
    while (n != s) {
        if (res_and_neqz(n->resources, s->resources) != 0) {
              --n->conflicts;
        }

        np = &n->next;
        n = *np;
    }

    /* unlink from queue */
    *np = s->next;

    _mcs_unlock(&l->lock, &lock_node);
}

////////////////////////////////////////////////////////////////////////////////

/// RW-SQDGL -- single queue group lock protocol (reader-writer variant)

/** initialize lock */
void rwsqdgl1_init(rwsqdgl1_lock_t *l)
{
    assert(l != NULL);

    l->tail = NULL;
    _ticket_spin_init(&l->lock);
}

/** lock resources "r" for reading and "w" for writing */
void rwsqdgl1_lock(rwsqdgl1_lock_t *l, uint32_t cpu, const res_t r, const res_t w)
{
    rwsqdgl1_node_t *s;
    rwsqdgl1_node_t *n;

    assert(l != NULL);
    assert(cpu < MAX_CPUS);

    s = &l->c[cpu];
    s->conflicts = 0;
    /* assign resources: write requests are implicit read requests, too */
    res_or_assign(s->rw_resources, r, w);
    res_assign(s->w_resources, w);

    _ticket_spin_lock(&l->lock);

    /* insert at tail */
    s->next = l->tail;
    l->tail = s;

    /* scan older nodes for conflicts */
    n = s->next;
    while (n != NULL) {
        if ((res_and_neqz(r, n->w_resources) != 0) ||
            (res_and_neqz(w, n->rw_resources) != 0)) {
            ++s->conflicts;
        }

        n = n->next;
    }

    _ticket_spin_unlock(&l->lock);

    /* spin ... */
    while (atomic_load_acquire(&s->conflicts) != 0) {
        /* idle hyperthread */
        atomic_relax();
    }
}


/** unlock */
void rwsqdgl1_unlock(rwsqdgl1_lock_t *l, uint32_t cpu, const res_t r, const res_t w)
{
    rwsqdgl1_node_t *s;
    rwsqdgl1_node_t **np;
    rwsqdgl1_node_t *n;

    assert(l != NULL);
    assert(cpu < MAX_CPUS);

    s = &l->c[cpu];
    assert(s->conflicts == 0);

    atomic_release_barrier();

    _ticket_spin_lock(&l->lock);

    /* tell newer nodes that conflicts are resolved */
    np = &l->tail;
    n = *np;
    while (n != s) {
        if ((res_and_neqz(r, n->w_resources) != 0) ||
            (res_and_neqz(w, n->rw_resources) != 0)) {
              --n->conflicts;
        }

        np = &n->next;
        n = *np;
    }

    /* unlink from queue */
    *np = s->next;

    _ticket_spin_unlock(&l->lock);
}

////////////////////////////////////////////////////////////////////////////////

/// RW-SQDGL -- ... with MCS lock

/** initialize lock */
void rwsqdgl2_init(rwsqdgl2_lock_t *l)
{
    assert(l != NULL);

    l->tail = NULL;
    _mcs_init(&l->lock);
}

/** lock resources "r" for reading and "w" for writing */
void rwsqdgl2_lock(rwsqdgl2_lock_t *l, uint32_t cpu, const res_t r, const res_t w)
{
    mcs_node_t lock_node;
    rwsqdgl2_node_t *s;
    rwsqdgl2_node_t *n;

    assert(l != NULL);
    assert(cpu < MAX_CPUS);

    s = &l->c[cpu];
    s->conflicts = 0;
    /* assign resources: write requests are implicit read requests, too */
    res_or_assign(s->rw_resources, r, w);
    res_assign(s->w_resources, w);

    _mcs_lock(&l->lock, &lock_node);

    /* insert at tail */
    s->next = l->tail;
    l->tail = s;

    /* scan older nodes for conflicts */
    n = s->next;
    while (n != NULL) {
        if ((res_and_neqz(r, n->w_resources) != 0) ||
            (res_and_neqz(w, n->rw_resources) != 0)) {
            ++s->conflicts;
        }

        n = n->next;
    }

    _mcs_unlock(&l->lock, &lock_node);

    /* spin ... */
    while (atomic_load_acquire(&s->conflicts) != 0) {
        /* idle hyperthread */
        atomic_relax();
    }
}


/** unlock */
void rwsqdgl2_unlock(rwsqdgl2_lock_t *l, uint32_t cpu, const res_t r, const res_t w)
{
    mcs_node_t lock_node;
    rwsqdgl2_node_t *s;
    rwsqdgl2_node_t **np;
    rwsqdgl2_node_t *n;

    assert(l != NULL);
    assert(cpu < MAX_CPUS);

    s = &l->c[cpu];
    assert(s->conflicts == 0);

    atomic_release_barrier();

    _mcs_lock(&l->lock, &lock_node);

    /* tell newer nodes that conflicts are resolved */
    np = &l->tail;
    n = *np;
    while (n != s) {
        if ((res_and_neqz(r, n->w_resources) != 0) ||
            (res_and_neqz(w, n->rw_resources) != 0)) {
              --n->conflicts;
        }

        np = &n->next;
        n = *np;
    }

    /* unlink from queue */
    *np = s->next;

    _mcs_unlock(&l->lock, &lock_node);
}

////////////////////////////////////////////////////////////////////////////////

/// multi-resource TATAS

/** initialize lock */
void tatas_init(tatas_lock_t *l)
{
    assert(l != NULL);

    *l = 0;
}

/** lock resources "r" */
void tatas_lock(tatas_lock_t *l, tatas_res_t r)
{
    tatas_lock_t val;

    assert(l != NULL);

again:
    val = atomic_load_relaxed(l);
    if ((val & r) != 0) {
        /* idle hyperthread */
        atomic_relax();
        goto again;
    }

    if (unlikely(!atomic_cas_acquire(l, val, val | r))) {
        /* CAS failed */
        goto again;
    }
    /* implicit acquire barrier */
}

/** unlock */
void tatas_unlock(tatas_lock_t *l, tatas_res_t r)
{
    assert(l != NULL);

    /* implicit release barrier */

    /* clear the bits -- we use XOR, as the C atomics API does not have BIC */
    atomic_xor_fetch_release(l, r);
}

////////////////////////////////////////////////////////////////////////////////

/// array of spinlocks, uint32_t array for resources (compact memory layout)

/** initialize lock */
void array1_init(array1_lock_t *l)
{
    assert(l != NULL);

    for (size_t i = 0; i < RES_BITS; i++) {
        l->a[i].ticket = 0;
        l->a[i].served = 0;
    }
}

/** lock resources "r" */
void array1_lock(array1_lock_t *l, const res_t r)
{
    assert(l != NULL);
    assert(r != NULL);

    res_for_each_word(r) {
        size_t i;
        res_for_each_bit(r, i) {
            uint32_t ticket = atomic_fetch_and_add_relaxed(&l->a[i].ticket, 1);
            while (ticket != atomic_load_relaxed(&l->a[i].served)) {
                /* idle hyperthread */
                atomic_relax();
            }
        }
    }

    atomic_acquire_barrier();
}

/** unlock */
void array1_unlock(array1_lock_t *l, const res_t r)
{
    assert(l != NULL);
    assert(r != NULL);

    atomic_release_barrier();

    res_for_each_word(r) {
        size_t i;
        res_for_each_bit(r, i) {
            l->a[i].served += 1;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////

/// array of spinlocks, uint32_t array for resources (one cacheline per lock)

/** initialize lock */
void array2_init(array2_lock_t *l)
{
    assert(l != NULL);

    for (size_t i = 0; i < RES_BITS; i++) {
        l->a[i].ticket = 0;
        l->a[i].served = 0;
    }
}

/** lock resources "r" */
void array2_lock(array2_lock_t *l, const res_t r)
{
    assert(l != NULL);
    assert(r != NULL);

    res_for_each_word(r) {
        size_t i;
        res_for_each_bit(r, i) {
            uint32_t ticket = atomic_fetch_and_add_relaxed(&l->a[i].ticket, 1);
            while (ticket != atomic_load_relaxed(&l->a[i].served)) {
                /* idle hyperthread */
                atomic_relax();
            }
        }
    }

    atomic_acquire_barrier();
}

/** unlock */
void array2_unlock(array2_lock_t *l, const res_t r)
{
    assert(l != NULL);
    assert(r != NULL);

    atomic_release_barrier();

    res_for_each_word(r) {
        size_t i;
        res_for_each_bit(r, i) {
            l->a[i].served += 1;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////

/// lock-less array-based multi-resource lock (LLAB-MRLOCK)
/// NOTE: fifth variant of the protocol using load-acquire/store-release

/** initialize lock */
void llab5_init(llab5_lock_t *l, uint32_t num_cpus)
{
    assert(l != NULL);
    assert(num_cpus <= MAX_CPUS);

    memset(l, 0, sizeof(*l));
    l->num_cpus = num_cpus;
}

/** lock resources "r" */
void llab5_lock(llab5_lock_t *l, uint32_t cpu, const res_t r)
{
    llab5_node_t *self, *c, *end;
    uint32_t self_ticket, c_ticket;

    assert(l != NULL);
    assert(cpu < l->num_cpus);

    /* register our resource request */
    self = &l->c[cpu];
    res_assign(self->resources, r);

    /* Set our own ticket to 0x1 ("in preparation").
     * Other CPUs will spin when they observe the LSB is set.
     * The release barrier below orders this store before the FAA.
     */
    atomic_store_relaxed(&self->drawn_ticket, 0x1);

    /* draw a unique ticket:
     * Writing to our own ticket must be visible before the FAA below completes,
     * i.e. when the store-part of the FAA happens.
     * We use FAA with both acquire and release semantics.
     */
    self_ticket = atomic_fetch_and_add_acq_rel(&l->global_ticket, 0x4);

    /* Save the ticket + 2 in our array entry.
     * This makes our entry a valid entry to the other CPUs.
     * The acquire barrier above prevents the store to move before the FAA.
     */
    atomic_store_relaxed(&self->drawn_ticket, self_ticket + 0x2);

    /* iterate all CPUs in linear order (any order will do) */
    end = &l->c[l->num_cpus];
    for (c = &l->c[0]; c < end; c++) {
        if (c == self) {
            continue; /* skip self */
        }

again:
        /* Check the ticket value and spin while a ticket is in preparation.
         * A relaxed load is sufficient before accessing the resources;
         * ordering is ensures by the FAA above.
         */
        c_ticket = atomic_load_relaxed(&c->drawn_ticket);

        if ((c_ticket & 0x1) != 0x0) {
            /* request in preparation; spin */
            /* idle hyperthread */
            atomic_relax();
            goto again;
        }

        if ((c_ticket & 0x2) == 0x0) {
            continue; /* skip if not active */
        }
        if ((int32_t)(c_ticket - self_ticket) > 0) {
            continue; /* skip if newer (wrap-around safe) */
        }

        /* check for resource request conflicts */
        if (res_and_neqz(c->resources, r) == 0) {
            continue; /* skip if no conflicts */
        }

        /* Local spinning until the ticket changes:
         * This catches any intermediate changes,
         * even if the CPU has updated its resources.
         * Relaxed spinning is OK.
         *
         * Note that using a load_acquire() here is not sufficient to replace
         * the atomic_acquire_barrier() below, as other CPUs might release the shared
         * resources briefly before our check of the ticket state above.
         */
        while (atomic_load_relaxed(&c->drawn_ticket) == c_ticket) {
            /* idle hyperthread */
            atomic_relax();
        }
    }

    /* final acquire barrier before accessing the shared resources */
    atomic_acquire_barrier();
}

/** unlock */
void llab5_unlock(llab5_lock_t *l, uint32_t cpu)
{
    llab5_node_t *self;

    assert(l != NULL);
    assert(cpu < l->num_cpus);

    self = &l->c[cpu];

    /* clear the active bit (we simply set the ticket to zero) */
    atomic_store_release(&self->drawn_ticket, 0);
}

////////////////////////////////////////////////////////////////////////////////

/// lock-less array-based multi-resource lock (LLAB-MRLOCK) + compact layout
/// NOTE: sixth variant of the protocol using load-acquire/store-release

/** initialize lock */
void llab6_init(llab6_lock_t *l, uint32_t num_cpus)
{
    assert(l != NULL);
    assert(num_cpus <= MAX_CPUS);

    memset(l, 0, sizeof(*l));
    l->num_cpus = num_cpus;
}

/** lock resources "r" */
void llab6_lock(llab6_lock_t *l, uint32_t cpu, const res_t r)
{
    llab6_node_t *self, *c, *end;
    uint32_t self_ticket, c_ticket;

    assert(l != NULL);
    assert(cpu < l->num_cpus);

    /* register our resource request */
    self = &l->c[cpu];
    res_assign(self->resources, r);

    /* Set our own ticket to 0x1 ("in preparation").
     * Other CPUs will spin when they observe the LSB is set.
     * The release barrier below orders this store before the FAA.
     */
    atomic_store_relaxed(&self->drawn_ticket, 0x1);

    /* draw a unique ticket:
     * Writing to our own ticket must be visible before the FAA below completes,
     * i.e. when the store-part of the FAA happens.
     * We use FAA with both acquire and release semantics.
     */
    self_ticket = atomic_fetch_and_add_acq_rel(&l->global_ticket, 0x4);

    /* Save the ticket + 2 in our array entry.
     * This makes our entry a valid entry to the other CPUs.
     * The acquire barrier above prevents the store to move before the FAA.
     */
    atomic_store_relaxed(&self->drawn_ticket, self_ticket + 0x2);

    /* iterate all CPUs in linear order (any order will do) */
    end = &l->c[l->num_cpus];
    for (c = &l->c[0]; c < end; c++) {
        if (c == self) {
            continue; /* skip self */
        }

again:
        /* Check the ticket value and spin while a ticket is in preparation.
         * A relaxed load is sufficient before accessing the resources;
         * ordering is ensures by the FAA above.
         */
        c_ticket = atomic_load_relaxed(&c->drawn_ticket);

        if ((c_ticket & 0x1) != 0x0) {
            /* request in preparation; spin */
            /* idle hyperthread */
            atomic_relax();
            goto again;
        }

        if ((c_ticket & 0x2) == 0x0) {
            continue; /* skip if not active */
        }
        if ((int32_t)(c_ticket - self_ticket) > 0) {
            continue; /* skip if newer (wrap-around safe) */
        }

        /* check for resource request conflicts */
        if (res_and_neqz(c->resources, r) == 0) {
            continue; /* skip if no conflicts */
        }

        /* Local spinning until the ticket changes:
         * This catches any intermediate changes,
         * even if the CPU has updated its resources.
         * Relaxed spinning is OK.
         *
         * Note that using a load_acquire() here is not sufficient to replace
         * the atomic_acquire_barrier() below, as other CPUs might release the shared
         * resources briefly before our check of the ticket state above.
         */
        while (atomic_load_relaxed(&c->drawn_ticket) == c_ticket) {
            /* idle hyperthread */
            atomic_relax();
        }
    }

    /* final acquire barrier before accessing the shared resources */
    atomic_acquire_barrier();
}

/** unlock */
void llab6_unlock(llab6_lock_t *l, uint32_t cpu)
{
    llab6_node_t *self;

    assert(l != NULL);
    assert(cpu < l->num_cpus);

    self = &l->c[cpu];

    /* clear the active bit (we simply set the ticket to zero) */
    atomic_store_release(&self->drawn_ticket, 0);
}

////////////////////////////////////////////////////////////////////////////////

/// reader-writer lock-less array-based multi-resource lock (RW-LLAB-MRLOCK, based on LLAB5)

/** initialize lock */
void rwllab1_init(rwllab1_lock_t *l, uint32_t num_cpus)
{
    assert(l != NULL);
    assert(num_cpus <= MAX_CPUS);

    memset(l, 0, sizeof(*l));
    l->num_cpus = num_cpus;
}

/** lock resources "r" for reading and "w" for writing */
void rwllab1_lock(rwllab1_lock_t *l, uint32_t cpu, const res_t r, const res_t w)
{
    rwllab1_per_cpu_t *self, *c, *end;
    uint32_t self_ticket, c_ticket;

    assert(l != NULL);
    assert(cpu < l->num_cpus);

    /* register our resource request */
    self = &l->c[cpu];
    /* assign resources: write requests are implicit read requests, too */
    res_or_assign(self->rw_resources, r, w);
    res_assign(self->w_resources, w);

    /* Set our own ticket to 0x1 ("in preparation").
     * Other CPUs will spin when they observe the LSB is set.
     * The release barrier below orders this store before the FAA.
     */
    atomic_store_relaxed(&self->drawn_ticket, 0x1);

    /* draw a unique ticket:
     * Writing to our own ticket must be visible before the FAA below completes,
     * i.e. when the store-part of the FAA happens.
     * We use FAA with both acquire and release semantics.
     */
    self_ticket = atomic_fetch_and_add_acq_rel(&l->global_ticket, 0x4);

    /* Save the ticket + 2 in our array entry.
     * This makes our entry a valid entry to the other CPUs.
     * The acquire barrier above prevents the store to move before the FAA.
     */
    atomic_store_relaxed(&self->drawn_ticket, self_ticket + 0x2);

    /* iterate all CPUs in linear order (any order will do) */
    end = &l->c[l->num_cpus];
    for (c = &l->c[0]; c < end; c++) {
        if (c == self) {
            continue; /* skip self */
        }

again:
        /* Check the ticket value and spin while a ticket is in preparation.
         * A relaxed load is sufficient before accessing the resources;
         * ordering is ensures by the FAA above.
         */
        c_ticket = atomic_load_relaxed(&c->drawn_ticket);

        if ((c_ticket & 0x1) != 0x0) {
            /* request in preparation; spin */
            /* idle hyperthread */
            atomic_relax();
            goto again;
        }

        if ((c_ticket & 0x2) == 0x0) {
            continue; /* skip if not active */
        }
        if ((int32_t)(c_ticket - self_ticket) > 0) {
            continue; /* skip if newer (wrap-around safe) */
        }

        /* check for resource request conflicts */
        if ((res_and_neqz(r, c->w_resources) == 0) &&
            (res_and_neqz(w, c->rw_resources) == 0)) {
            continue; /* skip if no conflicts */
        }

        /* Local spinning until the ticket changes:
         * This catches any intermediate changes,
         * even if the CPU has updated its resources.
         * Relaxed spinning is OK.
         *
         * Note that using a load_acquire() here is not sufficient to replace
         * the atomic_acquire_barrier() below, as other CPUs might release the shared
         * resources briefly before our check of the ticket state above.
         */
        while (atomic_load_relaxed(&c->drawn_ticket) == c_ticket) {
            /* idle hyperthread */
            atomic_relax();
        }
    }

    /* final acquire barrier before accessing the shared resources */
    atomic_acquire_barrier();
}

/** unlock */
void rwllab1_unlock(rwllab1_lock_t *l, uint32_t cpu)
{
    rwllab1_per_cpu_t *self;

    assert(l != NULL);
    assert(cpu < l->num_cpus);

    self = &l->c[cpu];

    /* clear the active bit (we simply set the ticket to zero) */
    atomic_store_release(&self->drawn_ticket, 0);
}

////////////////////////////////////////////////////////////////////////////////

/// reader-writer lock-less array-based multi-resource lock (RW-LLAB-MRLOCK, based on LLAB6) + compact layout

/** initialize lock */
void rwllab2_init(rwllab2_lock_t *l, uint32_t num_cpus)
{
    assert(l != NULL);
    assert(num_cpus <= MAX_CPUS);

    memset(l, 0, sizeof(*l));
    l->num_cpus = num_cpus;
}

/** lock resources "r" for reading and "w" for writing */
void rwllab2_lock(rwllab2_lock_t *l, uint32_t cpu, const res_t r, const res_t w)
{
    rwllab2_per_cpu_t *self, *c, *end;
    uint32_t self_ticket, c_ticket;

    assert(l != NULL);
    assert(cpu < l->num_cpus);

    /* register our resource request */
    self = &l->c[cpu];
    /* assign resources: write requests are implicit read requests, too */
    res_or_assign(self->rw_resources, r, w);
    res_assign(self->w_resources, w);

    /* Set our own ticket to 0x1 ("in preparation").
     * Other CPUs will spin when they observe the LSB is set.
     * The release barrier below orders this store before the FAA.
     */
    atomic_store_relaxed(&self->drawn_ticket, 0x1);

    /* draw a unique ticket:
     * Writing to our own ticket must be visible before the FAA below completes,
     * i.e. when the store-part of the FAA happens.
     * We use FAA with both acquire and release semantics.
     */
    self_ticket = atomic_fetch_and_add_acq_rel(&l->global_ticket, 0x4);

    /* Save the ticket + 2 in our array entry.
     * This makes our entry a valid entry to the other CPUs.
     * The acquire barrier above prevents the store to move before the FAA.
     */
    atomic_store_relaxed(&self->drawn_ticket, self_ticket + 0x2);

    /* iterate all CPUs in linear order (any order will do) */
    end = &l->c[l->num_cpus];
    for (c = &l->c[0]; c < end; c++) {
        if (c == self) {
            continue; /* skip self */
        }

again:
        /* Check the ticket value and spin while a ticket is in preparation.
         * A relaxed load is sufficient before accessing the resources;
         * ordering is ensures by the FAA above.
         */
        c_ticket = atomic_load_relaxed(&c->drawn_ticket);

        if ((c_ticket & 0x1) != 0x0) {
            /* request in preparation; spin */
            /* idle hyperthread */
            atomic_relax();
            goto again;
        }

        if ((c_ticket & 0x2) == 0x0) {
            continue; /* skip if not active */
        }
        if ((int32_t)(c_ticket - self_ticket) > 0) {
            continue; /* skip if newer (wrap-around safe) */
        }

        /* check for resource request conflicts */
        if ((res_and_neqz(r, c->w_resources) == 0) &&
            (res_and_neqz(w, c->rw_resources) == 0)) {
            continue; /* skip if no conflicts */
        }

        /* Local spinning until the ticket changes:
         * This catches any intermediate changes,
         * even if the CPU has updated its resources.
         * Relaxed spinning is OK.
         *
         * Note that using a load_acquire() here is not sufficient to replace
         * the atomic_acquire_barrier() below, as other CPUs might release the shared
         * resources briefly before our check of the ticket state above.
         */
        while (atomic_load_relaxed(&c->drawn_ticket) == c_ticket) {
            /* idle hyperthread */
            atomic_relax();
        }
    }

    /* final acquire barrier before accessing the shared resources */
    atomic_acquire_barrier();
}

/** unlock */
void rwllab2_unlock(rwllab2_lock_t *l, uint32_t cpu)
{
    rwllab2_per_cpu_t *self;

    assert(l != NULL);
    assert(cpu < l->num_cpus);

    self = &l->c[cpu];

    /* clear the active bit (we simply set the ticket to zero) */
    atomic_store_release(&self->drawn_ticket, 0);
}

////////////////////////////////////////////////////////////////////////////////

/// Ward's RNLP
///
/// Bryan C. Ward, James H. Anderson:
/// "Fine-grained Multiprocessor Real-Time Locking with Improved Blocking"
/// In: Conference on Real-Time Networks and Systems (RTNS), 67-76, 2013
/// https://www.cs.unc.edu/~anderson/papers/rtns13_long.pdf
///
/// This version of RNLP is a simplified variant of the R/W-RNLP implementation
/// based on the following observation in the paper (end of Section 4 on DGLs):
///
///     If all concurrent resource accesses in a system are sup-
///     ported by using DGLs, then the implementation of the RNLP
///     can be greatly simplified. The timestamp-ordered queues be-
///     come simple FIFO queues, and there is no need for jobs to
///     "reserve" their position in any queue. This is due to the fact
///     that all enqueueing due to one request is done atomically.
///     Thus, in this case, not only is the number of system calls
///     reduced, but the execution time of any one system call is
///     likely lessened as well.
///
/// In this spirit, we simplify the implementation to support non-nested
/// requests to multiple resources. The implementation comprises FIFO queues
/// for each resource, with head and tail pointers,
/// and we simply use our current CPU ID as queue nodes.

/** initialize lock */
void rnlp1_init(rnlp1_global_t *l)
{
    assert(l != NULL);

    memset(l, 0, sizeof(*l));
    _ticket_spin_init(&l->lock);

#ifndef NDEBUG
    for (size_t i = 0; i < RNLP1_Q; i++) {
        for (size_t pos = 0; pos < RNLP1_M; pos++) {
            l->queue[i][pos] = -1u;
        }
    }
#endif
}

/** lock resources "resources" */
void rnlp1_lock(rnlp1_global_t *l, uint32_t proc, const res_t resources)
{
    assert(l != NULL);
    assert(resources != NULL);
    assert(proc < RNLP1_M);

    /* enqueue our requests in the relevant queues */
    _ticket_spin_lock(&l->lock);
    res_for_each_word(resources) {
        size_t i;
        res_for_each_bit(resources, i) {
            uint32_t pos = l->tail[i];
            assert(l->queue[i][pos] == -1u);
            l->queue[i][pos] = proc;
            l->tail[i] = (pos + 1) % RNLP1_M;
        }
    }
    _ticket_spin_unlock(&l->lock);

    /* spin until our request becomes the head of each relevant queue */
    res_for_each_word(resources) {
        size_t i;
        res_for_each_bit(resources, i) {
            while (l->queue[i][atomic_load_relaxed(&l->head[i])] != proc) {
                /* idle hyperthread */
                atomic_relax();
            }
        }
    }

    // NOTE: needed!
    atomic_acquire_barrier();
}

/** unlock resources "resources" */
void rnlp1_unlock(rnlp1_global_t *l, uint32_t proc, const res_t resources)
{
    assert(l != NULL);
    assert(resources != NULL);
    assert(proc < RNLP1_M);
    (void)proc;

    // NOTE: needed!
    atomic_release_barrier();

    _ticket_spin_lock(&l->lock);
    res_for_each_word(resources) {
        size_t i;
        res_for_each_bit(resources, i) {
            uint32_t pos = l->head[i];
            assert(l->queue[i][pos] == proc);
#ifndef NDEBUG
            l->queue[i][pos] = -1u; // invalid processor ID
#endif
            l->head[i] = (pos + 1) % RNLP1_M;
        }
    }
    _ticket_spin_unlock(&l->lock);
}

////////////////////////////////////////////////////////////////////////////////

/// Ward's RNLP (like RNLP1, using an MCS lock)

/** initialize lock */
void rnlp2_init(rnlp2_global_t *l)
{
    assert(l != NULL);

    memset(l, 0, sizeof(*l));
    _mcs_init(&l->lock);

#ifndef NDEBUG
    for (size_t i = 0; i < RNLP2_Q; i++) {
        for (size_t pos = 0; pos < RNLP2_M; pos++) {
            l->queue[i][pos] = -1u;
        }
    }
#endif
}

/** lock resources "resources" */
void rnlp2_lock(rnlp2_global_t *l, uint32_t proc, const res_t resources)
{
    mcs_node_t lock_node;

    assert(l != NULL);
    assert(resources != NULL);
    assert(proc < RNLP2_M);

    /* enqueue our requests in the relevant queues */
    _mcs_lock(&l->lock, &lock_node);
    res_for_each_word(resources) {
        size_t i;
        res_for_each_bit(resources, i) {
            uint32_t pos = l->tail[i];
            assert(l->queue[i][pos] == -1u);
            l->queue[i][pos] = proc;
            l->tail[i] = (pos + 1) % RNLP2_M;
        }
    }
    _mcs_unlock(&l->lock, &lock_node);

    /* spin until our request becomes the head of each relevant queue */
    res_for_each_word(resources) {
        size_t i;
        res_for_each_bit(resources, i) {
            while (l->queue[i][atomic_load_relaxed(&l->head[i])] != proc) {
                /* idle hyperthread */
                atomic_relax();
            }
        }
    }

    // NOTE: needed!
    atomic_acquire_barrier();
}

/** unlock resources "resources" */
void rnlp2_unlock(rnlp2_global_t *l, uint32_t proc, const res_t resources)
{
    mcs_node_t lock_node;

    assert(l != NULL);
    assert(resources != NULL);
    assert(proc < RNLP2_M);
    (void)proc;

    // NOTE: needed!
    atomic_release_barrier();

    _mcs_lock(&l->lock, &lock_node);
    res_for_each_word(resources) {
        size_t i;
        res_for_each_bit(resources, i) {
            uint32_t pos = l->head[i];
            assert(l->queue[i][pos] == proc);
#ifndef NDEBUG
            l->queue[i][pos] = -1u; // invalid processor ID
#endif
            l->head[i] = (pos + 1) % RNLP2_M;
        }
    }
    _mcs_unlock(&l->lock, &lock_node);
}

////////////////////////////////////////////////////////////////////////////////

/// Nemitz's RNLP (single-queue implementation using a ticket lock (original))
///
/// Catherine E. Nemitz, Tanya Amert, James H. Anderson,
/// "Real-time multiprocessor locks with nesting: optimizing the common case",
/// In: International Conference on Real Time Networks and Systems (RTNS), 38--47, 2017
/// https://www.cs.unc.edu/~anderson/papers/rtsj19app.pdf
/// https://cs.unc.edu/~anderson/papers/fast_rwrnlp_code.tar.gz

/** initialize lock */
void rnlp3_init(rnlp3_global_t *l)
{
    assert(l != NULL);

    memset(l, 0, sizeof(*l));
    _ticket_spin_init(&l->lock);
}

/** lock resources "resources" */
void rnlp3_lock(rnlp3_global_t *l, uint32_t proc, const res_t resources)
{
    struct rnlp3_percpu *self;

    assert(l != NULL);
    assert(proc < MAX_CPUS);
    assert(resources != NULL);
    self = &l->percpu[proc];

    res_assign(self->resources, resources);
    assert(self->waiting == 0);

    /* enqueue our requests in the relevant queues */
    _ticket_spin_lock(&l->lock);

    if (res_and_neqz(l->unavailable, resources) == 0) {
        self->waiting = 0;
        res_or(l->locked, resources);
        res_or(l->unavailable, resources);
    } else {
        self->waiting = 1;
        self->next = NULL;

        if (l->head == NULL) {
            l->head = self;
        } else {
            l->tail->next = self;
        }
        l->tail = self;
    }

    _ticket_spin_unlock(&l->lock);

    /* spin until our request becomes available */
    while (atomic_load_relaxed(&self->waiting) != 0) {
        /* idle hyperthread */
        atomic_relax();
    }

    // NOTE: needed!
    atomic_acquire_barrier();
}

/** unlock resources "resources" */
void rnlp3_unlock(rnlp3_global_t *l, uint32_t proc, const res_t resources)
{
    struct rnlp3_percpu *node, *prev;

    assert(l != NULL);
    assert(proc < MAX_CPUS);
    (void)proc;
    assert(resources != NULL);

    // NOTE: needed!
    atomic_release_barrier();

    _ticket_spin_lock(&l->lock);

    res_bic(l->locked, resources);
    res_assign(l->unavailable, l->locked);

    for (prev = NULL, node = l->head; node != NULL; prev = node, node = node->next) {
        assert(node->waiting != 0);

        if (res_and_neqz(l->unavailable, node->resources) == 0) {
            res_or(l->locked, node->resources);
            node->waiting = 0;

            if (node == l->head) {
                l->head = node->next;
            } else if (node == l->tail) {
                l->tail = prev;
                prev->next = NULL;
            } else {
                prev->next = node->next;
            }
        }

        res_or(l->unavailable, node->resources);
    }

    _ticket_spin_unlock(&l->lock);
}

////////////////////////////////////////////////////////////////////////////////

/// Nemitz's RNLP (single-queue implementation using an MCS lock)
///
/// Catherine E. Nemitz, Tanya Amert, James H. Anderson,
/// "Real-time multiprocessor locks with nesting: optimizing the common case",
/// In: International Conference on Real Time Networks and Systems (RTNS), 38--47, 2017
/// https://www.cs.unc.edu/~anderson/papers/rtsj19app.pdf
/// https://cs.unc.edu/~anderson/papers/fast_rwrnlp_code.tar.gz

/** initialize lock */
void rnlp4_init(rnlp4_global_t *l)
{
    assert(l != NULL);

    memset(l, 0, sizeof(*l));
    _mcs_init(&l->lock);
}

/** lock resources "resources" */
void rnlp4_lock(rnlp4_global_t *l, uint32_t proc, const res_t resources)
{
    struct rnlp4_percpu *self;
    mcs_node_t lock_node;

    assert(l != NULL);
    assert(proc < MAX_CPUS);
    assert(resources != NULL);
    self = &l->percpu[proc];

    res_assign(self->resources, resources);
    assert(self->waiting == 0);

    /* enqueue our requests in the relevant queues */
    _mcs_lock(&l->lock, &lock_node);

    if (res_and_neqz(l->unavailable, resources) == 0) {
        self->waiting = 0;
        res_or(l->locked, resources);
        res_or(l->unavailable, resources);
    } else {
        self->waiting = 1;
        self->next = NULL;

        if (l->head == NULL) {
            l->head = self;
        } else {
            l->tail->next = self;
        }
        l->tail = self;
    }

    _mcs_unlock(&l->lock, &lock_node);

    /* spin until our request becomes available */
    while (atomic_load_relaxed(&self->waiting) != 0) {
        /* idle hyperthread */
        atomic_relax();
    }

    // NOTE: needed!
    atomic_acquire_barrier();
}

/** unlock resources "resources" */
void rnlp4_unlock(rnlp4_global_t *l, uint32_t proc, const res_t resources)
{
    struct rnlp4_percpu *node, *prev;
    mcs_node_t lock_node;

    assert(l != NULL);
    assert(proc < MAX_CPUS);
    (void)proc;
    assert(resources != NULL);

    // NOTE: needed!
    atomic_release_barrier();

    _mcs_lock(&l->lock, &lock_node);

    res_bic(l->locked, resources);
    res_assign(l->unavailable, l->locked);

    for (prev = NULL, node = l->head; node != NULL; prev = node, node = node->next) {
        assert(node->waiting != 0);

        if (res_and_neqz(l->unavailable, node->resources) == 0) {
            res_or(l->locked, node->resources);
            node->waiting = 0;

            if (node == l->head) {
                l->head = node->next;
            } else if (node == l->tail) {
                l->tail = prev;
                prev->next = NULL;
            } else {
                prev->next = node->next;
            }
        }

        res_or(l->unavailable, node->resources);
    }

    _mcs_unlock(&l->lock, &lock_node);
}

////////////////////////////////////////////////////////////////////////////////

/// Ward's RW-RNLP (using PF-C and a ticket lock)
///
/// Bryan C. Ward, James H. Anderson:
/// "Multi-Resource Real-Time Reader/Writer Locks for Multiprocessors"
/// In: International Parallel and Distributed Processing Symposium (IPDPS), 177-186, 2014
/// https://www.cs.unc.edu/~bcw/pubs/ipdps14_long.pdf
///
/// Or from Ward's dissertation, pages 104 to 107

/** request status */
#define RWRNLP1_WAITING  0
#define RWRNLP1_ACQUIRED 1
#define RWRNLP1_ENTITLED 2

/** request type */
#define RWRNLP1_READ     0
#define RWRNLP1_WRITE    1

/** initialize lock */
void rwrnlp1_init(rwrnlp1_global_t *l)
{
    assert(l != NULL);

    memset(l, 0, sizeof(*l));
    _pfc_init(&l->pflock);
    _ticket_spin_init(&l->mlock);
}

/** read lock resources "r" */
void rwrnlp1_read_lock(rwrnlp1_global_t *l, uint32_t proc, const res_t resources)
{
    rwrnlp1_request_t *r;

    assert(l != NULL);
    assert(proc < RWRNLP1_M);

    r = &l->requests[proc];
    res_assign(r->resources, resources);
    r->type = RWRNLP1_READ;
    r->status = RWRNLP1_WAITING;
    l->entry[proc] += 1;

    _pfc_read_lock(&l->pflock);
    if (res_and_neqz(r->resources, l->unavailable) == 0) {
        r->status = RWRNLP1_ACQUIRED;
    }
    _pfc_read_unlock(&l->pflock);

    if (r->status != RWRNLP1_ACQUIRED) {
        while (res_and_neqz(r->resources, l->wentitled) != 0) {
            /* idle hyperthread */
            atomic_relax();
        }
        r->status = RWRNLP1_ENTITLED;
        while (res_and_neqz(r->resources, l->wlocked) != 0) {
            /* idle hyperthread */
            atomic_relax();
        }
    }

    // NOTE: needed!
    atomic_acquire_barrier();
}

/** read unlock resources "r" */
void rwrnlp1_read_unlock(rwrnlp1_global_t *l, uint32_t proc, const res_t resources)
{
    rwrnlp1_request_t *r;
    (void)resources;    // unused variable

    assert(l != NULL);
    assert(proc < RWRNLP1_M);

    // NOTE: needed!
    atomic_release_barrier();

    l->exit[proc] += 1;
    //l->resources = NULL; // NOTE: impossible, clear the resources instead
    r = &l->requests[proc];
    res_clear(r->resources);
}

/** write lock resources "r" */
void rwrnlp1_write_lock(rwrnlp1_global_t *l, uint32_t proc, const res_t resources)
{
    rwrnlp1_request_t *r;

    assert(l != NULL);
    assert(proc < RWRNLP1_M);

    r = &l->requests[proc];
    res_assign(r->resources, resources);
    r->type = RWRNLP1_WRITE;
    r->status = RWRNLP1_WAITING;
    l->entry[proc] += 1;

    /* enqueue our requests in the relevant queues */
    _ticket_spin_lock(&l->mlock);
    res_for_each_word(resources) {
        size_t i;
        res_for_each_bit(resources, i) {
            uint32_t pos = l->wtail[i];
            assert(l->wqueue[i][pos] == NULL);
            l->wqueue[i][pos] = r;
            l->wtail[i] = (pos + 1) % RWRNLP1_M;
        }
    }
    _ticket_spin_unlock(&l->mlock);

    /* spin until our request becomes the head of each relevant queue */
    res_for_each_word(resources) {
        size_t i;
        res_for_each_bit(resources, i) {
            while (l->wqueue[i][atomic_load_relaxed(&l->whead[i])] != r) {
                /* idle hyperthread */
                atomic_relax();
            }
        }
    }

    _pfc_write_lock(&l->pflock);
    res_or(l->unavailable, resources);
    r->status = RWRNLP1_ENTITLED;
    /* NOTE: the following step is missing in paper! */
    res_or(l->wentitled, resources);
    _pfc_write_unlock(&l->pflock);

    for (size_t i = 0; i < RWRNLP1_M; i++) {
        if (i != proc) {
            uint32_t start = l->entry[i];
            uint32_t end = l->entry[i];
            rwrnlp1_request_t *tmp = &l->requests[i];
            if ((start == end) ||
                    (tmp->type == RWRNLP1_WRITE) ||
                    (tmp->status == RWRNLP1_WAITING) ||
                    (res_and_neqz(tmp->resources, r->resources) == 0)) {
                continue;
            }

            /* NOTE: wrap-around safe check of: exit[i] < start */
            while ((int32_t)(atomic_load_relaxed(&l->exit[i]) - start) < 0) {
                /* idle hyperthread */
                atomic_relax();
            }
        }
    }

    _pfc_write_lock(&l->pflock);
    // zero the bits in wentitled corresponding to each requested resource
    res_bic(l->wentitled, resources);
    res_or(l->wlocked, resources);
    _pfc_write_unlock(&l->pflock);

    /* NOTE: no additional acquire barrier required here,
     * the one from _pfc_write_lock() is sufficient
     */
}

/** write unlock resources "r" */
void rwrnlp1_write_unlock(rwrnlp1_global_t *l, uint32_t proc, const res_t resources)
{
    rwrnlp1_request_t *r;

    assert(l != NULL);
    assert(proc < RWRNLP1_M);

    // NOTE: needed!
    atomic_release_barrier();

    l->exit[proc] += 1;
    //l->resources = NULL; // NOTE: impossible, clear the resources instead
    r = &l->requests[proc];
    res_clear(r->resources);

    _pfc_write_lock(&l->pflock);
    // zero the bits in wlocked + unavailable corresponding to each requested resource
    res_bic(l->wlocked, resources);
    res_bic(l->unavailable, resources);
    _pfc_write_unlock(&l->pflock);

    _ticket_spin_lock(&l->mlock);
    res_for_each_word(resources) {
        size_t i;
        res_for_each_bit(resources, i) {
            uint32_t pos = l->whead[i];
            assert(l->wqueue[i][pos] == r);
#ifndef NDEBUG
            l->wqueue[i][pos] = NULL;
#endif
            l->whead[i] = (pos + 1) % RWRNLP1_M;
        }
    }
    _ticket_spin_unlock(&l->mlock);
}

////////////////////////////////////////////////////////////////////////////////

/// Ward's RW-RNLP (using PF-C and an MCS lock)
///
/// Bryan C. Ward, James H. Anderson:
/// "Multi-Resource Real-Time Reader/Writer Locks for Multiprocessors"
/// In: International Parallel and Distributed Processing Symposium (IPDPS), 177-186, 2014
/// https://www.cs.unc.edu/~bcw/pubs/ipdps14_long.pdf
///
/// Or from Ward's dissertation, pages 104 to 107

/** request status */
#define RWRNLP2_WAITING  0
#define RWRNLP2_ACQUIRED 1
#define RWRNLP2_ENTITLED 2

/** request type */
#define RWRNLP2_READ     0
#define RWRNLP2_WRITE    1

/** initialize lock */
void rwrnlp2_init(rwrnlp2_global_t *l)
{
    assert(l != NULL);

    memset(l, 0, sizeof(*l));
    _pfc_init(&l->pflock);
    _mcs_init(&l->mlock);
}

/** read lock resources "r" */
void rwrnlp2_read_lock(rwrnlp2_global_t *l, uint32_t proc, const res_t resources)
{
    rwrnlp2_request_t *r;

    assert(l != NULL);
    assert(proc < RWRNLP2_M);

    r = &l->requests[proc];
    res_assign(r->resources, resources);
    r->type = RWRNLP2_READ;
    r->status = RWRNLP2_WAITING;
    l->entry[proc] += 1;

    _pfc_read_lock(&l->pflock);
    if (res_and_neqz(r->resources, l->unavailable) == 0) {
        r->status = RWRNLP2_ACQUIRED;
    }
    _pfc_read_unlock(&l->pflock);

    if (r->status != RWRNLP2_ACQUIRED) {
        while (res_and_neqz(r->resources, l->wentitled) != 0) {
            /* idle hyperthread */
            atomic_relax();
        }
        r->status = RWRNLP2_ENTITLED;
        while (res_and_neqz(r->resources, l->wlocked) != 0) {
            /* idle hyperthread */
            atomic_relax();
        }
    }

    // NOTE: needed!
    atomic_acquire_barrier();
}

/** read unlock resources "r" */
void rwrnlp2_read_unlock(rwrnlp2_global_t *l, uint32_t proc, const res_t resources)
{
    rwrnlp2_request_t *r;
    (void)resources;    // unused variable

    assert(l != NULL);
    assert(proc < RWRNLP2_M);

    // NOTE: needed!
    atomic_release_barrier();

    l->exit[proc] += 1;
    //l->resources = NULL; // NOTE: impossible, clear the resources instead
    r = &l->requests[proc];
    res_clear(r->resources);
}

/** write lock resources "r" */
void rwrnlp2_write_lock(rwrnlp2_global_t *l, uint32_t proc, const res_t resources)
{
    mcs_node_t lock_node;
    rwrnlp2_request_t *r;

    assert(l != NULL);
    assert(proc < RWRNLP2_M);

    r = &l->requests[proc];
    res_assign(r->resources, resources);
    r->type = RWRNLP2_WRITE;
    r->status = RWRNLP2_WAITING;
    l->entry[proc] += 1;

    /* enqueue our requests in the relevant queues */
    _mcs_lock(&l->mlock, &lock_node);
    res_for_each_word(resources) {
        size_t i;
        res_for_each_bit(resources, i) {
            uint32_t pos = l->wtail[i];
            assert(l->wqueue[i][pos] == NULL);
            l->wqueue[i][pos] = r;
            l->wtail[i] = (pos + 1) % RWRNLP2_M;
        }
    }
    _mcs_unlock(&l->mlock, &lock_node);

    /* spin until our request becomes the head of each relevant queue */
    res_for_each_word(resources) {
        size_t i;
        res_for_each_bit(resources, i) {
            while (l->wqueue[i][atomic_load_relaxed(&l->whead[i])] != r) {
                /* idle hyperthread */
                atomic_relax();
            }
        }
    }

    _pfc_write_lock(&l->pflock);
    res_or(l->unavailable, resources);
    r->status = RWRNLP2_ENTITLED;
    /* NOTE: the following step is missing in paper! */
    res_or(l->wentitled, resources);
    _pfc_write_unlock(&l->pflock);

    for (size_t i = 0; i < RWRNLP2_M; i++) {
        if (i != proc) {
            uint32_t start = l->entry[i];
            uint32_t end = l->entry[i];
            rwrnlp2_request_t *tmp = &l->requests[i];
            if ((start == end) ||
                    (tmp->type == RWRNLP2_WRITE) ||
                    (tmp->status == RWRNLP2_WAITING) ||
                    (res_and_neqz(tmp->resources, r->resources) == 0)) {
                continue;
            }

            /* NOTE: wrap-around safe check of: exit[i] < start */
            while ((int32_t)(atomic_load_relaxed(&l->exit[i]) - start) < 0) {
                /* idle hyperthread */
                atomic_relax();
            }
        }
    }

    _pfc_write_lock(&l->pflock);
    // zero the bits in wentitled corresponding to each requested resource
    res_bic(l->wentitled, resources);
    res_or(l->wlocked, resources);
    _pfc_write_unlock(&l->pflock);

    /* NOTE: no additional acquire barrier required here,
     * the one from _pfc_write_lock() is sufficient
     */
}

/** write unlock resources "r" */
void rwrnlp2_write_unlock(rwrnlp2_global_t *l, uint32_t proc, const res_t resources)
{
    mcs_node_t lock_node;
    rwrnlp2_request_t *r;

    assert(l != NULL);
    assert(proc < RWRNLP2_M);

    // NOTE: needed!
    atomic_release_barrier();

    l->exit[proc] += 1;
    //l->resources = NULL; // NOTE: impossible, clear the resources instead
    r = &l->requests[proc];
    res_clear(r->resources);

    _pfc_write_lock(&l->pflock);
    // zero the bits in wlocked + unavailable corresponding to each requested resource
    res_bic(l->wlocked, resources);
    res_bic(l->unavailable, resources);
    _pfc_write_unlock(&l->pflock);

    _mcs_lock(&l->mlock, &lock_node);
    res_for_each_word(resources) {
        size_t i;
        res_for_each_bit(resources, i) {
            uint32_t pos = l->whead[i];
            assert(l->wqueue[i][pos] == r);
#ifndef NDEBUG
            l->wqueue[i][pos] = NULL;
#endif
            l->whead[i] = (pos + 1) % RWRNLP2_M;
        }
    }
    _mcs_unlock(&l->mlock, &lock_node);
}

////////////////////////////////////////////////////////////////////////////////

/// Ward's RW-RNLP (using PF-T and an MCS lock)
///
/// Bryan C. Ward, James H. Anderson:
/// "Multi-Resource Real-Time Reader/Writer Locks for Multiprocessors"
/// In: International Parallel and Distributed Processing Symposium (IPDPS), 177-186, 2014
/// https://www.cs.unc.edu/~bcw/pubs/ipdps14_long.pdf
///
/// Or from Ward's dissertation, pages 104 to 107

/** request status */
#define RWRNLP3_WAITING  0
#define RWRNLP3_ACQUIRED 1
#define RWRNLP3_ENTITLED 2

/** request type */
#define RWRNLP3_READ     0
#define RWRNLP3_WRITE    1

/** initialize lock */
void rwrnlp3_init(rwrnlp3_global_t *l)
{
    assert(l != NULL);

    memset(l, 0, sizeof(*l));
    _pft_init(&l->pflock);
    _mcs_init(&l->mlock);
}

/** read lock resources "r" */
void rwrnlp3_read_lock(rwrnlp3_global_t *l, uint32_t proc, const res_t resources)
{
    rwrnlp3_request_t *r;

    assert(l != NULL);
    assert(proc < RWRNLP3_M);

    r = &l->requests[proc];
    res_assign(r->resources, resources);
    r->type = RWRNLP3_READ;
    r->status = RWRNLP3_WAITING;
    l->entry[proc] += 1;

    _pft_read_lock(&l->pflock);
    if (res_and_neqz(r->resources, l->unavailable) == 0) {
        r->status = RWRNLP3_ACQUIRED;
    }
    _pft_read_unlock(&l->pflock);

    if (r->status != RWRNLP3_ACQUIRED) {
        while (res_and_neqz(r->resources, l->wentitled) != 0) {
            /* idle hyperthread */
            atomic_relax();
        }
        r->status = RWRNLP3_ENTITLED;
        while (res_and_neqz(r->resources, l->wlocked) != 0) {
            /* idle hyperthread */
            atomic_relax();
        }
    }

    // NOTE: needed!
    atomic_acquire_barrier();
}

/** read unlock resources "r" */
void rwrnlp3_read_unlock(rwrnlp3_global_t *l, uint32_t proc, const res_t resources)
{
    rwrnlp3_request_t *r;
    (void)resources;    // unused variable

    assert(l != NULL);
    assert(proc < RWRNLP3_M);

    // NOTE: needed!
    atomic_release_barrier();

    l->exit[proc] += 1;
    //l->resources = NULL; // NOTE: impossible, clear the resources instead
    r = &l->requests[proc];
    res_clear(r->resources);
}

/** write lock resources "r" */
void rwrnlp3_write_lock(rwrnlp3_global_t *l, uint32_t proc, const res_t resources)
{
    mcs_node_t lock_node;
    rwrnlp3_request_t *r;

    assert(l != NULL);
    assert(proc < RWRNLP3_M);

    r = &l->requests[proc];
    res_assign(r->resources, resources);
    r->type = RWRNLP3_WRITE;
    r->status = RWRNLP3_WAITING;
    l->entry[proc] += 1;

    /* enqueue our requests in the relevant queues */
    _mcs_lock(&l->mlock, &lock_node);
    res_for_each_word(resources) {
        size_t i;
        res_for_each_bit(resources, i) {
            uint32_t pos = l->wtail[i];
            assert(l->wqueue[i][pos] == NULL);
            l->wqueue[i][pos] = r;
            l->wtail[i] = (pos + 1) % RWRNLP3_M;
        }
    }
    _mcs_unlock(&l->mlock, &lock_node);

    /* spin until our request becomes the head of each relevant queue */
    res_for_each_word(resources) {
        size_t i;
        res_for_each_bit(resources, i) {
            while (l->wqueue[i][atomic_load_relaxed(&l->whead[i])] != r) {
                /* idle hyperthread */
                atomic_relax();
            }
        }
    }

    _pft_write_lock(&l->pflock);
    res_or(l->unavailable, resources);
    r->status = RWRNLP3_ENTITLED;
    /* NOTE: the following step is missing in paper! */
    res_or(l->wentitled, resources);
    _pft_write_unlock(&l->pflock);

    for (size_t i = 0; i < RWRNLP3_M; i++) {
        if (i != proc) {
            uint32_t start = l->entry[i];
            uint32_t end = l->entry[i];
            rwrnlp3_request_t *tmp = &l->requests[i];
            if ((start == end) ||
                    (tmp->type == RWRNLP3_WRITE) ||
                    (tmp->status == RWRNLP3_WAITING) ||
                    (res_and_neqz(tmp->resources, r->resources) == 0)) {
                continue;
            }

            /* NOTE: wrap-around safe check of: exit[i] < start */
            while ((int32_t)(atomic_load_relaxed(&l->exit[i]) - start) < 0) {
                /* idle hyperthread */
                atomic_relax();
            }
        }
    }

    _pft_write_lock(&l->pflock);
    // zero the bits in wentitled corresponding to each requested resource
    res_bic(l->wentitled, resources);
    res_or(l->wlocked, resources);
    _pft_write_unlock(&l->pflock);

    /* NOTE: no additional acquire barrier required here,
     * the one from _pft_write_lock() is sufficient
     */
}

/** write unlock resources "r" */
void rwrnlp3_write_unlock(rwrnlp3_global_t *l, uint32_t proc, const res_t resources)
{
    mcs_node_t lock_node;
    rwrnlp3_request_t *r;

    assert(l != NULL);
    assert(proc < RWRNLP3_M);

    // NOTE: needed!
    atomic_release_barrier();

    l->exit[proc] += 1;
    //l->resources = NULL; // NOTE: impossible, clear the resources instead
    r = &l->requests[proc];
    res_clear(r->resources);

    _pft_write_lock(&l->pflock);
    // zero the bits in wlocked + unavailable corresponding to each requested resource
    res_bic(l->wlocked, resources);
    res_bic(l->unavailable, resources);
    _pft_write_unlock(&l->pflock);

    _mcs_lock(&l->mlock, &lock_node);
    res_for_each_word(resources) {
        size_t i;
        res_for_each_bit(resources, i) {
            uint32_t pos = l->whead[i];
            assert(l->wqueue[i][pos] == r);
#ifndef NDEBUG
            l->wqueue[i][pos] = NULL;
#endif
            l->whead[i] = (pos + 1) % RWRNLP3_M;
        }
    }
    _mcs_unlock(&l->mlock, &lock_node);
}

////////////////////////////////////////////////////////////////////////////////

/// Nemitz and Ward's Uniform C-RNLP with an internal ticket lock
///
/// Catherine E. Jarrett, Bryan C. Ward, and James H. Anderson:
/// "A Contention-Sensitive Fine-Grained Locking Protocol
///  for Multiprocessor Real-Time Systems"
/// In: International Conference on Real Time Networks and Systems (RTNS), 3-12, 2015
/// https://www.cs.unc.edu/~anderson/papers/rtns15a.pdf
///
/// Or from Nemitz's dissertation, pages 92 to 94
/// See also: https://www.cs.unc.edu/~jarretc/dissertation/

/** initialize lock */
void ucrnlp1_init(ucrnlp1_global_t *l)
{
    assert(l != NULL);

    memset(l, 0, sizeof(*l));
    _ticket_spin_init(&l->lock);
    l->enabled[0] = 1;
}

/** lock resources "r" */
uint32_t ucrnlp1_lock(ucrnlp1_global_t *l, const res_t resources)
{
    uint32_t start, next;

    assert(l != NULL);

    _ticket_spin_lock(&l->lock);
    if (l->pending_requests > 0) {
        start = (l->head + 1) % UCRNLP1_SIZE;

#if 1
        /* NOTE: The algorithm presented in the RTNS paper is suboptimal
         * when there are only non-conflicting requests. In this case,
         * a second/third request will be blocked even it is non-conflicting
         * to the first. Without violating the WCET properties of the protocol,
         * we can admit non-conflicting requests immediately in parallel
         * as long as there are no other requests already waiting.
         */
        if (l->pending_requests == l->blocked[start]) {
            start = l->head;
        }
#endif

        while (res_and_neqz(l->table[start], resources) != 0) {
            start = (start + 1) % UCRNLP1_SIZE;
        }
    } else {
        start = l->head;
    }
    next = (start + 1) % UCRNLP1_SIZE;
    l->blocked[next] += 1;
    l->pending_requests += 1;
    res_or(l->table[start], resources);
    _ticket_spin_unlock(&l->lock);

    while (atomic_load_relaxed(&l->enabled[start]) != 1) {
        /* idle hyperthread */
        atomic_relax();
    }

    // NOTE: needed!
    atomic_acquire_barrier();

    return next;
}

/** unlock resources "r" */
void ucrnlp1_unlock(ucrnlp1_global_t *l, const res_t resources, uint32_t next)
{
    assert(l != NULL);
    assert(next < UCRNLP1_SIZE);

    // NOTE: needed!
    atomic_release_barrier();

    _ticket_spin_lock(&l->lock);
    res_bic(l->table[l->head], resources);
    l->blocked[next] -= 1;
    l->pending_requests -= 1;
    if (l->blocked[next] == 0) {
        l->enabled[l->head] = 0;
        l->head = next;
        l->enabled[l->head] = 1;
    }
    _ticket_spin_unlock(&l->lock);
}

////////////////////////////////////////////////////////////////////////////////

/// Nemitz and Ward's Uniform C-RNLP with an internal MCS lock

/** initialize lock */
void ucrnlp2_init(ucrnlp2_global_t *l)
{
    assert(l != NULL);

    memset(l, 0, sizeof(*l));
    _mcs_init(&l->lock);
    l->enabled[0] = 1;
}

/** lock resources "r" */
uint32_t ucrnlp2_lock(ucrnlp2_global_t *l, const res_t resources)
{
    mcs_node_t lock_node;
    uint32_t start, next;

    assert(l != NULL);

    _mcs_lock(&l->lock, &lock_node);
    if (l->pending_requests > 0) {
        start = (l->head + 1) % UCRNLP1_SIZE;

#if 1
        /* NOTE: The algorithm presented in the RTNS paper is suboptimal
         * when there are only non-conflicting requests. In this case,
         * a second/third request will be blocked even it is non-conflicting
         * to the first. Without violating the WCET properties of the protocol,
         * we can admit non-conflicting requests immediately in parallel
         * as long as there are no other requests already waiting.
         */
        if (l->pending_requests == l->blocked[start]) {
            start = l->head;
        }
#endif

        while (res_and_neqz(l->table[start], resources) != 0) {
            start = (start + 1) % UCRNLP2_SIZE;
        }
    } else {
        start = l->head;
    }
    next = (start + 1) % UCRNLP2_SIZE;
    l->blocked[next] += 1;
    l->pending_requests += 1;
    res_or(l->table[start], resources);
    _mcs_unlock(&l->lock, &lock_node);

    while (atomic_load_relaxed(&l->enabled[start]) != 1) {
        /* idle hyperthread */
        atomic_relax();
    }

    // NOTE: needed!
    atomic_acquire_barrier();

    return next;
}

/** unlock resources "r" */
void ucrnlp2_unlock(ucrnlp2_global_t *l, const res_t resources, uint32_t next)
{
    mcs_node_t lock_node;

    assert(l != NULL);
    assert(next < UCRNLP2_SIZE);

    // NOTE: needed!
    atomic_release_barrier();

    _mcs_lock(&l->lock, &lock_node);
    res_bic(l->table[l->head], resources);
    l->blocked[next] -= 1;
    l->pending_requests -= 1;
    if (l->blocked[next] == 0) {
        l->enabled[l->head] = 0;
        l->head = next;
        l->enabled[l->head] = 1;
    }
    _mcs_unlock(&l->lock, &lock_node);
}

////////////////////////////////////////////////////////////////////////////////

/// MRLock -- Multi-resource Locks
///
/// Deli Zhang, Brendan Lynch, Damian Dechev
/// "Queue-Based and Adaptive Lock Algorithms for Scalable Resource Allocation
///   on Shared-Memory Multiprocessors""
/// Int J Parallel Prog; DOI 10.1007/s10766-014-0317-6, 2014


/** initialize lock */
void mrlock_init(mrlock_global_t *l)
{
    assert(l != NULL);

    /* must be a power of two */
    assert((MRLOCK_CELLS & (MRLOCK_CELLS - 1)) == 0);
    l->mask = MRLOCK_CELLS - 1;
    l->head = 0;
    l->tail = 0;

    for (uint32_t i = 0; i < MRLOCK_CELLS; i++) {
        res_fill(l->buffer[i].bits);
        l->buffer[i].seq = i;
    }
}

/** lock resources "r" */
uint32_t mrlock_lock(mrlock_global_t *l, res_t r)
{
    assert(l != NULL);
    assert(r != NULL);
    assert((r[0] | r[1]) != 0); /* MRLOCK does not like all-zero requests */

    mrlock_cell_t *c;
    uint32_t pos;

    for (;;) {
        pos = atomic_load_relaxed(&l->tail);
        c = &l->buffer[pos & l->mask];
        uint32_t seq = atomic_load_acquire(&c->seq);
        int32_t diff = (int32_t)seq - (int32_t)pos;
        if (diff == 0) {
            if (atomic_cas_relaxed(&l->tail, pos, pos + 1)) {
                break;
            }
        }
        /* idle hyperthread */
        atomic_relax();
    }

    res_assign(c->bits, r);
    atomic_store_release(&c->seq, pos + 1);

    uint32_t spin = atomic_load_relaxed(&l->head);
    while (spin != pos) {
        if ((pos - atomic_load_relaxed(&l->buffer[spin & l->mask].seq) > l->mask) ||
                (res_and_neqz(l->buffer[spin & l->mask].bits, r) == 0)) {
            spin++;
        } else {
            /* idle hyperthread */
            atomic_relax();
        }
    }
    return pos;
}

/** unlock resources "r"; the caller previously locked the handle "h" */
void mrlock_unlock(mrlock_global_t *l, uint32_t h)
{
    assert(l != NULL);

    mrlock_cell_t *c;
    uint32_t pos;

    res_clear(l->buffer[h & l->mask].bits);

again:
    pos = atomic_load_relaxed(&l->head);
    c = &l->buffer[pos & l->mask];
    if (res_eqz(c->bits)) {
        uint32_t seq = atomic_load_acquire(&c->seq);
        int32_t diff = (int32_t)seq - (int32_t)(pos + 1);
        if (diff == 0) {
            if (atomic_cas_relaxed(&l->head, pos, pos + 1)) {
                res_fill(c->bits);
                atomic_store_release(&c->seq, pos + l->mask + 1);
            }
        } else {
            /* idle hyperthread */
            atomic_relax();
        }
        goto again;
    }
}
