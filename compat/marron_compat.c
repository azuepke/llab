/* SPDX-License-Identifier: MIT */
/*
 * marron/compat.c
 *
 * Marron compatibility layer for Linux.
 *
 * azuepke, 2020-10-08: initial
 */

/* keep asserts enabled! */
#undef NDEBUG
#define _GNU_SOURCE

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <marron_compat.h>
#include <assert.h>
#include <sys/sysinfo.h>
#include <locks.h>
#include <sched.h>

#define THR_NUM (MAX_CPUS + 16)

static unsigned int num_cpus;
static pthread_t threads[THR_NUM];

unsigned int sys_cpu_get(void)
{
	unsigned int cpu = sched_getcpu();
	assert(cpu < num_cpus);
	return cpu;
}

unsigned long sys_cpu_mask(void)
{
	return ((((1ul << ((num_cpus) - 1)) -1) << 1) | 1ul);
}

unsigned int sys_thread_self(void)
{
	for (unsigned int i = 0; i < THR_NUM; i++) {
		if (threads[i] == pthread_self()) {
			return i;
		}
	}
	assert(0);
	return 0;
}

unsigned long long sys_time_get(void)
{
	struct timespec ts;

	clock_gettime(CLOCK_MONOTONIC, &ts);

	return ts.tv_sec * 1000*1000*1000 + ts.tv_nsec;
}

void sys_thread_exit(void)
{
	pthread_exit(NULL);
}

void sys_sleep(unsigned long ns)
{
	struct timespec ts;

	ts.tv_sec = 0;
	ts.tv_nsec = ns;
	clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &ts, NULL);
}

void create_thread_cpu(
	unsigned int thr_id,
	void (*func)(void *),
	void *arg,
	unsigned int prio,
	unsigned int cpu)
{
	pthread_attr_t attr;
	cpu_set_t cpumask;
	int ret;

	assert((thr_id > 0) && (thr_id < THR_NUM));
	assert(cpu < num_cpus);
	// NOTE: priority ignored
	(void)prio;

	ret = pthread_attr_init(&attr);
	assert(ret == 0);
	CPU_ZERO(&cpumask);
	CPU_SET(cpu, &cpumask);
	ret = pthread_attr_setaffinity_np(&attr, sizeof(cpumask), &cpumask);
	assert(ret == 0);

	ret = pthread_create(&threads[thr_id], &attr, (void*)func, arg);
	assert(ret == 0);
}

void join_thread(unsigned int thr_id)
{
	int ret;

	assert((thr_id > 0) && (thr_id < THR_NUM));

	ret = pthread_join(threads[thr_id], NULL);
	assert(ret == 0);
}

void marron_compat_init(unsigned int limit_cpus)
{
	cpu_set_t cpumask;
	int ret;
	unsigned int avail_cpus;

	avail_cpus = get_nprocs();
	num_cpus = avail_cpus;

	if (num_cpus > MAX_CPUS) {
		num_cpus = MAX_CPUS;
	}
	if (num_cpus > limit_cpus) {
		num_cpus = limit_cpus;
	}

	printf("Marron compat on Linux: using %d of %d CPUs\n", num_cpus, avail_cpus);

	/* restrict caller to CPU #0 by default */
	threads[0] = pthread_self();
	CPU_ZERO(&cpumask);
	CPU_SET(0, &cpumask);
	ret = pthread_setaffinity_np(threads[0], sizeof(cpumask), &cpumask);
	assert(ret == 0);
}
