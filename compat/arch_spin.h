/* SPDX-License-Identifier: MIT */
/*
 * arch_spin.h
 *
 * x86 ticket spin locks
 *
 * azuepke, 2013-04-09: initial
 * azuepke, 2020-01-31: ticket locks
 */

#ifndef __ARCH_SPIN_H__
#define __ARCH_SPIN_H__

#include <stdint.h>
#include <compiler.h>


/** spin lock data type */
typedef union {
	uint32_t lock;
	struct {
		/* little endian order: ticket is as MSB position */
		uint16_t owner_cpu;
		uint8_t served;
		uint8_t ticket;
	} s;
} arch_spin_t;


/* increments */
#define ARCH_TICKET_INC 0x01000000
#define ARCH_SERVED_INC 0x00010000

/** spin lock static initializer */
#define ARCH_SPIN_INIT { 0 }

/** spin lock initializer */
static inline void arch_spin_init(arch_spin_t *lock)
{
	lock->lock = 0;
}

/** try to lock spin lock, returns TRUE on success */
static inline int arch_spin_trylock(arch_spin_t *lock)
{
	arch_spin_t val, newval, oldval;

	val = access_once(*lock);
	if (unlikely(val.s.ticket != val.s.served)) {
		return 0;
	}

	newval = val;
	newval.lock += ARCH_TICKET_INC;

	/* NOTE: LOCK is always set, spinlocks are only used on SMP systems */
	__asm__ volatile (
		"lock; cmpxchgl %3, %1\n"
		: "=a"(oldval), "+m"(lock->lock) : "0"(val), "r"(newval) : "memory", "cc");

	return (val.lock == oldval.lock);
}

/** lock spin lock */
static inline void arch_spin_lock(arch_spin_t *lock)
{
	arch_spin_t val;

	/* NOTE: LOCK is always set, spinlocks are only used on SMP systems */
	__asm__ volatile (
		"lock; xaddl %0, %1\n"
		: "=&a"(val), "+m" (lock->lock) : "0" (ARCH_TICKET_INC) : "memory", "cc");

	while (val.s.ticket != val.s.served) {
		__asm__ volatile ("pause" : : : "memory");
		val.s.served = access_once(lock->s.served);
	}
}

/** unlock spin lock */
static inline void arch_spin_unlock(arch_spin_t *lock)
{
	barrier();
	lock->s.served++;
}

#endif
