/* SPDX-License-Identifier: MIT */
/*
 * marron/compat.h
 *
 * Marron compatibility layer for Linux.
 *
 * azuepke, 2020-10-08: initial
 */

#ifndef MARRON_COMPAT_H
#define MARRON_COMPAT_H

#define TIMEOUT_INFINITE 0

unsigned int sys_cpu_get(void);
unsigned long sys_cpu_mask(void);
unsigned int sys_thread_self(void);

unsigned long long sys_time_get(void);

void sys_thread_exit(void);

void sys_sleep(unsigned long ns);

void create_thread_cpu(
	unsigned int thr_id,
	void (*func)(void *),
	void *arg,
	unsigned int prio,
	unsigned int cpu);

void join_thread(unsigned int thr_id);

void marron_compat_init(unsigned int limit_cpus);

#endif
