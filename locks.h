/* SPDX-License-Identifier: CC-BY-4.0 */
/*
 * locks.h
 *
 * Implementation of different multi-resource locks
 *
 * Unless attributed otherwise, the lock implementations are
 * Copyright (c) 2020-2021 Alexander Zuepke
 * Creative Commons Attribution 4.0 International (CC-BY-4.0) license
 *
 * MCS locks are from:
 * John M. Mellor-Crummey, Michael L. Scott:
 * "Algorithms for Scalable Synchronization on Shared-Memory Multiprocessors"
 * ACM Trans. Comput. Syst. 9(1), 21-65, 1991
 *
 * Brandenburg's phase-fair ticket-based RW-locks are from:
 * Björn B. Brandenburg, James H. Anderson:
 * "Spin-Based Reader-Writer Synchronization for Multiprocessor Real-Time Systems",
 * Real-Time Systems 46(1), 25-87, 2010
 *
 * Ward's multi-queue version of RNLP is from:
 * Bryan C. Ward, James H. Anderson:
 * "Fine-grained Multiprocessor Real-Time Locking with Improved Blocking",
 * Conference on Real-Time Networks and Systems (RTNS), 67-76, 2013
 *
 * Nemitz's single-queue version of RNLP is from:
 * Catherine E. Nemitz, Tanya Amert, James H. Anderson,
 * "Real-time multiprocessor locks with nesting: optimizing the common case",
 * International Conference on Real Time Networks and Systems (RTNS), 38--47, 2017
 *
 * RW-RNLP is from:
 * Bryan C. Ward, James H. Anderson:
 * "Multi-Resource Real-Time Reader/Writer Locks for Multiprocessors"
 * International Parallel and Distributed Processing Symposium (IPDPS), 177-186, 2014
 *
 * Nemitz and Ward's Uniform C-RNLP is from:
 * Catherine E. Jarrett, Bryan C. Ward, and James H. Anderson:
 * "A Contention-Sensitive Fine-Grained Locking Protocol
 *  for Multiprocessor Real-Time Systems"
 * In: International Conference on Real Time Networks and Systems (RTNS), 3-12, 2015
 *
 * MRLOCK is from:
 * Deli Zhang, Brendan Lynch, Damian Dechev
 * "Queue-Based and Adaptive Lock Algorithms for Scalable Resource Allocation
 *  on Shared-Memory Multiprocessors""
 * Int J Parallel Prog; DOI 10.1007/s10766-014-0317-6, 2014
 *
 *
 * NOTE: We assume sizeof(pointer) == sizeof(long).
 *       We also assume that "long" matches the machine's native type,
 *       i.e. an ILP32 or LP64 environment,
 *       and that both int and long are supported by atomic intrinsics.
 *       The code is fine for 32-bit and 64-bit architectures.
 *
 * azuepke, 2020-07-24: initial
 * azuepke, 2020-10-01: move lock implementation to own lib
 * azuepke, 2020-10-04: optimize for sparsely populated resource-bitmaps
 * azuepke, 2020-10-11: reader-write variants of SQDGL2/LLAB4
 * azuepke, 2020-10-16: implementation based on pure C11 atomics
 * azuepke, 2020-10-22: cleanup for paper
 * azuepke, 2021-08-27: include C-RNLP
 * azuepke, 2021-09-03: refactor resource bitmaps
 * azuepke, 2021-10-19: CC-BY-4.0 license
 */

#ifndef __LOCKS_H__
#define __LOCKS_H__

#include <stdint.h>

////////////////////////////////////////////////////////////////////////////////

#define RES_BITS 64
#include "res.h"

/** cache alignment (64 bytes fits on all known x86 and ARM architectures) */
#define LOCK_ALIGNMENT 64

#define __lock_aligned(a)   __attribute__((__aligned__(a)))

/** maximum number of CPUs -- used to allocate static per-CPU arrays */
#ifndef MAX_CPUS
#ifdef __arm__
#define MAX_CPUS 4
#elif defined(__aarch64__)
#define MAX_CPUS 16
#else /* 64-bit x86 */
#define MAX_CPUS 64
#endif
#endif

////////////////////////////////////////////////////////////////////////////////

/// single-resource TAS and TATAS locks using various implementation tricks

/** TAS global lock */
typedef uint32_t sspin_lock_t;

/** initialize lock */
void sspin_init(sspin_lock_t *l);

/** lock */
void sspin_lock_tas(sspin_lock_t *l);

/** lock */
void sspin_lock_swap(sspin_lock_t *l);

/** lock */
void sspin_lock_cas(sspin_lock_t *l);

/** lock */
void sspin_lock_test_tas(sspin_lock_t *l);

/** lock */
void sspin_lock_test_swap(sspin_lock_t *l);

/** lock */
void sspin_lock_test_cas(sspin_lock_t *l);

/** unlock */
void sspin_unlock(sspin_lock_t *l);

////////////////////////////////////////////////////////////////////////////////

/// ticket spin locks

typedef union {
    uint32_t lock;
    struct {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
        uint16_t served;
        uint16_t ticket;
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
        uint16_t ticket;
        uint16_t served;
#else
#error "Unknown endianess"
#endif
    } s;
} ticket_spin_t;

void ticket_spin_init(ticket_spin_t *lock);
void ticket_spin_lock(ticket_spin_t *lock);
void ticket_spin_unlock(ticket_spin_t *lock);

////////////////////////////////////////////////////////////////////////////////

/// MCS locks
///
/// John M. Mellor-Crummey, Michael L. Scott:
/// "Algorithms for Scalable Synchronization on Shared-Memory Multiprocessors"
/// ACM Trans. Comput. Syst. 9(1), 21-65, 1991
///
/// NOTE: node and lock data should be embedded in other data structures
/// and properly aligned to cache lines

typedef struct mcs_node {
    struct mcs_node *next;
    unsigned int locked;
} mcs_node_t;

typedef struct {
    mcs_node_t *tail;
} mcs_lock_t;

void mcs_init(mcs_lock_t *lock);
void mcs_lock(mcs_lock_t *lock, mcs_node_t *self);
void mcs_unlock(mcs_lock_t *lock, mcs_node_t *self);

////////////////////////////////////////////////////////////////////////////////

/// Spinning barrier -- all CPUs have to reach the barrier

typedef struct {
    unsigned long cpu_mask;
    unsigned long spin_mask;
} __lock_aligned(LOCK_ALIGNMENT) spin_barrier_t;

/** initialize barrier for "num_cpus" parallel CPUs */
void spin_barrier_init(spin_barrier_t *barrier, uint32_t num_cpus);

/** wait for all CPUs to reach the barrier, returns 1 on the last CPU */
int spin_barrier_wait(spin_barrier_t *barrier, uint32_t curr_cpu);

////////////////////////////////////////////////////////////////////////////////

/// Brandenburg's phase-fair ticket-based RW-locks, ticket based (PF-T)
///
/// Björn B. Brandenburg, James H. Anderson:
/// "Spin-Based Reader-Writer Synchronization for Multiprocessor Real-Time Systems",
/// Real-Time Systems 46(1), 25-87, 2010
/// https://people.mpi-sws.org/~bbb/papers/pdf/rtsj11.pdf

typedef struct {
    union {
        unsigned int rin;
        struct {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
        uint8_t rin_lsb;
        uint8_t rin_other[3];
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
        uint8_t rin_other[3];
        uint8_t rin_lsb;
#else
#error "Unknown endianess"
#endif
        };
    } __lock_aligned(LOCK_ALIGNMENT);
    unsigned int rout __lock_aligned(LOCK_ALIGNMENT);
    unsigned int win __lock_aligned(LOCK_ALIGNMENT);
    unsigned int wout __lock_aligned(LOCK_ALIGNMENT);
} __lock_aligned(LOCK_ALIGNMENT) pft_lock_t;

void pft_init(pft_lock_t *l);
void pft_read_lock(pft_lock_t *l);
void pft_read_unlock(pft_lock_t *l);
void pft_write_lock(pft_lock_t *l);
void pft_write_unlock(pft_lock_t *l);

////////////////////////////////////////////////////////////////////////////////

/// Brandenburg's phase-fair ticket-based RW-locks, compact version (PF-C)
///
/// Björn B. Brandenburg, James H. Anderson:
/// "Spin-Based Reader-Writer Synchronization for Multiprocessor Real-Time Systems",
/// Real-Time Systems 46(1), 25-87, 2010
/// https://people.mpi-sws.org/~bbb/papers/pdf/rtsj11.pdf

typedef uint32_t pfc_lock_t;

void pfc_init(pfc_lock_t *l);
void pfc_read_lock(pfc_lock_t *l);
void pfc_read_unlock(pfc_lock_t *l);
void pfc_write_lock(pfc_lock_t *l);
void pfc_write_unlock(pfc_lock_t *l);

////////////////////////////////////////////////////////////////////////////////

/// SQDGL -- single queue dynamic group locks using an internal ticket lock

/** per-CPU FIFO queue node data, kept on its own cacheline */
typedef struct sqdgl1_node {
    /** pointer to next older node (or NULL at end of queue) */
    struct sqdgl1_node *next;

    /** number of pending conflicts; 0 means no conflicts */
    uint32_t conflicts;

    /** bitmap of resources */
    res_t resources;
} __lock_aligned(LOCK_ALIGNMENT) sqdgl1_node_t;

/** global lock data, kept on its own cacheline */
typedef struct {
    /** pointer to tail node in queue (or NULL if queue is empty) */
    struct sqdgl1_node *tail;

    /** internal ticket lock */
    ticket_spin_t lock;

    /** array of per-CPU queue nodes */
    sqdgl1_node_t c[MAX_CPUS];
} __lock_aligned(LOCK_ALIGNMENT) sqdgl1_lock_t;


/** initialize lock */
void sqdgl1_init(sqdgl1_lock_t *l);

/** lock resources "r" */
void sqdgl1_lock(sqdgl1_lock_t *l, uint32_t cpu, const res_t r);

/** unlock */
void sqdgl1_unlock(sqdgl1_lock_t *l, uint32_t cpu);

////////////////////////////////////////////////////////////////////////////////

/// SQDGL -- single queue dynamic group locks using an internal MCS lock

/** per-CPU FIFO queue node data, kept on its own cacheline */
typedef struct sqdgl2_node {
    /** pointer to next older node (or NULL at end of queue) */
    struct sqdgl2_node *next;

    /** number of pending conflicts; 0 means no conflicts */
    uint32_t conflicts;

    /** bitmap of resources */
    res_t resources;
} __lock_aligned(LOCK_ALIGNMENT) sqdgl2_node_t;

/** global lock data, kept on its own cacheline */
typedef struct {
    /** internal MCS lock */
    mcs_lock_t lock;

    /** pointer to tail node in queue (or NULL if queue is empty) */
    struct sqdgl2_node *tail;

    /** array of per-CPU queue nodes */
    sqdgl2_node_t c[MAX_CPUS];
} __lock_aligned(LOCK_ALIGNMENT) sqdgl2_lock_t;


/** initialize lock */
void sqdgl2_init(sqdgl2_lock_t *l);

/** lock resources "r" */
void sqdgl2_lock(sqdgl2_lock_t *l, uint32_t cpu, const res_t r);

/** unlock */
void sqdgl2_unlock(sqdgl2_lock_t *l, uint32_t cpu);

////////////////////////////////////////////////////////////////////////////////

/// RW-SQDGL -- single queue dynamic group locks (reader-writer variant)

/** per-CPU FIFO queue node data, kept on its own cacheline */
typedef struct rwsqdgl1_node {
    /** pointer to next older node (or NULL at end of queue) */
    struct rwsqdgl1_node *next;

    /** number of pending conflicts; 0 means no conflicts */
    uint32_t conflicts;

    /** bitmap of resources, one for readers, one for writers */
    res_t rw_resources;
    res_t w_resources;
} __lock_aligned(LOCK_ALIGNMENT) rwsqdgl1_node_t;

/** global lock data, kept on its own cacheline */
typedef struct {
    /** pointer to tail node in queue (or NULL if queue is empty) */
    struct rwsqdgl1_node *tail;

    /** internal ticket lock */
    ticket_spin_t lock;

    /** per-CPU node data */
    rwsqdgl1_node_t c[MAX_CPUS];
} __lock_aligned(LOCK_ALIGNMENT) rwsqdgl1_lock_t;


/** initialize lock */
void rwsqdgl1_init(rwsqdgl1_lock_t *l);

/** lock resources "r" for reading and "w" for writing */
void rwsqdgl1_lock(rwsqdgl1_lock_t *l, uint32_t self_cpu, const res_t r, const res_t w);

/** unlock */
void rwsqdgl1_unlock(rwsqdgl1_lock_t *l, uint32_t self_cpu, const res_t r, const res_t w);

////////////////////////////////////////////////////////////////////////////////

/// RW-SQDGL -- ... with MCS lock

/** per-CPU FIFO queue node data, kept on its own cacheline */
typedef struct rwsqdgl2_node {
    /** pointer to next older node (or NULL at end of queue) */
    struct rwsqdgl2_node *next;

    /** number of pending conflicts; 0 means no conflicts */
    uint32_t conflicts;

    /** bitmap of resources, one for readers, one for writers */
    res_t rw_resources;
    res_t w_resources;
} __lock_aligned(LOCK_ALIGNMENT) rwsqdgl2_node_t;

/** global lock data, kept on its own cacheline */
typedef struct {
    /** pointer to tail node in queue (or NULL if queue is empty) */
    struct rwsqdgl2_node *tail;

    /** internal MCS lock */
    mcs_lock_t lock;

    /** per-CPU node data */
    rwsqdgl2_node_t c[MAX_CPUS];
} __lock_aligned(LOCK_ALIGNMENT) rwsqdgl2_lock_t;


/** initialize lock */
void rwsqdgl2_init(rwsqdgl2_lock_t *l);

/** lock resources "r" for reading and "w" for writing */
void rwsqdgl2_lock(rwsqdgl2_lock_t *l, uint32_t self_cpu, const res_t r, const res_t w);

/** unlock */
void rwsqdgl2_unlock(rwsqdgl2_lock_t *l, uint32_t self_cpu, const res_t r, const res_t w);

////////////////////////////////////////////////////////////////////////////////

/// multi-resource TATAS
///
/// The number of resources are hardcoded to 64.
/// NOTE: on 32-bit architectures, we assume to have 64-bit atomics.
/// This is the case for both 32-bit x86 and ARM.

/** bitmap of resources (here: up to 64) */
typedef uint64_t tatas_res_t;

/** TATAS global lock */
typedef tatas_res_t __lock_aligned(LOCK_ALIGNMENT) tatas_lock_t;

/** initialize lock */
void tatas_init(tatas_lock_t *l);

/** lock resources "r" */
void tatas_lock(tatas_lock_t *l, tatas_res_t r);

/** unlock */
void tatas_unlock(tatas_lock_t *l, tatas_res_t r);

////////////////////////////////////////////////////////////////////////////////

/// array of spinlocks, uint32_t array for resources (compact memory layout)

/** array of spinlocks, all kept in same cache line */
typedef struct {
    struct {
        uint32_t ticket;
        uint32_t served;
    } a[RES_BITS];
} array1_lock_t;

/** initialize lock */
void array1_init(array1_lock_t *l);

/** lock resources "r" */
void array1_lock(array1_lock_t *l, const res_t r);

/** unlock */
void array1_unlock(array1_lock_t *l, const res_t r);

////////////////////////////////////////////////////////////////////////////////

/// array of spinlocks, uint32_t array for resources (one cacheline per lock)

/** array of spinlocks, each kept on its own cacheline */
typedef struct {
    struct {
        uint32_t ticket;
        uint32_t served;
    } __lock_aligned(LOCK_ALIGNMENT) a[RES_BITS];
} array2_lock_t;

/** initialize lock */
void array2_init(array2_lock_t *l);

/** lock resources "r" */
void array2_lock(array2_lock_t *l, const res_t r);

/** unlock */
void array2_unlock(array2_lock_t *l, const res_t r);

////////////////////////////////////////////////////////////////////////////////

/// lock-less array-based multi-resource lock (LLAB-MRLOCK)
/// NOTE: fifth variant of the protocol using load-acquire/store-release

/** per-CPU array element data, kept on its own cacheline */
typedef struct {
    /** drawn ticket, encodes the request state in the two LSBs */
    uint32_t drawn_ticket;
    uint32_t padding;

    /** bitmap of resources */
    res_t resources;
} __lock_aligned(LOCK_ALIGNMENT) llab5_node_t;

/** global lock data, kept on its own cacheline */
typedef struct {
    /** global ticket, incremented by four for each request */
    uint32_t global_ticket;

    /** number of CPUs (cached value) */
    uint32_t num_cpus;

    /** array of per-CPU queue nodes */
    llab5_node_t c[MAX_CPUS];
} __lock_aligned(LOCK_ALIGNMENT) llab5_lock_t;


/** initialize lock */
void llab5_init(llab5_lock_t *l, uint32_t num_cpus);

/** lock resources "r" */
void llab5_lock(llab5_lock_t *l, uint32_t cpu, const res_t r);

/** unlock */
void llab5_unlock(llab5_lock_t *l, uint32_t cpu);

////////////////////////////////////////////////////////////////////////////////

/// lock-less array-based multi-resource lock (LLAB-MRLOCK) + compact layout
/// NOTE: sixth variant of the protocol using load-acquire/store-release

/** per-CPU array element data, kept on its own cacheline */
typedef struct {
    /** drawn ticket, encodes the request state in the two LSBs */
    uint32_t drawn_ticket;
    uint32_t padding;

    /** bitmap of resources */
    res_t resources;
} llab6_node_t;

/** global lock data, kept on its own cacheline */
typedef struct {
    /** global ticket, incremented by four for each request */
    uint32_t global_ticket;

    /** number of CPUs (cached value) */
    uint32_t num_cpus;

    /** array of per-CPU queue nodes */
    llab6_node_t c[MAX_CPUS];
} __lock_aligned(LOCK_ALIGNMENT) llab6_lock_t;


/** initialize lock */
void llab6_init(llab6_lock_t *l, uint32_t num_cpus);

/** lock resources "r" */
void llab6_lock(llab6_lock_t *l, uint32_t cpu, const res_t r);

/** unlock */
void llab6_unlock(llab6_lock_t *l, uint32_t cpu);

////////////////////////////////////////////////////////////////////////////////

/// reader-writer lock-less array-based multi-resource lock (RW-LLAB-MRLOCK, based on LLAB5)

/** per-CPU array element data, kept on its own cacheline */
typedef struct {
    /** drawn ticket, encodes the request state in the two LSBs */
    uint32_t drawn_ticket;
    uint32_t padding;

    /** bitmap of resources, one for readers, one for writers */
    res_t rw_resources;
    res_t w_resources;
} __lock_aligned(LOCK_ALIGNMENT) rwllab1_per_cpu_t;

/** per-CPU array element data, kept on its own cacheline */
typedef struct {
    /** global ticket, incremented by four for each request */
    uint32_t global_ticket;

    /** number of CPUs (cached value) */
    uint32_t num_cpus;

    /** array of per-CPU queue nodes */
    rwllab1_per_cpu_t c[MAX_CPUS];
} __lock_aligned(LOCK_ALIGNMENT) rwllab1_lock_t;


/** initialize lock */
void rwllab1_init(rwllab1_lock_t *l, uint32_t num_cpus);

/** lock resources "r" for reading and "w" for writing */
void rwllab1_lock(rwllab1_lock_t *l, uint32_t cpu, const res_t r, const res_t w);

/** unlock */
void rwllab1_unlock(rwllab1_lock_t *l, uint32_t cpu);

////////////////////////////////////////////////////////////////////////////////

/// reader-writer lock-less array-based multi-resource lock (RW-LLAB-MRLOCK, based on LLAB6) + compact layout

/** per-CPU array element data, kept on its own cacheline */
typedef struct {
    /** drawn ticket, encodes the request state in the two LSBs */
    uint32_t drawn_ticket;
    uint32_t padding;

    /** bitmap of resources, one for readers, one for writers */
    res_t rw_resources;
    res_t w_resources;
} rwllab2_per_cpu_t;

/** per-CPU array element data, kept on its own cacheline */
typedef struct {
    /** global ticket, incremented by four for each request */
    uint32_t global_ticket;

    /** number of CPUs (cached value) */
    uint32_t num_cpus;

    /** array of per-CPU queue nodes */
    rwllab2_per_cpu_t c[MAX_CPUS];
} __lock_aligned(LOCK_ALIGNMENT) rwllab2_lock_t;


/** initialize lock */
void rwllab2_init(rwllab2_lock_t *l, uint32_t num_cpus);

/** lock resources "r" for reading and "w" for writing */
void rwllab2_lock(rwllab2_lock_t *l, uint32_t cpu, const res_t r, const res_t w);

/** unlock */
void rwllab2_unlock(rwllab2_lock_t *l, uint32_t cpu);

////////////////////////////////////////////////////////////////////////////////

/// Ward's RNLP
///
/// Bryan C. Ward, James H. Anderson:
/// "Fine-grained Multiprocessor Real-Time Locking with Improved Blocking"
/// In: Conference on Real-Time Networks and Systems (RTNS), 67-76, 2013
/// https://www.cs.unc.edu/~anderson/papers/rtns13_long.pdf
///
/// This version of RNLP is a simplified variant of the R/W-RNLP implementation
/// based on the following observation in the paper (end of Section 4 on DGLs):
///
///     If all concurrent resource accesses in a system are sup-
///     ported by using DGLs, then the implementation of the RNLP
///     can be greatly simplified. The timestamp-ordered queues be-
///     come simple FIFO queues, and there is no need for jobs to
///     "reserve" their position in any queue. This is due to the fact
///     that all enqueueing due to one request is done atomically.
///     Thus, in this case, not only is the number of system calls
///     reduced, but the execution time of any one system call is
///     likely lessened as well.
///
/// In this spirit, we simplify the implementation to support non-nested
/// requests to multiple resources. With this, the implementation
/// just comprises FIFO queues for each resource, with head and tail pointers,
/// and we simply use our current CPU ID as queue nodes.


/** constant Q: number of queues (matches number of bits in res_t) */
#define RNLP1_Q RES_BITS

/** constant M: number of CPUs */
#define RNLP1_M MAX_CPUS

/** shared variables in the RNLP implementation */
typedef struct {
    ticket_spin_t lock;
    uint32_t queue[RNLP1_Q][RNLP1_M];
    uint32_t head[RNLP1_Q];
    uint32_t tail[RNLP1_Q];
} __lock_aligned(LOCK_ALIGNMENT) rnlp1_global_t;

/** initialize lock */
void rnlp1_init(rnlp1_global_t *l);

/** lock resources "resources" */
void rnlp1_lock(rnlp1_global_t *l, uint32_t proc, const res_t resources);

/** unlock resources "resources" */
void rnlp1_unlock(rnlp1_global_t *l, uint32_t proc, const res_t resources);

////////////////////////////////////////////////////////////////////////////////

/// Ward's RNLP (like RNLP1, using an MCS lock)

/** constant Q: number of queues (matches number of bits in res_t) */
#define RNLP2_Q RES_BITS

/** constant M: number of CPUs */
#define RNLP2_M MAX_CPUS

/** shared variables in the RNLP implementation */
typedef struct {
    mcs_lock_t lock;
    uint32_t queue[RNLP2_Q][RNLP2_M];
    uint32_t head[RNLP2_Q];
    uint32_t tail[RNLP2_Q];
} __lock_aligned(LOCK_ALIGNMENT) rnlp2_global_t;

/** initialize lock */
void rnlp2_init(rnlp2_global_t *l);

/** lock resources "resources" */
void rnlp2_lock(rnlp2_global_t *l, uint32_t proc, const res_t resources);

/** unlock resources "resources" */
void rnlp2_unlock(rnlp2_global_t *l, uint32_t proc, const res_t resources);

////////////////////////////////////////////////////////////////////////////////

/// Nemitz's RNLP (single-queue implementation using a ticket lock (original))
///
/// Catherine E. Nemitz, Tanya Amert, James H. Anderson,
/// "Real-time multiprocessor locks with nesting: optimizing the common case",
/// In: International Conference on Real Time Networks and Systems (RTNS), 38--47, 2017
/// https://www.cs.unc.edu/~anderson/papers/rtsj19app.pdf
/// https://cs.unc.edu/~anderson/papers/fast_rwrnlp_code.tar.gz

/** per-CPU variables */
typedef struct rnlp3_percpu {
    res_t resources;
    struct rnlp3_percpu *next;
    uint32_t waiting;
} __lock_aligned(LOCK_ALIGNMENT) rnlp3_percpu_t;

/** shared variables in the RNLP implementation */
typedef struct {
    ticket_spin_t lock;
    struct rnlp3_percpu *head;
    struct rnlp3_percpu *tail;
    res_t locked;
    res_t unavailable;
    rnlp3_percpu_t percpu[MAX_CPUS];
} __lock_aligned(LOCK_ALIGNMENT) rnlp3_global_t;

/** initialize lock */
void rnlp3_init(rnlp3_global_t *l);

/** lock resources "resources" */
void rnlp3_lock(rnlp3_global_t *l, uint32_t proc, const res_t resources);

/** unlock resources "resources" */
void rnlp3_unlock(rnlp3_global_t *l, uint32_t proc, const res_t resources);

////////////////////////////////////////////////////////////////////////////////

/// Nemitz's RNLP (single-queue implementation using an MCS lock)
///
/// Catherine E. Nemitz, Tanya Amert, James H. Anderson,
/// "Real-time multiprocessor locks with nesting: optimizing the common case",
/// In: International Conference on Real Time Networks and Systems (RTNS), 38--47, 2017
/// https://www.cs.unc.edu/~anderson/papers/rtsj19app.pdf
/// https://cs.unc.edu/~anderson/papers/fast_rwrnlp_code.tar.gz

/** per-CPU variables */
typedef struct rnlp4_percpu {
    res_t resources;
    struct rnlp4_percpu *next;
    uint32_t waiting;
} __lock_aligned(LOCK_ALIGNMENT) rnlp4_percpu_t;

/** shared variables in the RNLP implementation */
typedef struct {
    mcs_lock_t lock;
    struct rnlp4_percpu *head;
    struct rnlp4_percpu *tail;
    res_t locked;
    res_t unavailable;
    rnlp4_percpu_t percpu[MAX_CPUS];
} __lock_aligned(LOCK_ALIGNMENT) rnlp4_global_t;

/** initialize lock */
void rnlp4_init(rnlp4_global_t *l);

/** lock resources "resources" */
void rnlp4_lock(rnlp4_global_t *l, uint32_t proc, const res_t resources);

/** unlock resources "resources" */
void rnlp4_unlock(rnlp4_global_t *l, uint32_t proc, const res_t resources);

////////////////////////////////////////////////////////////////////////////////

/// Ward's RW-RNLP (using PF-C and a ticket lock)
///
/// Bryan C. Ward, James H. Anderson:
/// "Multi-Resource Real-Time Reader/Writer Locks for Multiprocessors"
/// In: International Parallel and Distributed Processing Symposium (IPDPS), 177-186, 2014
/// https://www.cs.unc.edu/~bcw/pubs/ipdps14_long.pdf
///
/// Or from Ward's dissertation, pages 104 to 107

/** constant Q: number of queues (matches number of bits in res_t) */
#define RWRNLP1_Q RES_BITS

/** constant M: number of CPUs */
#define RWRNLP1_M MAX_CPUS

/** request-struct members */
typedef struct {
    res_t resources;
    uint8_t status; /* WAITING, ACQUIRED, ENTITLED */
    uint8_t type;   /* READ, WRITE */
    uint16_t padding;
    uint32_t proc;  /* originating processor */
} __lock_aligned(LOCK_ALIGNMENT) rwrnlp1_request_t;

/** shared variables in the R/W RNLP implementation */
typedef struct {
    pfc_lock_t pflock;
    ticket_spin_t mlock;
    res_t unavailable, wentitled, wlocked;
    rwrnlp1_request_t *wqueue[RWRNLP1_Q][RWRNLP1_M];
    uint32_t whead[RWRNLP1_Q];
    uint32_t wtail[RWRNLP1_Q];
    uint32_t entry[RWRNLP1_M];
    uint32_t exit[RWRNLP1_M];
    rwrnlp1_request_t requests[RWRNLP1_M];
} rwrnlp1_global_t;

/** initialize lock */
void rwrnlp1_init(rwrnlp1_global_t *l);

/** read lock resources "r" */
void rwrnlp1_read_lock(rwrnlp1_global_t *l, uint32_t proc, const res_t resources);

/** read unlock resources "r" */
void rwrnlp1_read_unlock(rwrnlp1_global_t *l, uint32_t proc, const res_t resources);

/** write lock resources "r" */
void rwrnlp1_write_lock(rwrnlp1_global_t *l, uint32_t proc, const res_t resources);

/** write unlock resources "r" */
void rwrnlp1_write_unlock(rwrnlp1_global_t *l, uint32_t proc, const res_t resources);

////////////////////////////////////////////////////////////////////////////////

/// Ward's RW-RNLP (using PF-C and an MCS lock)
///
/// Bryan C. Ward, James H. Anderson:
/// "Multi-Resource Real-Time Reader/Writer Locks for Multiprocessors"
/// In: International Parallel and Distributed Processing Symposium (IPDPS), 177-186, 2014
/// https://www.cs.unc.edu/~bcw/pubs/ipdps14_long.pdf
///
/// Or from Ward's dissertation, pages 104 to 107

/** constant Q: number of queues (matches number of bits in res_t) */
#define RWRNLP2_Q RES_BITS

/** constant M: number of CPUs */
#define RWRNLP2_M MAX_CPUS

/** request-struct members */
typedef struct {
    res_t resources;
    uint8_t status; /* WAITING, ACQUIRED, ENTITLED */
    uint8_t type;   /* READ, WRITE */
    uint16_t padding;
    uint32_t proc;  /* originating processor */
} __lock_aligned(LOCK_ALIGNMENT) rwrnlp2_request_t;

/** shared variables in the R/W RNLP implementation */
typedef struct {
    pfc_lock_t pflock;
    mcs_lock_t mlock;
    res_t unavailable, wentitled, wlocked;
    rwrnlp2_request_t *wqueue[RWRNLP2_Q][RWRNLP2_M];
    uint32_t whead[RWRNLP2_Q];
    uint32_t wtail[RWRNLP2_Q];
    uint32_t entry[RWRNLP2_M];
    uint32_t exit[RWRNLP2_M];
    rwrnlp2_request_t requests[RWRNLP2_M];
} rwrnlp2_global_t;

/** initialize lock */
void rwrnlp2_init(rwrnlp2_global_t *l);

/** read lock resources "r" */
void rwrnlp2_read_lock(rwrnlp2_global_t *l, uint32_t proc, const res_t resources);

/** read unlock resources "r" */
void rwrnlp2_read_unlock(rwrnlp2_global_t *l, uint32_t proc, const res_t resources);

/** write lock resources "r" */
void rwrnlp2_write_lock(rwrnlp2_global_t *l, uint32_t proc, const res_t resources);

/** write unlock resources "r" */
void rwrnlp2_write_unlock(rwrnlp2_global_t *l, uint32_t proc, const res_t resources);

////////////////////////////////////////////////////////////////////////////////

/// Ward's RW-RNLP (using PF-T and an MCS lock)
///
/// Bryan C. Ward, James H. Anderson:
/// "Multi-Resource Real-Time Reader/Writer Locks for Multiprocessors"
/// In: International Parallel and Distributed Processing Symposium (IPDPS), 177-186, 2014
/// https://www.cs.unc.edu/~bcw/pubs/ipdps14_long.pdf
///
/// Or from Ward's dissertation, pages 104 to 107

/** constant Q: number of queues (matches number of bits in res_t) */
#define RWRNLP3_Q RES_BITS

/** constant M: number of CPUs */
#define RWRNLP3_M MAX_CPUS

/** request-struct members */
typedef struct {
    res_t resources;
    uint8_t status; /* WAITING, ACQUIRED, ENTITLED */
    uint8_t type;   /* READ, WRITE */
    uint16_t padding;
    uint32_t proc;  /* originating processor */
} __lock_aligned(LOCK_ALIGNMENT) rwrnlp3_request_t;

/** shared variables in the R/W RNLP implementation */
typedef struct {
    pft_lock_t pflock;
    mcs_lock_t mlock;
    res_t unavailable, wentitled, wlocked;
    rwrnlp3_request_t *wqueue[RWRNLP3_Q][RWRNLP3_M];
    uint32_t whead[RWRNLP3_Q];
    uint32_t wtail[RWRNLP3_Q];
    uint32_t entry[RWRNLP3_M];
    uint32_t exit[RWRNLP3_M];
    rwrnlp3_request_t requests[RWRNLP3_M];
} rwrnlp3_global_t;

/** initialize lock */
void rwrnlp3_init(rwrnlp3_global_t *l);

/** read lock resources "r" */
void rwrnlp3_read_lock(rwrnlp3_global_t *l, uint32_t proc, const res_t resources);

/** read unlock resources "r" */
void rwrnlp3_read_unlock(rwrnlp3_global_t *l, uint32_t proc, const res_t resources);

/** write lock resources "r" */
void rwrnlp3_write_lock(rwrnlp3_global_t *l, uint32_t proc, const res_t resources);

/** write unlock resources "r" */
void rwrnlp3_write_unlock(rwrnlp3_global_t *l, uint32_t proc, const res_t resources);

////////////////////////////////////////////////////////////////////////////////

/// Nemitz and Ward's Uniform C-RNLP with an internal ticket lock
///
/// Catherine E. Jarrett, Bryan C. Ward, and James H. Anderson:
/// "A Contention-Sensitive Fine-Grained Locking Protocol
///  for Multiprocessor Real-Time Systems"
/// In: International Conference on Real Time Networks and Systems (RTNS), 3-12, 2015
/// https://www.cs.unc.edu/~anderson/papers/rtns15a.pdf
///
/// Or from Nemitz's dissertation, pages 92 to 94
/// See also: https://www.cs.unc.edu/~jarretc/dissertation/

/* entries in the table == maximum number of outstanding requests */
#define UCRNLP1_SIZE (2*MAX_CPUS)

/** shared variables in the C-RNLP implementation */
typedef struct {
    ticket_spin_t lock;
    uint32_t pending_requests;
    uint32_t head;
    uint8_t enabled[UCRNLP1_SIZE];
    uint8_t blocked[UCRNLP1_SIZE];
    res_t table[UCRNLP1_SIZE];
} __lock_aligned(LOCK_ALIGNMENT) ucrnlp1_global_t;

/** initialize lock */
void ucrnlp1_init(ucrnlp1_global_t *l);

/** lock resources "r" */
uint32_t ucrnlp1_lock(ucrnlp1_global_t *l, const res_t resources);

/** unlock resources "r" */
void ucrnlp1_unlock(ucrnlp1_global_t *l, const res_t resources, uint32_t next);

////////////////////////////////////////////////////////////////////////////////

/// Nemitz and Ward's Uniform C-RNLP with an internal MCS lock

/* entries in the table == maximum number of outstanding requests */
#define UCRNLP2_SIZE (2*MAX_CPUS)

/** shared variables in the C-RNLP implementation */
typedef struct {
    mcs_lock_t lock;
    uint32_t pending_requests;
    uint32_t head;
    uint8_t enabled[UCRNLP2_SIZE];
    uint8_t blocked[UCRNLP2_SIZE];
    res_t table[UCRNLP2_SIZE];
} __lock_aligned(LOCK_ALIGNMENT) ucrnlp2_global_t;

/** initialize lock */
void ucrnlp2_init(ucrnlp2_global_t *l);

/** lock resources "r" */
uint32_t ucrnlp2_lock(ucrnlp2_global_t *l, const res_t resources);

/** unlock resources "r" */
void ucrnlp2_unlock(ucrnlp2_global_t *l, const res_t resources, uint32_t next);

////////////////////////////////////////////////////////////////////////////////

/// MRLock -- Multi-resource Locks
///
/// Deli Zhang, Brendan Lynch, Damian Dechev
/// "Queue-Based and Adaptive Lock Algorithms for Scalable Resource Allocation
///  on Shared-Memory Multiprocessors""
/// Int J Parallel Prog; DOI 10.1007/s10766-014-0317-6, 2014

/** Number of Cells (must be a power of two, we use 4xNUM_CPUS) */
#define MRLOCK_CELLS (4*MAX_CPUS)

typedef struct {
        uint32_t seq;
        res_t bits;
} __lock_aligned(LOCK_ALIGNMENT) mrlock_cell_t;

typedef struct {
        mrlock_cell_t buffer[MRLOCK_CELLS];
        uint32_t mask;
        uint32_t head;
        uint32_t tail;
} __lock_aligned(LOCK_ALIGNMENT) mrlock_global_t;

/** initialize lock */
void mrlock_init(mrlock_global_t *l);

/** lock resources "r"; returns a handle for the unlock operation */
uint32_t mrlock_lock(mrlock_global_t *l, res_t r);

/** unlock resources "r"; the caller previously locked the handle "h" */
void mrlock_unlock(mrlock_global_t *l, uint32_t h);

#endif
