
##### drone2 scenario on ARM

set terminal postscript eps size 6.0,3.0 enhanced color \
font ',16' linewidth 2
set output 'bench_drone2.eps'

# no borders of the graph on the right and top
set border 3

# Labels for the axis
set grid
set xlabel "Random taskset (ordered by increasing execution time)"
set xrange [0:30]

set ylabel "Overall execution time [{/Symbol m}s]" offset 1.0, 0.0
set yrange [0:7000]
set key inside top left box width 1.0 height 0.5 font ",14"


RUNS=1024
TS_US=600 # CPU cycles per usec (600 MHz)

plot \
     'bench_drone2_tatas.tmp'    using ($2):($4/TS_US) title 'just WCET' with points pt 9 lc 2, \
     'bench_drone2_mcs.tmp'      using ($2):($3/TS_US) title 'global MCS lock' with points pt 11 lc 11, \
     'bench_drone2_tatas.tmp'    using ($2):($3/TS_US) title 'TATAS'    with points pt 4 lc 0, \
     'bench_drone2_mrlock.tmp'   using ($2):($3/TS_US) title 'MRLock'   with points pt 6 lc 7, \
     'bench_drone2_rnlp4.tmp'    using ($2):($3/TS_US) title 'DGL'      with points pt 1 lc 2, \
     'bench_drone2_rwsqdgl2.tmp' using ($2):($3/TS_US) title 'R/W-DGL'  with points pt 2 lc 2, \
     'bench_drone2_llab6.tmp'    using ($2):($3/TS_US) title 'LLAB'     with points pt 1 lc 4, \
     'bench_drone2_rwllab2.tmp'  using ($2):($3/TS_US) title 'R/W-LLAB'  with points pt 2 lc 4, \

     #'bench_drone2_llab3.tmp'    using ($2):($3/TS_US) title 'LLAB3'    with points pt 11 lc 11, \
     #'bench_drone2_rwrnlp.tmp'   using ($2):($3/TS_US) title 'RW-RNLP'  with points pt 7 lc 7, \
     #'bench_drone2_sqdgl2.tmp'   using ($2):($3/TS_US) title 'SQ-DGL'    with points pt 6 lc 5, \
     #'bench_drone2_rwsqdgl2.tmp' using ($2):($3/TS_US) title 'RW-SQ-DGL' with points pt 7 lc 5, \
