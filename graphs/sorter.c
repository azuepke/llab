/*
 * sorter.c
 *
 * azuepke, 2020-10-18: initial
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define TESTS 12

struct samples {
	struct samples *next;
	struct {
		char name[20];
		unsigned int t1;
	} data[TESTS];
	unsigned int seed;
	unsigned int t2;
};

int main(int argc, char *argv[])
{
	char name[20];
	unsigned int seed;
	unsigned int t1;
	unsigned int t2;
	int ret;

	struct samples *s = NULL;
	struct samples *r = NULL;
	unsigned int curr_seed = -1;
	unsigned int slot = 0;

	while (1) {
		ret = scanf("%s %d %d %d\n", name, &seed, &t1, &t2);
		if (ret == EOF) {
			break;
		}
		if (ret == 4) {
			if (seed != curr_seed) {
				/* append to new set */
				s = calloc(1, sizeof(*s));
				if (r != NULL) {
					/* append to list */
					struct samples **x;
					for (x = &r; *x != NULL; x = &(*x)->next) {
						if (t2 < (*x)->t2) {
							s->next = (*x);
							break;
						}
					}
					*x = s;
				} else {
					r = s;
				}
				curr_seed = seed;
				slot = 0;
				s->seed = seed;
				s->t2 = t2;
			} else {
				/* append to current set */
				assert(s->t2 == t2);
			}
			if (slot < TESTS) {
				strcpy(s->data[slot].name, name);
				s->data[slot].t1 = t1;
				slot++;
			}
		}
	}

	unsigned int new_seed = 1;
	for (s = r; s != NULL; s = s->next) {
		for (unsigned int i = 0; i < TESTS; i++) {
			printf("%s %d %d %d\n", s->data[i].name, new_seed, s->data[i].t1, s->t2);
		}
		new_seed++;
	}

	return 0;
}
