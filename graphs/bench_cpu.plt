
##### uncontended_parallel on ARM  and x86 side by side

set terminal postscript eps size 6.0,3.0 enhanced color \
font ',16' linewidth 2
set output 'bench_cpu.eps'

# no borders of the graph on the right and top
set border 3

set multiplot layout 1,2 columnsfirst

set size 0.3,1
set origin 0,0

set label "A" at -1,6.0 font ",22"

# Labels for the axis
set grid
set xlabel "Number of CPU cores (ARM)"
set xrange [0:4]
set xtics 1
set ylabel "Average execution time [{/Symbol m}s]" offset 1.0, 0.0
set yrange [0:6.0]
set key inside top left box width 1.0 height 0.5 font ",14"


RUNS=1024
TS_US=600 # CPU cycles per usec (600 MHz)

plot \
     'bench_arm_tatas.tmp'    using 2:($5/RUNS/TS_US) title 'TATAS'     with points pt 4 lc 0, \
     'bench_arm_mrlock.tmp'   using 2:($5/RUNS/TS_US) title 'MRLock'    with points pt 6 lc 7, \
     'bench_arm_rnlp4.tmp'    using 2:($5/RUNS/TS_US) title 'DGL'       with points pt 1 lc 2, \
     'bench_arm_rwsqdgl2.tmp' using 2:($5/RUNS/TS_US) title 'R/W-DGL'   with points pt 2 lc 2, \
     'bench_arm_rwrnlp3.tmp'  using 2:($5/RUNS/TS_US) title 'R/W-RNLP'  with points pt 2 lc 6, \
     'bench_arm_llab6.tmp'    using 2:($5/RUNS/TS_US) title 'LLAB'      with points pt 1 lc 4, \
     'bench_arm_rwllab2.tmp'  using 2:($5/RUNS/TS_US) title 'R/W-LLAB'  with points pt 2 lc 4, \

     #'bench_arm_ucrnlp2.tmp'  using 2:($5/RUNS/TS_US) title 'ucrnlp.mcs'     with points pt 2 lc 3, \

unset label

################################################################################

set size 0.7,1
set origin 0.3,0

set label "B" at -7.2,240 font ",22"

# Labels for the axis
set grid
set xlabel "Number of CPU cores/hardware threads (Intel)"
set xrange [0:64]
set xtics 16
set ylabel "Average execution time [{/Symbol m}s]" offset 1.0, 0.0
set yrange [0:240]
set key off


RUNS=1024
TS_US=2100 # CPU cycles per usec (2.1 GHz)

plot \
     'bench_intel_tatas.tmp'    using 2:($5/RUNS/TS_US) title 'TATAS'     with points pt 4 lc 0, \
     'bench_intel_mrlock.tmp'   using 2:($5/RUNS/TS_US) title 'MRLock'    with points pt 6 lc 7, \
     'bench_intel_rnlp4.tmp'    using 2:($5/RUNS/TS_US) title 'DGL'       with points pt 1 lc 2, \
     'bench_intel_rwsqdgl2.tmp' using 2:($5/RUNS/TS_US) title 'R/W-DGL'   with points pt 2 lc 2, \
     'bench_intel_rwrnlp3.tmp'  using 2:($5/RUNS/TS_US) title 'R/W-RNLP'  with points pt 2 lc 6, \
     'bench_intel_llab6.tmp'    using 2:($5/RUNS/TS_US) title 'LLAB'      with points pt 1 lc 4, \
     'bench_intel_rwllab2.tmp'  using 2:($5/RUNS/TS_US) title 'R/W-LLAB'  with points pt 2 lc 4, \

     #'bench_intel_ucrnlp2.tmp'  using 2:($5/RUNS/TS_US) title 'ucrnlp.mcs'     with points pt 2 lc 3, \

unset label

################################################################################

set object rectangle from screen 0.402,0.632 to screen 0.652,0.935 behind fillcolor rgb 'white' fillstyle solid border 15
set size 0.300,0.350
set origin 0.375,0.610

# Labels for the axis
set grid
unset xlabel
set xrange [0:16]
unset xtics
unset ylabel
set yrange [0:10.7]
set key off


RUNS=1024
TS_US=2100 # CPU cycles per usec (2.1 GHz)

plot \
     'bench_intel_tatas.tmp'    using 2:($5/RUNS/TS_US) title 'TATAS'     with points pt 4 lc 0, \
     'bench_intel_mrlock.tmp'   using 2:($5/RUNS/TS_US) title 'MRLock'    with points pt 6 lc 7, \
     'bench_intel_rnlp4.tmp'    using 2:($5/RUNS/TS_US) title 'DGL'       with points pt 1 lc 2, \
     'bench_intel_rwsqdgl2.tmp' using 2:($5/RUNS/TS_US) title 'R/W-DGL'   with points pt 2 lc 2, \
     'bench_intel_rwrnlp3.tmp'  using 2:($5/RUNS/TS_US) title 'R/W-RNLP'  with points pt 2 lc 6, \
     'bench_intel_llab6.tmp'    using 2:($5/RUNS/TS_US) title 'LLAB'      with points pt 1 lc 4, \
     'bench_intel_rwllab2.tmp'  using 2:($5/RUNS/TS_US) title 'R/W-LLAB'  with points pt 2 lc 4, \

     #'bench_intel_ucrnlp2.tmp'  using 2:($5/RUNS/TS_US) title 'ucrnlp.mcs'     with points pt 2 lc 3, \

unset multiplot
