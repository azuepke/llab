#!/bin/sh

# prevent warnings
touch dump_intel.txt
touch dump_arm.txt
touch dump_drone2.txt

# split dump.txt for Intel
grep '^uncontended_isolation' dump_intel.txt > bench_intel_uncontended.tmp.csv
grep '^uncontended_parallel' dump_intel.txt | grep ';sspin_tas'       | tr ';' ' ' > bench_intel_sspin_tas.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';sspin_swap'      | tr ';' ' ' > bench_intel_sspin_swap.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';sspin_cas'       | tr ';' ' ' > bench_intel_sspin_cas.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';sspin_test_tas'  | tr ';' ' ' > bench_intel_sspin_test_tas.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';sspin_test_swap' | tr ';' ' ' > bench_intel_sspin_test_swap.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';sspin_test_cas'  | tr ';' ' ' > bench_intel_sspin_test_cas.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';ticket'   | tr ';' ' ' > bench_intel_ticket.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rticket1' | tr ';' ' ' > bench_intel_rticket1.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rticket2' | tr ';' ' ' > bench_intel_rticket2.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rticket3' | tr ';' ' ' > bench_intel_rticket3.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rticket4' | tr ';' ' ' > bench_intel_rticket4.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';wticket1' | tr ';' ' ' > bench_intel_wticket1.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';wticket2' | tr ';' ' ' > bench_intel_wticket2.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';wticket3' | tr ';' ' ' > bench_intel_wticket3.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';wticket4' | tr ';' ' ' > bench_intel_wticket4.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rwticket1'| tr ';' ' ' > bench_intel_rwticket1.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rwticket2'| tr ';' ' ' > bench_intel_rwticket2.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rwticket3'| tr ';' ' ' > bench_intel_rwticket3.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rwticket4'| tr ';' ' ' > bench_intel_rwticket4.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';wdticket2'| tr ';' ' ' > bench_intel_wdticket2.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';wdticket3'| tr ';' ' ' > bench_intel_wdticket3.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';wdticket4'| tr ';' ' ' > bench_intel_wdticket4.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';mcs'      | tr ';' ' ' > bench_intel_mcs.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rmcs1'    | tr ';' ' ' > bench_intel_rmcs1.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rmcs2'    | tr ';' ' ' > bench_intel_rmcs2.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rmcs3'    | tr ';' ' ' > bench_intel_rmcs3.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rmcs4'    | tr ';' ' ' > bench_intel_rmcs4.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';wmcs1'    | tr ';' ' ' > bench_intel_wmcs1.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';wmcs2'    | tr ';' ' ' > bench_intel_wmcs2.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';wmcs3'    | tr ';' ' ' > bench_intel_wmcs3.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';wmcs4'    | tr ';' ' ' > bench_intel_wmcs4.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rwmcs1'   | tr ';' ' ' > bench_intel_rwmcs1.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rwmcs2'   | tr ';' ' ' > bench_intel_rwmcs2.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rwmcs3'   | tr ';' ' ' > bench_intel_rwmcs3.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rwmcs4'   | tr ';' ' ' > bench_intel_rwmcs4.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';wdmcs2'   | tr ';' ' ' > bench_intel_wdmcs2.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';wdmcs3'   | tr ';' ' ' > bench_intel_wdmcs3.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';wdmcs4'   | tr ';' ' ' > bench_intel_wdmcs4.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';pftr'     | tr ';' ' ' > bench_intel_pftr.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';pftw'     | tr ';' ' ' > bench_intel_pftw.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';pfcr'     | tr ';' ' ' > bench_intel_pfcr.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';pfcw'     | tr ';' ' ' > bench_intel_pfcw.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';tatas'    | tr ';' ' ' > bench_intel_tatas.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';array1'   | tr ';' ' ' > bench_intel_array1.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';array2'   | tr ';' ' ' > bench_intel_array2.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rnlp1'    | tr ';' ' ' > bench_intel_rnlp1.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rnlp2'    | tr ';' ' ' > bench_intel_rnlp2.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rnlp3'    | tr ';' ' ' > bench_intel_rnlp3.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rnlp4'    | tr ';' ' ' > bench_intel_rnlp4.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rwrnlp1w' | tr ';' ' ' > bench_intel_rwrnlp1.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rwrnlp2w' | tr ';' ' ' > bench_intel_rwrnlp2.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rwrnlp3w' | tr ';' ' ' > bench_intel_rwrnlp3.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';sqdgl1'   | tr ';' ' ' > bench_intel_sqdgl1.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';sqdgl2'   | tr ';' ' ' > bench_intel_sqdgl2.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rwsqdgl1' | tr ';' ' ' > bench_intel_rwsqdgl1.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rwsqdgl2' | tr ';' ' ' > bench_intel_rwsqdgl2.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';llab5'    | tr ';' ' ' > bench_intel_llab5.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';llab6'    | tr ';' ' ' > bench_intel_llab6.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rwllab1'  | tr ';' ' ' > bench_intel_rwllab1.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';rwllab2'  | tr ';' ' ' > bench_intel_rwllab2.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';ucrnlp1'  | tr ';' ' ' > bench_intel_ucrnlp1.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';ucrnlp2'  | tr ';' ' ' > bench_intel_ucrnlp2.tmp
grep '^uncontended_parallel' dump_intel.txt | grep ';mrlock'   | tr ';' ' ' > bench_intel_mrlock.tmp

# split dump.txt for arm
grep '^uncontended_isolation' dump_arm.txt > bench_arm_uncontended.tmp.csv
grep '^uncontended_parallel' dump_arm.txt | grep ';sspin_tas'       | tr ';' ' ' > bench_arm_sspin_tas.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';sspin_swap'      | tr ';' ' ' > bench_arm_sspin_swap.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';sspin_cas'       | tr ';' ' ' > bench_arm_sspin_cas.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';sspin_test_tas'  | tr ';' ' ' > bench_arm_sspin_test_tas.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';sspin_test_swap' | tr ';' ' ' > bench_arm_sspin_test_swap.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';sspin_test_cas'  | tr ';' ' ' > bench_arm_sspin_test_cas.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';ticket'   | tr ';' ' ' > bench_arm_ticket.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rticket1' | tr ';' ' ' > bench_arm_rticket1.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rticket2' | tr ';' ' ' > bench_arm_rticket2.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rticket3' | tr ';' ' ' > bench_arm_rticket3.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rticket4' | tr ';' ' ' > bench_arm_rticket4.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';wticket1' | tr ';' ' ' > bench_arm_wticket1.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';wticket2' | tr ';' ' ' > bench_arm_wticket2.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';wticket3' | tr ';' ' ' > bench_arm_wticket3.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';wticket4' | tr ';' ' ' > bench_arm_wticket4.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rwticket1'| tr ';' ' ' > bench_arm_rwticket1.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rwticket2'| tr ';' ' ' > bench_arm_rwticket2.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rwticket3'| tr ';' ' ' > bench_arm_rwticket3.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rwticket4'| tr ';' ' ' > bench_arm_rwticket4.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';wdticket2'| tr ';' ' ' > bench_arm_wdticket2.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';wdticket3'| tr ';' ' ' > bench_arm_wdticket3.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';wdticket4'| tr ';' ' ' > bench_arm_wdticket4.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';mcs'      | tr ';' ' ' > bench_arm_mcs.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rmcs1'    | tr ';' ' ' > bench_arm_rmcs1.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rmcs2'    | tr ';' ' ' > bench_arm_rmcs2.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rmcs3'    | tr ';' ' ' > bench_arm_rmcs3.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rmcs4'    | tr ';' ' ' > bench_arm_rmcs4.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';wmcs1'    | tr ';' ' ' > bench_arm_wmcs1.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';wmcs2'    | tr ';' ' ' > bench_arm_wmcs2.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';wmcs3'    | tr ';' ' ' > bench_arm_wmcs3.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';wmcs4'    | tr ';' ' ' > bench_arm_wmcs4.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rwmcs1'   | tr ';' ' ' > bench_arm_rwmcs1.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rwmcs2'   | tr ';' ' ' > bench_arm_rwmcs2.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rwmcs3'   | tr ';' ' ' > bench_arm_rwmcs3.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rwmcs4'   | tr ';' ' ' > bench_arm_rwmcs4.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';wdmcs2'   | tr ';' ' ' > bench_arm_wdmcs2.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';wdmcs3'   | tr ';' ' ' > bench_arm_wdmcs3.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';wdmcs4'   | tr ';' ' ' > bench_arm_wdmcs4.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';pftr'     | tr ';' ' ' > bench_arm_pftr.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';pftw'     | tr ';' ' ' > bench_arm_pftw.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';pfcr'     | tr ';' ' ' > bench_arm_pfcr.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';pfcw'     | tr ';' ' ' > bench_arm_pfcw.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';tatas'    | tr ';' ' ' > bench_arm_tatas.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';array1'   | tr ';' ' ' > bench_arm_array1.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';array2'   | tr ';' ' ' > bench_arm_array2.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rnlp1'    | tr ';' ' ' > bench_arm_rnlp1.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rnlp2'    | tr ';' ' ' > bench_arm_rnlp2.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rnlp3'    | tr ';' ' ' > bench_arm_rnlp3.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rnlp4'    | tr ';' ' ' > bench_arm_rnlp4.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rwrnlp1w' | tr ';' ' ' > bench_arm_rwrnlp1.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rwrnlp2w' | tr ';' ' ' > bench_arm_rwrnlp2.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rwrnlp3w' | tr ';' ' ' > bench_arm_rwrnlp3.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';sqdgl1'   | tr ';' ' ' > bench_arm_sqdgl1.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';sqdgl2'   | tr ';' ' ' > bench_arm_sqdgl2.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rwsqdgl1' | tr ';' ' ' > bench_arm_rwsqdgl1.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rwsqdgl2' | tr ';' ' ' > bench_arm_rwsqdgl2.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';llab5'    | tr ';' ' ' > bench_arm_llab5.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';llab6'    | tr ';' ' ' > bench_arm_llab6.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rwllab1'  | tr ';' ' ' > bench_arm_rwllab1.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';rwllab2'  | tr ';' ' ' > bench_arm_rwllab2.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';ucrnlp1'  | tr ';' ' ' > bench_arm_ucrnlp1.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';ucrnlp2'  | tr ';' ' ' > bench_arm_ucrnlp2.tmp
grep '^uncontended_parallel' dump_arm.txt | grep ';mrlock'   | tr ';' ' ' > bench_arm_mrlock.tmp

# split dump.txt for Drone2
gcc -o sorter sorter.c
grep '^drone2_' dump_drone2.txt | tr ',' ' ' | ./sorter > bench_drone2.tmp

grep '^drone2_nolock'   bench_drone2.tmp | tr ',' ' ' > bench_drone2_nolock.tmp
grep '^drone2_ticket'   bench_drone2.tmp | tr ',' ' ' > bench_drone2_ticket.tmp
grep '^drone2_mcs'      bench_drone2.tmp | tr ',' ' ' > bench_drone2_mcs.tmp
grep '^drone2_tatas'    bench_drone2.tmp | tr ',' ' ' > bench_drone2_tatas.tmp
grep '^drone2_rnlp2'    bench_drone2.tmp | tr ',' ' ' > bench_drone2_rnlp2.tmp
grep '^drone2_rnlp4'    bench_drone2.tmp | tr ',' ' ' > bench_drone2_rnlp4.tmp
grep '^drone2_ucrnlp2'  bench_drone2.tmp | tr ',' ' ' > bench_drone2_ucrnlp2.tmp
grep '^drone2_sqdgl2'   bench_drone2.tmp | tr ',' ' ' > bench_drone2_sqdgl2.tmp
grep '^drone2_llab6'    bench_drone2.tmp | tr ',' ' ' > bench_drone2_llab6.tmp
grep '^drone2_mrlock'   bench_drone2.tmp | tr ',' ' ' > bench_drone2_mrlock.tmp
grep '^drone2_rwrnlp1'  bench_drone2.tmp | tr ',' ' ' > bench_drone2_rwrnlp1.tmp
grep '^drone2_rwsqdgl2' bench_drone2.tmp | tr ',' ' ' > bench_drone2_rwsqdgl2.tmp
grep '^drone2_rwllab2'  bench_drone2.tmp | tr ',' ' ' > bench_drone2_rwllab2.tmp
