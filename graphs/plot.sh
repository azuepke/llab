#!/bin/sh

gnuplot -p bench_lockoverheads.plt
epstopdf --outfile=bench_lockoverheads.pdf --gsopt=-dCompatibilityLevel=1.4 bench_lockoverheads.eps

gnuplot -p bench_cpu.plt
epstopdf --outfile=bench_cpu.pdf --gsopt=-dCompatibilityLevel=1.4 bench_cpu.eps

gnuplot -p bench_drone2.plt
epstopdf --outfile=bench_drone2.pdf --gsopt=-dCompatibilityLevel=1.4 bench_drone2.eps
