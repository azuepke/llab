
##### uncontended_parallel "lock overheads" on x86

#set terminal postscript eps size 5.0,3.0 enhanced color \

set terminal postscript eps size 4.4,2.1 enhanced color \
font ',16' linewidth 2
set output 'bench_lockoverheads.eps'

# no borders of the graph on the right and top
set border 3

# Labels for the axis
set grid
set xlabel "Number of CPU cores/hardware threads"
set xrange [1:20]
set xtics 1
set ylabel "Average execution time [{/Symbol m}s]" offset 1.0, 0.0
set yrange [0:6]
set key inside top left box width 1.0 height 0.5 font ",14"


RUNS=1024
TS_US=2400 # CPU cycles per usec (2.4 GHz)

plot \
     'bench_intel_tatas.tmp'    using 2:($5/RUNS/TS_US) title 'TATAS'       with points pt 5 lc 7, \
     'bench_intel_ticket.tmp'   using 2:($5/RUNS/TS_US) title 'Ticket'      with points pt 1 lc 1, \
     'bench_intel_mcs.tmp'      using 2:($5/RUNS/TS_US) title 'MCS'         with points pt 2 lc 2, \
     'bench_intel_pftr.tmp'     using 2:($5/RUNS/TS_US) title 'PF-T reader' with points pt 3 lc 3, \
     'bench_intel_pftw.tmp'     using 2:($5/RUNS/TS_US) title 'PF-T writer' with points pt 4 lc 3, \
     'bench_intel_pfcr.tmp'     using 2:($5/RUNS/TS_US) title 'PF-C reader' with points pt 3 lc 4, \
     'bench_intel_pfcw.tmp'     using 2:($5/RUNS/TS_US) title 'PF-C writer' with points pt 4 lc 4, \
