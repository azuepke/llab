# Lock-less array-based multi-resource locks

This repository contains various lock implementations
and related benchmarks from the paper on multi-resource locks:

Mohammed Foughali and Alexander Zuepke:
"Formal Verification of Real-Time Autonomous Robots: an Interdisciplinary Approach",
Frontiers in Robotics and AI, 2022 [DOI:10.3389/frobt.2022.791757](https://doi.org/10.3389/frobt.2022.791757)


## Lock implementation
* ticket spin locks
* MCS locks
* Brandenburg's phase-fair ticket-based RW-locks
* various implementations of RNLP
* Ward et al.'s R/W-RNLP
* Nemitz et al.'s U-C-RNLP
* Zhang et al.'s MRLock
* LLAB
* RWLLAB


## License

The lock implementations are released under the
Creative Commons Attribution 4.0 International (CC-BY-4.0) license.
The related benchmarks, and scripts to generate the related graphs
are released under the MIT license, see [LICENSE.TXT](LICENSE.TXT).


## Compiling

To compile the lock library, type:

	$ make DEBUG=no

This generates

	liblock.a

and the two locking benchmarks

	test1 test2


## Benchmarking

To run the benchmarks on Linux, type:

	$ ./test1 > graphs/dump_intel.txt

The output contains the data points for the graphs.

Generate the graphs as follows:

	$ cd graphs
	graphs$ ./split.sh
	graphs$ ./plot.sh

This generates:

	bench_lockoverheads.eps bench_cpu.eps bench_drone2.eps
	bench_lockoverheads.pdf bench_cpu.pdf bench_drone2.pdf

To cleanup and remove all temporary files, type:

	graphs$ ./cleanup

See [graphs/README.TXT](graphs/README.TXT) for more details.


## Datasets

The datasets for the diagrams in the paper come from the following source files:

### dump_arm.txt

Integrate the benchmark into Kuri (see kuri.patch),
run the "bench_mrlock"-benchmark,
and copy the outout of the benchmark run into the file "dump_arm.txt".

### dump_intel.txt

Compile benchmark on a x86-64 machine:
	$ make DEBUG=no clean test1

Run on the machine:
	$ ./test1 > dump_intel.txt

### dump_drone2.txt

Run the "bench_mrlock_usecase"-benchmark on Kuri,
and copy the outout of the benchmark run into the file "dump_drone2.txt".
