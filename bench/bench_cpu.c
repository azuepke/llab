/* SPDX-License-Identifier: MIT */
/*
 * bench_cpu.c
 *
 * CPU core benchmark
 *
 * azuepke, 2014-03-04: initial
 * azuepke, 2020-01-31: ARM, atomics and spinlock benchmarks
 */


#ifdef LINUX_NATIVE
#include <stdint.h>
typedef uintptr_t addr_t;

#include <stdio.h>
#include <atomic.h>
#include <compiler.h>
#include <marron_compat.h>
#include <arch_spin.h>
#include <assert.h>
#include "bench.h"
#else
#include <stdint.h>
#include <stdio.h>
#include <marron/api.h>
#include <atomic.h>
#include <compiler.h>
#include <arch_spin.h>
#include "assert.h"
#include "app.h"
#include "bench.h"
#endif

/* for memory performance */
#define MEMSIZE	(16*1024)

/* for cache strider (AM57xx's caches are 2M) */
#define STRIDESIZE (4*1024*1024)

#define MAP_BASE 0x10000

/* memory to test write performance */
//static uint8_t storage[MEMSIZE] __aligned(0x1000);
static uint8_t storage[STRIDESIZE] __aligned(0x1000);

////////////////////////////////////////////////////////////////////////////////

static void bench_nops(void)
{
	TIME_T before, after;
	unsigned int runs;

	printf("- 0x nop in a loop\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("" : : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- 1x nop in a loop\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("nop" : : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- 2x nop in a loop\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- 3x nop in a loop\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- 4x nop in a loop\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- 5x nop in a loop\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- 6x nop in a loop\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- 7x nop in a loop\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- 8x nop in a loop\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- 9x nop in a loop\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- 10x nop in a loop\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
		__asm__ volatile ("nop" : : : "memory");
	}
	after = GETTS();
	delta(before, after);
}

////////////////////////////////////////////////////////////////////////////////

static __noinline void dummy_function1(void)
{
	/* just an empty function */
	barrier();
}

static __noinline void dummy_function2(void)
{
	dummy_function1();
}

static __noinline void dummy_function3(void)
{
	dummy_function2();
}

static __noinline void dummy_function4(void)
{
	dummy_function3();
}

static __noinline void dummy_function5(void)
{
	dummy_function4();
}

static __noinline void dummy_function6(void)
{
	dummy_function5();
}

static __noinline void dummy_function7(void)
{
	dummy_function6();
}

static __noinline void dummy_function8(void)
{
	dummy_function7();
}

static __noinline void dummy_function9(void)
{
	dummy_function8();
}

static void bench_calls(void)
{
	TIME_T before, after;
	unsigned int runs;

	printf("- function call + return\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		dummy_function1();
	}
	after = GETTS();
	delta(before, after);

	printf("- function call + return, depth 2\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		dummy_function2();
	}
	after = GETTS();
	delta(before, after);

	printf("- function call + return, depth 3\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		dummy_function3();
	}
	after = GETTS();
	delta(before, after);

	printf("- function call + return, depth 4\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		dummy_function4();
	}
	after = GETTS();
	delta(before, after);

	printf("- function call + return, depth 5\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		dummy_function5();
	}
	after = GETTS();
	delta(before, after);

	printf("- function call + return, depth 6\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		dummy_function6();
	}
	after = GETTS();
	delta(before, after);

	printf("- function call + return, depth 7\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		dummy_function7();
	}
	after = GETTS();
	delta(before, after);

	printf("- function call + return, depth 8\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		dummy_function8();
	}
	after = GETTS();
	delta(before, after);

	printf("- function call + return, depth 9\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		dummy_function9();
	}
	after = GETTS();
	delta(before, after);
}

////////////////////////////////////////////////////////////////////////////////

/* read and write memory */
static __noinline void mem_read8(void *_p)
{
	uint8_t *p = _p;
	unsigned long tmp;
	unsigned int i;

#ifdef __powerpc__
	/* PowerPC load/store with update requires an adjustment before the loop */
	p--;
#endif

	for (i = 0; i < MEMSIZE / sizeof(*p); i++) {
#if defined __x86_64__ || defined __i386__
		__asm__ volatile ("movb (%1), %%al\n"
		                  "add $1, %1\n"
		                  : "=&a"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __arm__
		__asm__ volatile ("ldrb %0, [%1], #1\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __aarch64__
		__asm__ volatile ("ldrb %w0, [%1], #1\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __powerpc__
		__asm__ volatile ("lbzu %0, 1(%1)\n"
		                  : "=&r"(tmp), "=b"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __riscv
		__asm__ volatile ("lbu %0, 0(%1)\n"
		                  "addi %1, %1, 1\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#endif
	}
}

static __noinline void mem_read16(void *_p)
{
	uint16_t *p = _p;
	unsigned long tmp;
	unsigned int i;

#ifdef __powerpc__
	/* PowerPC load/store with update requires an adjustment before the loop */
	p--;
#endif

	for (i = 0; i < MEMSIZE / sizeof(*p); i++) {
#if defined __x86_64__ || defined __i386__
		__asm__ volatile ("movw (%1), %%ax\n"
		                  "add $2, %1\n"
		                  : "=&a"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __arm__
		__asm__ volatile ("ldrh %0, [%1], #2\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __aarch64__
		__asm__ volatile ("ldrh %w0, [%1], #2\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __powerpc__
		__asm__ volatile ("lhzu %0, 2(%1)\n"
		                  : "=&r"(tmp), "=b"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __riscv
		__asm__ volatile ("lhu %0, 0(%1)\n"
		                  "addi %1, %1, 2\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#endif
	}
}

static __noinline void mem_read32(void *_p)
{
	uint32_t *p = _p;
	unsigned long tmp;
	unsigned int i;

#ifdef __powerpc__
	/* PowerPC load/store with update requires an adjustment before the loop */
	p--;
#endif

	for (i = 0; i < MEMSIZE / sizeof(*p); i++) {
#if defined __x86_64__ || defined __i386__
		__asm__ volatile ("movl (%1), %%eax\n"
		                  "add $4, %1\n"
		                  : "=&a"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __arm__
		__asm__ volatile ("ldr %0, [%1], #4\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __aarch64__
		__asm__ volatile ("ldr %w0, [%1], #4\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __powerpc__
		__asm__ volatile ("lwzu %0, 4(%1)\n"
		                  : "=&r"(tmp), "=b"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __riscv
		__asm__ volatile ("lw %0, 0(%1)\n"
		                  "addi %1, %1, 4\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#endif
	}
}

static __noinline void mem_read64(void *_p)
{
	uint64_t *p = _p;
#if defined __x86_64__ || defined __arm__ || defined __aarch64__ || (defined __riscv && defined __LP64__)
	unsigned long long tmp;
#elif defined __i386__ || defined __powerpc__ || defined __riscv
	unsigned long tmp1, tmp2;
#endif
	unsigned int i;

	for (i = 0; i < MEMSIZE / sizeof(*p); i++) {
#if defined __x86_64__
		__asm__ volatile ("movq (%1), %%rax\n"
		                  "add $8, %1\n"
		                  : "=&a"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __i386__
		__asm__ volatile ("movl (%2), %0\n"
		                  "movl 4(%2), %1\n"
		                  "add $8, %2\n"
		                  : "=&r"(tmp1), "=&r"(tmp2), "=b"(p)
		                  : "2"(p)
		                  : "memory");
#elif defined __arm__
		__asm__ volatile ("ldrd %0, [%1], #8\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __aarch64__
		__asm__ volatile ("ldr %0, [%1], #8\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __powerpc__
		__asm__ volatile ("lwz %0, 0(%2)\n"
		                  "lwz %1, 4(%2)\n"
		                  "addi %2, %2, 8\n"
		                  : "=&r"(tmp1), "=&r"(tmp2), "=b"(p)
		                  : "2"(p)
		                  : "memory");
#elif defined __riscv && defined __LP64__
		__asm__ volatile ("ld %0, 0(%1)\n"
		                  "addi %1, %1, 8\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __riscv
		__asm__ volatile ("lw %0, 0(%2)\n"
		                  "lw %1, 4(%2)\n"
		                  "addi %2, %2, 8\n"
		                  : "=&r"(tmp1), "=&r"(tmp2), "=r"(p)
		                  : "2"(p)
		                  : "memory");
#endif
	}
}

#if defined __aarch64__
static __noinline void mem_read128(void *_p)
{
	__uint128_t *p = _p;
	unsigned long tmp1, tmp2;
	unsigned int i;

	for (i = 0; i < MEMSIZE / sizeof(*p); i++) {
		__asm__ volatile ("ldp %0, %1, [%2], #16\n"
		                  : "=&r"(tmp1), "=&r"(tmp2), "=r"(p)
		                  : "2"(p)
		                  : "memory");
	}
}
#endif

static __noinline void mem_write8(void *_p)
{
	uint8_t *p = _p;
	unsigned long tmp;
	unsigned int i;

#ifdef __powerpc__
	/* PowerPC load/store with update requires an adjustment before the loop */
	p--;
#endif

	for (i = 0; i < MEMSIZE / sizeof(*p); i++) {
#if defined __x86_64__ || defined __i386__
		__asm__ volatile ("movb %%al, (%1)\n"
		                  "add $1, %1\n"
		                  : "=&a"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __arm__
		__asm__ volatile ("strb %0, [%1], #1\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __aarch64__
		__asm__ volatile ("strb %w0, [%1], #1\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __powerpc__
		__asm__ volatile ("stbu %0, 1(%1)\n"
		                  : "=&r"(tmp), "=b"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __riscv
		__asm__ volatile ("sb %0, 0(%1)\n"
		                  "addi %1, %1, 1\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#endif
	}
}

static __noinline void mem_write16(void *_p)
{
	uint16_t *p = _p;
	unsigned long tmp;
	unsigned int i;

#ifdef __powerpc__
	/* PowerPC load/store with update requires an adjustment before the loop */
	p--;
#endif

	for (i = 0; i < MEMSIZE / sizeof(*p); i++) {
#if defined __x86_64__ || defined __i386__
		__asm__ volatile ("movw %%ax, (%1)\n"
		                  "add $2, %1\n"
		                  : "=&a"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __arm__
		__asm__ volatile ("strh %0, [%1], #2\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __aarch64__
		__asm__ volatile ("strh %w0, [%1], #2\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __powerpc__
		__asm__ volatile ("sthu %0, 2(%1)\n"
		                  : "=&r"(tmp), "=b"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __riscv
		__asm__ volatile ("sh %0, 0(%1)\n"
		                  "addi %1, %1, 2\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#endif
	}
}

static __noinline void mem_write32(void *_p)
{
	uint32_t *p = _p;
	unsigned long tmp;
	unsigned int i;

#ifdef __powerpc__
	/* PowerPC load/store with update requires an adjustment before the loop */
	p--;
#endif

	for (i = 0; i < MEMSIZE / sizeof(*p); i++) {
#if defined __x86_64__ || defined __i386__
		__asm__ volatile ("movl %%eax, (%1)\n"
		                  "add $4, %1\n"
		                  : "=&a"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __arm__
		__asm__ volatile ("str %0, [%1], #4\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __aarch64__
		__asm__ volatile ("str %w0, [%1], #4\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __powerpc__
		__asm__ volatile ("stwu %0, 4(%1)\n"
		                  : "=&r"(tmp), "=b"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __riscv
		__asm__ volatile ("sw %0, 0(%1)\n"
		                  "addi %1, %1, 4\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#endif
	}
}

static __noinline void mem_write64(void *_p)
{
	uint64_t *p = _p;
#if defined __x86_64__ || defined __arm__ || defined __aarch64__ || (defined __riscv && defined __LP64__)
	unsigned long long tmp;
#elif defined __i386__ || defined __powerpc__ || defined __riscv
	unsigned long tmp1, tmp2;
#endif
	unsigned int i;

	for (i = 0; i < MEMSIZE / sizeof(*p); i++) {
#if defined __x86_64__
		__asm__ volatile ("movq %%rax, (%1)\n"
		                  "add $8, %1\n"
		                  : "=&a"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __i386__
		__asm__ volatile ("movl %0, (%2)\n"
		                  "movl %1, 4(%2)\n"
		                  "add $8, %2\n"
		                  : "=&r"(tmp1), "=&r"(tmp2), "=b"(p)
		                  : "2"(p)
		                  : "memory");
#elif defined __arm__
		__asm__ volatile ("strd %0, [%1], #8\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __aarch64__
		__asm__ volatile ("str %0, [%1], #8\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __powerpc__
		__asm__ volatile ("stw %0, 0(%2)\n"
		                  "stw %1, 4(%2)\n"
		                  "addi %2, %2, 8\n"
		                  : "=&r"(tmp1), "=&r"(tmp2), "=b"(p)
		                  : "2"(p)
		                  : "memory");
#elif defined __riscv && defined __LP64__
		__asm__ volatile ("sd %0, 0(%1)\n"
		                  "addi %1, %1, 8\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __riscv
		__asm__ volatile ("sw %0, 0(%2)\n"
		                  "sw %1, 4(%2)\n"
		                  "addi %2, %2, 8\n"
		                  : "=&r"(tmp1), "=&r"(tmp2), "=r"(p)
		                  : "2"(p)
		                  : "memory");
#endif
	}
}

#if defined __aarch64__
static __noinline void mem_write128(void *_p)
{
	__uint128_t *p = _p;
	unsigned long tmp1, tmp2;
	unsigned int i;

	for (i = 0; i < MEMSIZE / sizeof(*p); i++) {
		__asm__ volatile ("stp %0, %1, [%2], #16\n"
		                  : "=&r"(tmp1), "=&r"(tmp2), "=r"(p)
		                  : "2"(p)
		                  : "memory");
	}
}
#endif

static __noinline void mem_rdwr8(void *_p)
{
	uint8_t *p = _p;
	unsigned long tmp;
	unsigned int i;

#ifdef __powerpc__
	/* PowerPC load/store with update requires an adjustment before the loop */
	p--;
#endif

	for (i = 0; i < MEMSIZE / sizeof(*p); i++) {
#if defined __x86_64__ || defined __i386__
		__asm__ volatile ("movb (%1), %%al\n"
		                  "movb %%al, (%1)\n"
		                  "add $1, %1\n"
		                  : "=&a"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __arm__
		__asm__ volatile ("ldrb %0, [%1]\n"
		                  "strb %0, [%1], #1\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __aarch64__
		__asm__ volatile ("ldrb %w0, [%1]\n"
		                  "strb %w0, [%1], #1\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __powerpc__
		__asm__ volatile ("lbz %0, 1(%1)\n"
		                  "stbu %0, 1(%1)\n"
		                  : "=&r"(tmp), "=b"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __riscv
		__asm__ volatile ("lb %0, 0(%1)\n"
		                  "sb %0, 0(%1)\n"
		                  "addi %1, %1, 1\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#endif
	}
}

static __noinline void mem_rdwr16(void *_p)
{
	uint16_t *p = _p;
	unsigned long tmp;
	unsigned int i;

#ifdef __powerpc__
	/* PowerPC load/store with update requires an adjustment before the loop */
	p--;
#endif

	for (i = 0; i < MEMSIZE / sizeof(*p); i++) {
#if defined __x86_64__ || defined __i386__
		__asm__ volatile ("movw (%1), %%ax\n"
		                  "movw %%ax, (%1)\n"
		                  "add $2, %1\n"
		                  : "=&a"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __arm__
		__asm__ volatile ("ldrh %0, [%1]\n"
		                  "strh %0, [%1], #2\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __aarch64__
		__asm__ volatile ("ldrh %w0, [%1]\n"
		                  "strh %w0, [%1], #2\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __powerpc__
		__asm__ volatile ("lhz %0, 2(%1)\n"
		                  "sthu %0, 2(%1)\n"
		                  : "=&r"(tmp), "=b"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __riscv
		__asm__ volatile ("lh %0, 0(%1)\n"
		                  "sh %0, 0(%1)\n"
		                  "addi %1, %1, 2\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#endif
	}
}

static __noinline void mem_rdwr32(void *_p)
{
	uint32_t *p = _p;
	unsigned long tmp;
	unsigned int i;

#ifdef __powerpc__
	/* PowerPC load/store with update requires an adjustment before the loop */
	p--;
#endif

	for (i = 0; i < MEMSIZE / sizeof(*p); i++) {
#if defined __x86_64__ || defined __i386__
		__asm__ volatile ("movl (%1), %%eax\n"
		                  "movl %%eax, (%1)\n"
		                  "add $4, %1\n"
		                  : "=&a"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __arm__
		__asm__ volatile ("ldr %0, [%1]\n"
		                  "str %0, [%1], #4\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __aarch64__
		__asm__ volatile ("ldr %w0, [%1]\n"
		                  "str %w0, [%1], #4\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __powerpc__
		__asm__ volatile ("lwz %0, 4(%1)\n"
		                  "stwu %0, 4(%1)\n"
		                  : "=&r"(tmp), "=b"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __riscv
		__asm__ volatile ("lw %0, 0(%1)\n"
		                  "sw %0, 0(%1)\n"
		                  "addi %1, %1, 4\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#endif
	}
}

static __noinline void mem_rdwr64(void *_p)
{
	uint64_t *p = _p;
#if defined __x86_64__ || defined __arm__ || defined __aarch64__ || (defined __riscv && defined __LP64__)
	unsigned long long tmp;
#elif defined __i386__ || defined __powerpc__ || defined __riscv
	unsigned long tmp1, tmp2;
#endif
	unsigned int i;

	for (i = 0; i < MEMSIZE / sizeof(*p); i++) {
#if defined __x86_64__
		__asm__ volatile ("movq (%1), %%rax\n"
		                  "movq %%rax, (%1)\n"
		                  "add $8, %1\n"
		                  : "=&a"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __i386__
		__asm__ volatile ("movl (%2), %0\n"
		                  "movl 4(%2), %1\n"
		                  "movl %0, (%2)\n"
		                  "movl %1, 4(%2)\n"
		                  "add $8, %2\n"
		                  : "=&r"(tmp1), "=&r"(tmp2), "=b"(p)
		                  : "2"(p)
		                  : "memory");
#elif defined __arm__
		__asm__ volatile ("ldrd %0, [%1]\n"
		                  "strd %0, [%1], #8\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __aarch64__
		__asm__ volatile ("ldr %0, [%1]\n"
		                  "str %0, [%1], #8\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __powerpc__
		__asm__ volatile ("lwz %0, 0(%2)\n"
		                  "lwz %1, 4(%2)\n"
		                  "stw %0, 0(%2)\n"
		                  "stw %1, 4(%2)\n"
		                  "addi %2, %2, 8\n"
		                  : "=&r"(tmp1), "=&r"(tmp2), "=b"(p)
		                  : "2"(p)
		                  : "memory");
#elif defined __riscv && defined __LP64__
		__asm__ volatile ("ld %0, 0(%1)\n"
		                  "sd %0, 0(%1)\n"
		                  "addi %1, %1, 8\n"
		                  : "=&r"(tmp), "=r"(p)
		                  : "1"(p)
		                  : "memory");
#elif defined __riscv
		__asm__ volatile ("lw %0, 0(%2)\n"
		                  "lw %1, 4(%2)\n"
		                  "sw %0, 0(%2)\n"
		                  "sw %1, 4(%2)\n"
		                  "addi %2, %2, 8\n"
		                  : "=&r"(tmp1), "=&r"(tmp2), "=r"(p)
		                  : "2"(p)
		                  : "memory");
#endif
	}
}

#if defined __aarch64__
static __noinline void mem_rdwr128(void *_p)
{
	__uint128_t *p = _p;
	unsigned long tmp1, tmp2;
	unsigned int i;

	for (i = 0; i < MEMSIZE / sizeof(*p); i++) {
		__asm__ volatile ("ldp %0, %1, [%2]\n"
		                  "stp %0, %1, [%2], #16\n"
		                  : "=&r"(tmp1), "=&r"(tmp2), "=r"(p)
		                  : "2"(p)
		                  : "memory");
	}
}
#endif

static void mem_bench(const char *type, void *map)
{
	TIME_T before, after;
	unsigned int runs;

	printf("- mem read  %2dK %s   8-bit\t\t\t\t", MEMSIZE / 1024, type);
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		mem_read8(map);
	}
	after = GETTS();
	delta(before, after);

	printf("- mem read  %2dK %s  16-bit\t\t\t\t", MEMSIZE / 1024, type);
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		mem_read16(map);
	}
	after = GETTS();
	delta(before, after);

	printf("- mem read  %2dK %s  32-bit\t\t\t\t", MEMSIZE / 1024, type);
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		mem_read32(map);
	}
	after = GETTS();
	delta(before, after);

	printf("- mem read  %2dK %s  64-bit\t\t\t\t", MEMSIZE / 1024, type);
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		mem_read64(map);
	}
	after = GETTS();
	delta(before, after);

	printf("- mem read  %2dK %s 128-bit\t\t\t\t", MEMSIZE / 1024, type);
	fflush(stdout);
#if defined __aarch64__
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		mem_read128(map);
	}
	after = GETTS();
	delta(before, after);
#else
	printf("SKIPPED\n");
#endif


	printf("- mem write %2dK %s   8-bit\t\t\t\t", MEMSIZE / 1024, type);
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		mem_write8(map);
	}
	after = GETTS();
	delta(before, after);

	printf("- mem write %2dK %s  16-bit\t\t\t\t", MEMSIZE / 1024, type);
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		mem_write16(map);
	}
	after = GETTS();
	delta(before, after);

	printf("- mem write %2dK %s  32-bit\t\t\t\t", MEMSIZE / 1024, type);
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		mem_write32(map);
	}
	after = GETTS();
	delta(before, after);

	printf("- mem write %2dK %s  64-bit\t\t\t\t", MEMSIZE / 1024, type);
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		mem_write64(map);
	}
	after = GETTS();
	delta(before, after);

	printf("- mem write %2dK %s 128-bit\t\t\t\t", MEMSIZE / 1024, type);
	fflush(stdout);
#if defined __aarch64__
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		mem_write128(map);
	}
	after = GETTS();
	delta(before, after);
#else
	printf("SKIPPED\n");
#endif


	printf("- mem rd/wr %2dK %s   8-bit\t\t\t\t", MEMSIZE / 1024, type);
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		mem_rdwr8(map);
	}
	after = GETTS();
	delta(before, after);

	printf("- mem rd/wr %2dK %s  16-bit\t\t\t\t", MEMSIZE / 1024, type);
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		mem_rdwr16(map);
	}
	after = GETTS();
	delta(before, after);

	printf("- mem rd/wr %2dK %s  32-bit\t\t\t\t", MEMSIZE / 1024, type);
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		mem_rdwr32(map);
	}
	after = GETTS();
	delta(before, after);

	printf("- mem rd/wr %2dK %s  64-bit\t\t\t\t", MEMSIZE / 1024, type);
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		mem_rdwr64(map);
	}
	after = GETTS();
	delta(before, after);

	printf("- mem rd/wr %2dK %s 128-bit\t\t\t\t", MEMSIZE / 1024, type);
	fflush(stdout);
#if defined __aarch64__
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		mem_rdwr128(map);
	}
	after = GETTS();
	delta(before, after);
#else
	printf("SKIPPED\n");
#endif
}

////////////////////////////////////////////////////////////////////////////////

static __noinline void cache_stride_read(void *map, unsigned int step)
{
	uint8_t *p = map;
	uint8_t *end = &p[STRIDESIZE];
	unsigned long tmp;

	while (p < end) {
#if defined __x86_64__ || defined __i386__
		__asm__ volatile ("movb (%1), %%al\n"
		                  : "=&a"(tmp)
		                  : "r"(p)
		                  : "memory");
#elif defined __arm__
		__asm__ volatile ("ldrb %0, [%1]\n"
		                  : "=&r"(tmp)
		                  : "r"(p)
		                  : "memory");
#elif defined __aarch64__
		__asm__ volatile ("ldrb %w0, [%1]\n"
		                  : "=&r"(tmp)
		                  : "r"(p)
		                  : "memory");
#elif defined __powerpc__
		__asm__ volatile ("lbzu %0, 0(%1)\n"
		                  : "=&r"(tmp)
		                  : "r"(p)
		                  : "memory");
#elif defined __riscv
		__asm__ volatile ("lbu %0, 0(%1)\n"
		                  : "=&r"(tmp)
		                  : "r"(p)
		                  : "memory");
#endif
		p += step;
	}
}

static __noinline void cache_stride_write(void *map, unsigned int step)
{
	uint8_t *p = map;
	uint8_t *end = &p[STRIDESIZE];

	while (p < end) {
#if defined __x86_64__ || defined __i386__
		__asm__ volatile ("movb %%al, (%0)\n"
		                  :
		                  : "r"(p)
		                  : "memory");
#elif defined __arm__
		__asm__ volatile ("strb %0, [%0]\n"
		                  :
		                  : "r"(p)
		                  : "memory");
#elif defined __aarch64__
		__asm__ volatile ("strb %w0, [%0]\n"
		                  :
		                  : "r"(p)
		                  : "memory");
#elif defined __powerpc__
		__asm__ volatile ("stbu %0, 0(%0)\n"
		                  :
		                  : "r"(p)
		                  : "memory");
#elif defined __riscv
		__asm__ volatile ("sb %0, 0(%0)\n"
		                  :
		                  : "r"(p)
		                  : "memory");
#endif
		p += step;
	}
}

static __noinline void cache_stride_write_readback(void *map, unsigned int step)
{
	uint8_t *p = map;
	uint8_t *end = &p[STRIDESIZE];

	/* on most architectures, the value is read back from the write buffer */

	while (p < end) {
#if defined __x86_64__
		__asm__ volatile ("movq %0, (%0)\n"
		                  "movq (%0), %0\n"
		                  :
		                  : "r"(p)
		                  : "memory");
#elif defined __i386__
		__asm__ volatile ("movl %0, (%0)\n"
		                  "movl (%0), %0\n"
		                  :
		                  : "r"(p)
		                  : "memory");
#elif defined __arm__
		__asm__ volatile ("str %0, [%0]\n"
		                  "ldr %0, [%0]\n"
		                  :
		                  : "r"(p)
		                  : "memory");
#elif defined __aarch64__
		__asm__ volatile ("str %w0, [%0]\n"
		                  "ldr %w0, [%0]\n"
		                  :
		                  : "r"(p)
		                  : "memory");
#elif defined __powerpc__
		__asm__ volatile ("stw %0, 0(%0)\n"
		                  "lwz %0, 0(%0)\n"
		                  :
		                  : "r"(p)
		                  : "memory");
#elif defined __riscv
		__asm__ volatile ("sw %0, 0(%0)\n"
		                  "lw %0, 0(%0)\n"
		                  :
		                  : "r"(p)
		                  : "memory");
#endif
		p += step;
	}
}

static void bench_cache_stride(void *map, unsigned int step)
{
	TIME_T before, after;
	unsigned int runs;
	unsigned int accesses = STRIDESIZE / step;

	printf("- cache stride read  %4dK, step %4d, accesses %4d\t",
	       STRIDESIZE / 1024, step, accesses);
	fflush(stdout);
	/* prime caches */
	cache_stride_read(map, step);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		cache_stride_read(map, step);
	}
	after = GETTS();
	delta(before, after);
	printf("- cold cache memory access, read-only cache line:\t\t\t" FMT7 "\n",
	       (after-before)/RUNS/accesses);


	printf("- cache stride write %4dK, step %4d, accesses %4d\t",
	       STRIDESIZE / 1024, step, accesses);
	fflush(stdout);
	/* prime caches */
	cache_stride_write(map, step);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		cache_stride_write(map, step);
	}
	after = GETTS();
	delta(before, after);
	printf("- cold cache memory access, dirty cache line:\t\t\t\t" FMT7 "\n",
	       (after-before)/RUNS/accesses);


	printf("- cache stride wr/rb %4dK, step %4d, accesses %4d\t",
	       STRIDESIZE / 1024, step, accesses);
	fflush(stdout);
	/* prime caches */
	cache_stride_write(map, step);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		cache_stride_write_readback(map, step);
	}
	after = GETTS();
	delta(before, after);
	printf("- cold cache memory access, dirty cache line(2):\t\t\t" FMT7 "\n",
	       (after-before)/RUNS/accesses);
}

////////////////////////////////////////////////////////////////////////////////

#if defined __x86_64__ || defined __i386__
static void bench_x86(void *_p __unused)
{
	TIME_T before, after;
	uint32_t *p = _p;
	unsigned int runs;
	unsigned int tmp, old, new;
	int have_fences;
	int have_rdtscp;
	uint32_t eax, ebx, ecx, edx;

	printf("- x86 PAUSE\t\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("pause" : : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- x86 PUSHF;POPF\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("pushf;popf" : : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- x86 PUSHA;POPA\t\t\t\t\t");
	fflush(stdout);
#ifdef __i386__
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("pusha;popa" : : : "memory");
	}
	after = GETTS();
	delta(before, after);
#else
	printf("32-bit only instructions\n");
#endif

	printf("- x86 PUSH+POP uregs\t\t\t\t\t");
	fflush(stdout);
#ifdef __i386__
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("push %%ebp\n"
		                  "push %%edi\n"
		                  "push %%esi\n"
		                  "push %%ebx\n"
		                  "push %%ecx\n"
		                  "push %%edx\n"
		                  "push %%eax\n"
		                  "pop %%eax\n"
		                  "pop %%edx\n"
		                  "pop %%ecx\n"
		                  "pop %%ebx\n"
		                  "pop %%esi\n"
		                  "pop %%edi\n"
		                  "pop %%ebp\n"
		                  : : : "memory");
	}
	after = GETTS();
	delta(before, after);
#else
	printf("32-bit only instructions\n");
#endif

	printf("- x86 CPUID\t\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("cpuid"
		                 : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
		                 : "0" (0)
		                 : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- x86 RDTSC\t\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("rdtsc" : "=a" (eax), "=d" (edx) : : "memory");
	}
	after = GETTS();
	delta(before, after);


	/* check for RDTSCP support */
	__asm__ volatile ("cpuid"
	                 : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
	                 : "0" (0x80000000)
	                 : "memory");
	if (eax >= 0x80000002) {
		__asm__ volatile ("cpuid"
		                 : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
		                 : "0" (0x80000001)
		                 : "memory");
		have_rdtscp = edx & (1u << 27);
	} else {
		have_rdtscp = 0;
	}

	printf("- x86 RDTSCP\t\t\t\t\t\t");
	fflush(stdout);
	if (have_rdtscp) {
		before = GETTS();
		for (runs = 0; runs < RUNS; runs++) {
			__asm__ volatile ("rdtscp" : "=a" (eax), "=d" (edx), "=c" (ecx) : : "memory");
		}
		after = GETTS();
		delta(before, after);
	} else {
		printf("SKIPPED: NO RDTSCP\n");
	}

	/* LFENCE and MFENCE need SSE2 support */
	__asm__ volatile ("cpuid"
	                 : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
	                 : "0" (1)
	                 : "memory");
	have_fences = edx & (1u << 26);

	printf("- x86 MFENCE\t\t\t\t\t\t");
	fflush(stdout);
	if (have_fences) {
		before = GETTS();
		for (runs = 0; runs < RUNS; runs++) {
			__asm__ volatile ("mfence" : : : "memory");
		}
		after = GETTS();
		delta(before, after);
	} else {
		printf("SKIPPED: NO SSE2\n");
	}

	printf("- x86 LFENCE\t\t\t\t\t\t");
	fflush(stdout);
	if (have_fences) {
		before = GETTS();
		for (runs = 0; runs < RUNS; runs++) {
			__asm__ volatile ("lfence" : : : "memory");
		}
		after = GETTS();
		delta(before, after);
	} else {
		printf("SKIPPED: NO SSE2\n");
	}

	/* SFENCE requires SSE support only, we expect this always */
	printf("- x86 SFENCE\t\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("sfence" : : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- x86 INC <mem> (32-bit)\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("incl %0" : "+m"(*p) : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- x86 ADD $1, <mem> (32-bit)\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("addl $1, %0" : "+m"(*p) : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- x86 XADD <reg>, <mem> (32-bit)\t\t\t");
	fflush(stdout);
	tmp = 42;
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("xaddl %1, %0" : "+m"(*p), "+r"(tmp) : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- x86 CMPXCHG (compare fail) (32-bit)\t\t\t");
	fflush(stdout);
	*p = 13;
	old = 17;
	new = 42;
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("cmpxchgl %3, %0"
						  : "+m" (*p), "=&a" (tmp)
						  : "1" (old), "r" (new)
						  : "memory", "cc");
	}
	after = GETTS();
	delta(before, after);

	printf("- x86 CMPXCHG (success) (32-bit)\t\t\t");
	fflush(stdout);
	*p = 42;
	old = 42;
	new = 42;
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("cmpxchgl %3, %0"
						  : "+m" (*p), "=&a" (tmp)
						  : "1" (old), "r" (new)
						  : "memory", "cc");
	}
	after = GETTS();
	delta(before, after);


	printf("- x86 LOCK INC <mem> (32-bit)\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("lock; incl %0" : "+m"(*p) : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- x86 LOCK ADD $1, <mem> (32-bit)\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("lock; addl $1, %0" : "+m"(*p) : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- x86 LOCK XADD <reg>, <mem> (32-bit)\t\t\t");
	fflush(stdout);
	tmp = 42;
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("lock; xaddl %1, %0" : "+m"(*p), "+r"(tmp) : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- x86 LOCK CMPXCHG (compare fail) (32-bit)\t\t");
	fflush(stdout);
	*p = 13;
	old = 17;
	new = 42;
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("lock; cmpxchgl %3, %0"
						  : "+m" (*p), "=&a" (tmp)
						  : "1" (old), "r" (new)
						  : "memory", "cc");
	}
	after = GETTS();
	delta(before, after);

	printf("- x86 LOCK CMPXCHG (success) (32-bit)\t\t\t");
	fflush(stdout);
	*p = 42;
	old = 42;
	new = 42;
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("lock; cmpxchgl %3, %0"
						  : "+m" (*p), "=&a" (tmp)
						  : "1" (old), "r" (new)
						  : "memory", "cc");
	}
	after = GETTS();
	delta(before, after);
}
#endif

////////////////////////////////////////////////////////////////////////////////

#if defined __aarch64__ || defined __arm__
static void bench_arm(void *_p __unused)
{
	TIME_T before, after;
	//uint32_t *p = _p;
	unsigned int runs;

	printf("- ARM yield\t\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("yield" : : : "memory");
	}
	after = GETTS();
	delta(before, after);

	printf("- ARM dmb ishst\t\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		_arm_dmb("ISHST");
	}
	after = GETTS();
	delta(before, after);

	printf("- ARM dmb ishld\t\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
#ifdef __arm__
		_arm_dmb("ISH");
#else
		_arm_dmb("ISHLD");
#endif
	}
	after = GETTS();
	delta(before, after);

	printf("- ARM dmb ish\t\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		_arm_dmb("ISH");
	}
	after = GETTS();
	delta(before, after);

	printf("- ARM dsb ish\t\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		_arm_dsb("ISH");
	}
	after = GETTS();
	delta(before, after);

	printf("- ARM dsb sy\t\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		_arm_dsb("SY");
	}
	after = GETTS();
	delta(before, after);

	printf("- ARM isb\t\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("isb" : : : "memory");
	}
	after = GETTS();
	delta(before, after);

#ifdef __arm__
	printf("- ARM push/pop {r0-r12}\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		__asm__ volatile ("push {r0-r12};pop {r0-r12}" : : : "memory");
	}
	after = GETTS();
	delta(before, after);
#endif
}
#endif

////////////////////////////////////////////////////////////////////////////////

static void bench_atomic(void)
{
	TIME_T before, after;
	unsigned int runs;
	uint32_t a;
	uint32_t r;

	printf("- atomic_full_barrier\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		atomic_full_barrier();
	}
	after = GETTS();
	delta(before, after);

	printf("- atomic_load_barrier\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		atomic_load_barrier();
	}
	after = GETTS();
	delta(before, after);

	printf("- atomic_store_barrier\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		atomic_store_barrier();
	}
	after = GETTS();
	delta(before, after);

	printf("- atomic_acquire_barrier\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		atomic_acquire_barrier();
	}
	after = GETTS();
	delta(before, after);

	printf("- atomic_release_barrier\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		atomic_release_barrier();
	}
	after = GETTS();
	delta(before, after);

	printf("- atomic_relax\t\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		atomic_relax();
	}
	after = GETTS();
	delta(before, after);

	printf("- atomic_inc/dec pair\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		atomic_add_relaxed(&a, 1);
		atomic_add_relaxed(&a, 1);
	}
	after = GETTS();
	delta(before, after);

	printf("- atomic_swap_relaxed\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	r = 0;
	for (runs = 0; runs < RUNS; runs++) {
		r = atomic_swap_relaxed(&a, r);
	}
	after = GETTS();
	delta(before, after);

	printf("- atomic_swap_relaxed\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	r = 0;
	for (runs = 0; runs < RUNS; runs++) {
		r = atomic_swap_relaxed(&a, r);
	}
	after = GETTS();
	delta(before, after);


	printf("- atomic_cas failed\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	a = 42;
	r = 0;
	for (runs = 0; runs < RUNS; runs++) {
		(void)atomic_cas(&a, r, r+1);
		r++;
	}
	after = GETTS();
	delta(before, after);

	printf("- atomic_cas success\t\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	a = 0;
	r = 0;
	for (runs = 0; runs < RUNS; runs++) {
		(void)atomic_cas(&a, r, r+1);
		r++;
	}
	after = GETTS();
	delta(before, after);

	printf("- atomic_cas_relaxed failed\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	a = 42;
	r = 0;
	for (runs = 0; runs < RUNS; runs++) {
		(void)atomic_cas_relaxed(&a, r, r+1);
		r++;
	}
	after = GETTS();
	delta(before, after);

	printf("- atomic_cas_relaxed success\t\t\t\t");
	fflush(stdout);
	before = GETTS();
	a = 0;
	r = 0;
	for (runs = 0; runs < RUNS; runs++) {
		(void)atomic_cas_relaxed(&a, r, r+1);
		r++;
	}
	after = GETTS();
	delta(before, after);
}

////////////////////////////////////////////////////////////////////////////////

static void bench_spinlock(void)
{
	TIME_T before, after;
	unsigned int runs;
	arch_spin_t s;

	printf("- raw arch_spin_lock/unlock pair\t\t\t");
	fflush(stdout);
	arch_spin_init(&s);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		arch_spin_lock(&s);
		arch_spin_unlock(&s);
	}
	after = GETTS();
	delta(before, after);

	printf("- raw arch_spin_lock/(fake unlock) pair\t\t\t");
	fflush(stdout);
	arch_spin_init(&s);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		arch_spin_lock(&s);
		s.s.served++;
	}
	after = GETTS();
	delta(before, after);

	printf("- raw (fake lock)/unlock pair\t\t\t\t");
	fflush(stdout);
	arch_spin_init(&s);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		s.s.ticket++;
		arch_spin_unlock(&s);
	}
	after = GETTS();
	delta(before, after);

	printf("- raw arch_spin_trylock/unlock pair\t\t\t");
	fflush(stdout);
	arch_spin_init(&s);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		(void)arch_spin_trylock(&s);
		arch_spin_unlock(&s);
	}
	after = GETTS();
	delta(before, after);

	printf("- raw arch_spin_trylock/(fake unlock) pair\t\t");
	fflush(stdout);
	arch_spin_init(&s);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		(void)arch_spin_trylock(&s);
		s.s.served++;
	}
	after = GETTS();
	delta(before, after);

	printf("- raw arch_spin_trylock (fail)\t\t\t\t");
	fflush(stdout);
	arch_spin_init(&s);
	arch_spin_lock(&s);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		(void)arch_spin_trylock(&s);
	}
	after = GETTS();
	delta(before, after);
	arch_spin_unlock(&s);
}

////////////////////////////////////////////////////////////////////////////////

void bench_cpu(void)
{
	uint64_t t0, t1, t2;
	TIME_T before, after;

	printf("\n");
	printf("NOTE: benchmark uses CPU time stamps, check board manual!\n");
	printf("RUNS: %u, MEMSIZE (memory chunk size): %uK\n", RUNS, MEMSIZE / 1024);

	printf("\n*** time stamp performance:");
	fflush(stdout);
	/* wait for first ticket increment */
	t0 = sys_time_get();
	do {
		t1 = sys_time_get();
	} while (t1 == t0);
	before = GETTS();
	/* wait for at least 100ms */
	do {
		t2 = sys_time_get();
	} while ((t2 - t1) < 100*1000*1000);
	after = GETTS();
	printf(" %lld nanoseconds takes about " FMT10 " cycles\n",
	       (unsigned long long)t2 - t1, after - before);

	printf("\n*** CPU performance:\n");
	bench_nops();
	bench_calls();

	printf("\n*** memory performance:\n");
	mem_bench("mapped", storage);
	bench_cache_stride(storage, 4096);
	printf("\n");

	printf("\n*** architecture-specific performance:\n");
#if defined __x86_64__ || defined __i386__
	bench_x86(&storage);
#elif __aarch64__ || defined __arm__
	bench_arm(&storage);
#else
	printf("SKIPPED / NOT IMPLEMENTED\n");
#endif

	printf("\n*** atomic operations performance:\n");
	bench_atomic();

	printf("\n*** raw spinlock performance:\n");
	bench_spinlock();
}
