/* SPDX-License-Identifier: MIT */
/*
 * bench.h
 *
 * Benchmark
 *
 * azuepke, 2014-03-04: initial
 */

#ifndef __BENCH_H__
#define __BENCH_H__

#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>

#define RUNS 1024

static int _print_short;
static const char *_series_name;
static unsigned int _series_num1;
static unsigned int _series_num2;

#if defined __i386__ || defined __x86_64__
#define TIME_T	unsigned long long
#define FMT0	"%llu"
#define FMT7	"%7llu"
#define FMT10	"%10llu"
/* use TSC lower register for benchmarking on x86 */
#define GETTS()	\
	({	\
		unsigned int __eax, __edx;	\
		__asm__ volatile ("rdtsc" : "=a" (__eax), "=d" (__edx) : : "memory");	\
		((uint64_t)__edx << 32) | __eax;	\
	})

#elif defined __arm__
#include <arm_perf.h>
#define TIME_T	uint32_t
#define FMT0	"%u"
#define FMT7	"%7u"
#define FMT10	"%10u"
/* performance counter differ between ARM v6 and v7 */
#ifdef ARM_V6
#define ARM_CP15_CCNT "c15, c12, 1"
#else
#define ARM_CP15_CCNT "c9, c13, 0"
#endif
#define GETTS()	\
	({	\
		unsigned long __val;	\
		__asm__ volatile ("isb; mrc p15, 0, %0, " ARM_CP15_CCNT : "=r" (__val) : : "memory");	\
		__val;	\
	})

#elif defined __aarch64__
#define TIME_T	uint64_t
#define FMT0	"%lu"
#define FMT7	"%7lu"
#define FMT10	"%10lu"
#define GETTS()	\
	({	\
		unsigned long __val;	\
		__asm__ volatile ("isb; mrs %0, PMCCNTR_EL0" : "=r" (__val) : : "memory");	\
		__val;	\
	})

#elif defined __powerpc__
#define TIME_T	uint32_t
#define FMT0	"%u"
#define FMT7	"%7u"
#define FMT10	"%10u"
/* use timebase low register for benchmarking on PowerPC */
#define GETTS()	\
	({	\
		unsigned long __val;	\
		__asm__ volatile ("mftbl %0" : "=r" (__val) : : "memory");	\
		__val;	\
	})

#elif defined __riscv
#define TIME_T	unsigned long long
#define FMT0	"%llu"
#define FMT7	"%7llu"
#define FMT10	"%10llu"
#ifdef __LP64__
#define GETTS()	\
	({	\
		unsigned long long __val;	\
		__asm__ volatile ("rdcycle %0" : "=r" (__val) : : "memory");	\
		__val;	\
	})
#else
#define GETTS()	\
	({	\
		unsigned long __h1, __lo, __h2;	\
		__asm__ volatile (	\
			"1:\n"	\
			"rdcycleh	%0\n"	\
			"rdcycle	%1\n"	\
			"rdcycleh	%2\n"	\
			"bne		%0, %2, 1b\n"	\
			: "=&r"(__h1), "=&r"(__lo), "=&r"(__h2));	\
		((unsigned long long)__h1 << 32) | __lo;	\
	})
#endif

#else

#error Adapt this file to your CPU architecture!
#endif

static inline void bench_series(const char *name, unsigned int num1, unsigned int num2)
{
	_series_name = name;
	_series_num1 = num1;
	_series_num2 = num2;
}

static inline void bench(const char *text_short, const char *format_long, ...)
{
	if (_print_short) {
		printf("%s;%u;%u;%s;", _series_name ? _series_name : "",
		                       _series_num1, _series_num2, text_short);
	} else {
		va_list args;

		va_start(args, format_long);
		vprintf(format_long, args);
		va_end(args);
	}
}

static inline void delta(TIME_T before, TIME_T after)
{
	TIME_T diff;

	diff = after - before;
	if (_print_short) {
		printf(FMT0 "\n", diff);
	} else {
		printf("T: " FMT10 " / " FMT7 "\n", diff, diff / RUNS);
	}
}

#endif
