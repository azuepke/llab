/* SPDX-License-Identifier: MIT */
/*
 * bench_sqdgl.c
 *
 * Benchmark for paper for multi-resource locks
 *
 * azuepke, 2020-07-24: initial
 * azuepke, 2020-10-01: move lock implementation to own lib
 */

#ifdef LINUX_NATIVE
#include <stdint.h>
typedef uintptr_t addr_t;

#include <stdio.h>
#include <stdlib.h>
#include <atomic.h>
#include <compiler.h>
#include <assert.h>
#include "locks.h"
#include "bench.h"
#include <marron_compat.h>
#else
#include <stdint.h>
#include <stdio.h>
#include <marron/api.h>
#include <atomic.h>
#include <compiler.h>
#include <assert.h>
#include <locks.h>
#include "app.h"
#include "bench.h"
#endif


#pragma GCC diagnostic ignored "-Wdeclaration-after-statement"
//#pragma GCC diagnostic ignored "-Wunused-function"
//#pragma GCC diagnostic ignored "-Wunused-variable"


//#define RUN_ONCE 1

#define PRINT_SHORT 1

/** number of benchmark runs */
#define RUNS 1024


#if __SIZEOF_LONG__ == 8
#define RES_INIT32(a, b) { ((unsigned long)(a) << 32) | (b) }
#else
#define RES_INIT32(a, b) { (a), (b) }
#endif


////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void spin_barrier_test(void)
{
    spin_barrier_t b;
    int ret;
    (void)ret;

    printf("#spin_barrier basic tests: ");
    fflush(stdout);

    spin_barrier_init(&b, 4);

    /* NOTE: this test blocks! skipped! */
    printf("skipped\n");
    return;

    ret = spin_barrier_wait(&b, 0);
    assert(ret == 0);
    ret = spin_barrier_wait(&b, 1);
    assert(ret == 0);
    ret = spin_barrier_wait(&b, 2);
    assert(ret == 0);
    ret = spin_barrier_wait(&b, 3);
    assert(ret == 1);

    ret = spin_barrier_wait(&b, 3);
    assert(ret == 0);
    ret = spin_barrier_wait(&b, 0);
    assert(ret == 0);
    ret = spin_barrier_wait(&b, 2);
    assert(ret == 0);
    ret = spin_barrier_wait(&b, 1);
    assert(ret == 1);
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void sqdgl1_basic_test(unsigned int num_cpus __unused)
{
    sqdgl1_lock_t l;

    printf("#sqdgl1 basic tests: ");
    fflush(stdout);

    sqdgl1_init(&l);

    /* dry runs: 0 resources and 0xfff...f resources */
    res_t rnone = RES_INIT32(0, 0);
    sqdgl1_lock(&l, 0, rnone);
    sqdgl1_unlock(&l, 0);

    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    sqdgl1_lock(&l, 0, rall);
    sqdgl1_unlock(&l, 0);

    /* nesting test */
    res_t r1 = RES_INIT32(0, 1);
    res_t r2 = RES_INIT32(0, 2);
    res_t r4 = RES_INIT32(0, 4);
    res_t r8 = RES_INIT32(0, 8);
    sqdgl1_lock(&l, 0, r1);
    sqdgl1_lock(&l, 1, r2);
    sqdgl1_lock(&l, 2, r4);
    sqdgl1_lock(&l, 3, r8);

    sqdgl1_unlock(&l, 0);
    sqdgl1_unlock(&l, 1);
    sqdgl1_unlock(&l, 2);
    sqdgl1_unlock(&l, 3);

    /* nesting test2 -- reverse release */
    sqdgl1_lock(&l, 0, r1);
    sqdgl1_lock(&l, 1, r2);
    sqdgl1_lock(&l, 2, r4);
    sqdgl1_lock(&l, 3, r8);

    sqdgl1_unlock(&l, 3);
    sqdgl1_unlock(&l, 2);
    sqdgl1_unlock(&l, 1);
    sqdgl1_unlock(&l, 0);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void sqdgl2_basic_test(unsigned int num_cpus __unused)
{
    sqdgl2_lock_t l;

    printf("#sqdgl2 basic tests: ");
    fflush(stdout);

    sqdgl2_init(&l);

    /* dry runs: 0 resources and 0xfff...f resources */
    res_t rnone = RES_INIT32(0, 0);
    sqdgl2_lock(&l, 0, rnone);
    sqdgl2_unlock(&l, 0);

    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    sqdgl2_lock(&l, 0, rall);
    sqdgl2_unlock(&l, 0);

    /* nesting test */
    res_t r1 = RES_INIT32(0, 1);
    res_t r2 = RES_INIT32(0, 2);
    res_t r4 = RES_INIT32(0, 4);
    res_t r8 = RES_INIT32(0, 8);
    sqdgl2_lock(&l, 0, r1);
    sqdgl2_lock(&l, 1, r2);
    sqdgl2_lock(&l, 2, r4);
    sqdgl2_lock(&l, 3, r8);

    sqdgl2_unlock(&l, 0);
    sqdgl2_unlock(&l, 1);
    sqdgl2_unlock(&l, 2);
    sqdgl2_unlock(&l, 3);

    /* nesting test2 -- reverse release */
    sqdgl2_lock(&l, 0, r1);
    sqdgl2_lock(&l, 1, r2);
    sqdgl2_lock(&l, 2, r4);
    sqdgl2_lock(&l, 3, r8);

    sqdgl2_unlock(&l, 3);
    sqdgl2_unlock(&l, 2);
    sqdgl2_unlock(&l, 1);
    sqdgl2_unlock(&l, 0);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void rwsqdgl1_basic_test(unsigned int num_cpus __unused)
{
    rwsqdgl1_lock_t l;

    printf("#rwsqdgl1 basic tests: ");
    fflush(stdout);

    rwsqdgl1_init(&l);

    /* dry runs: 0 resources and 0xfff...f resources */
    res_t rnone = RES_INIT32(0, 0);
    rwsqdgl1_lock(&l, 0, rnone, rnone);
    rwsqdgl1_unlock(&l, 0, rnone, rnone);

    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    rwsqdgl1_lock(&l, 0, rall, rnone);
    rwsqdgl1_unlock(&l, 0, rall, rnone);
    rwsqdgl1_lock(&l, 0, rnone, rall);
    rwsqdgl1_unlock(&l, 0, rnone, rall);

    /* nesting test */
    res_t r1 = RES_INIT32(0, 1);
    res_t r2 = RES_INIT32(0, 2);
    res_t r4 = RES_INIT32(0, 4);
    res_t r8 = RES_INIT32(0, 8);
    rwsqdgl1_lock(&l, 0, r1, rnone);
    rwsqdgl1_lock(&l, 1, r2, rnone);
    rwsqdgl1_lock(&l, 2, r4, rnone);
    rwsqdgl1_lock(&l, 3, r8, rnone);

    rwsqdgl1_unlock(&l, 0, r1, rnone);
    rwsqdgl1_unlock(&l, 1, r2, rnone);
    rwsqdgl1_unlock(&l, 2, r4, rnone);
    rwsqdgl1_unlock(&l, 3, r8, rnone);

    /* nesting test2 -- reverse release */
    rwsqdgl1_lock(&l, 0, r1, rnone);
    rwsqdgl1_lock(&l, 1, r2, rnone);
    rwsqdgl1_lock(&l, 2, r4, rnone);
    rwsqdgl1_lock(&l, 3, r8, rnone);

    rwsqdgl1_unlock(&l, 3, r8, rnone);
    rwsqdgl1_unlock(&l, 2, r4, rnone);
    rwsqdgl1_unlock(&l, 1, r2, rnone);
    rwsqdgl1_unlock(&l, 0, r1, rnone);

    /* nesting test3 -- mixed types*/
    rwsqdgl1_lock(&l, 0, r1, rnone);
    rwsqdgl1_lock(&l, 1, rnone, r2);
    rwsqdgl1_lock(&l, 2, r4, rnone);
    rwsqdgl1_lock(&l, 3, rnone, r8);

    rwsqdgl1_unlock(&l, 0, r1, rnone);
    rwsqdgl1_unlock(&l, 2, r4, rnone);
    rwsqdgl1_unlock(&l, 1, rnone, r2);
    rwsqdgl1_unlock(&l, 3, rnone, r8);

    /* shared readers + mixed bits */
    res_t r3 = RES_INIT32(0, 3);
    res_t r7 = RES_INIT32(0, 7);
    res_t rf = RES_INIT32(0, 0xf);
    rwsqdgl1_lock(&l, 0, r1, rnone);
    rwsqdgl1_lock(&l, 1, r3, rnone);
    rwsqdgl1_lock(&l, 2, r7, rnone);
    rwsqdgl1_lock(&l, 3, rf, rnone);

    rwsqdgl1_unlock(&l, 0, r1, rnone);
    rwsqdgl1_unlock(&l, 2, r7, rnone);
    rwsqdgl1_unlock(&l, 1, r3, rnone);
    rwsqdgl1_unlock(&l, 3, rf, rnone);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void rwsqdgl2_basic_test(unsigned int num_cpus __unused)
{
    rwsqdgl2_lock_t l;

    printf("#rwsqdgl2 basic tests: ");
    fflush(stdout);

    rwsqdgl2_init(&l);

    /* dry runs: 0 resources and 0xfff...f resources */
    res_t rnone = RES_INIT32(0, 0);
    rwsqdgl2_lock(&l, 0, rnone, rnone);
    rwsqdgl2_unlock(&l, 0, rnone, rnone);

    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    rwsqdgl2_lock(&l, 0, rall, rnone);
    rwsqdgl2_unlock(&l, 0, rall, rnone);
    rwsqdgl2_lock(&l, 0, rnone, rall);
    rwsqdgl2_unlock(&l, 0, rnone, rall);

    /* nesting test */
    res_t r1 = RES_INIT32(0, 1);
    res_t r2 = RES_INIT32(0, 2);
    res_t r4 = RES_INIT32(0, 4);
    res_t r8 = RES_INIT32(0, 8);
    rwsqdgl2_lock(&l, 0, r1, rnone);
    rwsqdgl2_lock(&l, 1, r2, rnone);
    rwsqdgl2_lock(&l, 2, r4, rnone);
    rwsqdgl2_lock(&l, 3, r8, rnone);

    rwsqdgl2_unlock(&l, 0, r1, rnone);
    rwsqdgl2_unlock(&l, 1, r2, rnone);
    rwsqdgl2_unlock(&l, 2, r4, rnone);
    rwsqdgl2_unlock(&l, 3, r8, rnone);

    /* nesting test2 -- reverse release */
    rwsqdgl2_lock(&l, 0, r1, rnone);
    rwsqdgl2_lock(&l, 1, r2, rnone);
    rwsqdgl2_lock(&l, 2, r4, rnone);
    rwsqdgl2_lock(&l, 3, r8, rnone);

    rwsqdgl2_unlock(&l, 3, r8, rnone);
    rwsqdgl2_unlock(&l, 2, r4, rnone);
    rwsqdgl2_unlock(&l, 1, r2, rnone);
    rwsqdgl2_unlock(&l, 0, r1, rnone);

    /* nesting test3 -- mixed types*/
    rwsqdgl2_lock(&l, 0, r1, rnone);
    rwsqdgl2_lock(&l, 1, rnone, r2);
    rwsqdgl2_lock(&l, 2, r4, rnone);
    rwsqdgl2_lock(&l, 3, rnone, r8);

    rwsqdgl2_unlock(&l, 0, r1, rnone);
    rwsqdgl2_unlock(&l, 2, r4, rnone);
    rwsqdgl2_unlock(&l, 1, rnone, r2);
    rwsqdgl2_unlock(&l, 3, rnone, r8);

    /* shared readers + mixed bits */
    res_t r3 = RES_INIT32(0, 3);
    res_t r7 = RES_INIT32(0, 7);
    res_t rf = RES_INIT32(0, 0xf);
    rwsqdgl2_lock(&l, 0, r1, rnone);
    rwsqdgl2_lock(&l, 1, r3, rnone);
    rwsqdgl2_lock(&l, 2, r7, rnone);
    rwsqdgl2_lock(&l, 3, rf, rnone);

    rwsqdgl2_unlock(&l, 0, r1, rnone);
    rwsqdgl2_unlock(&l, 2, r7, rnone);
    rwsqdgl2_unlock(&l, 1, r3, rnone);
    rwsqdgl2_unlock(&l, 3, rf, rnone);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void tatas_basic_test(unsigned int num_cpus __unused)
{
    tatas_lock_t l;

    printf("#tatas basic tests: ");
    fflush(stdout);

    tatas_init(&l);

    /* dry runs: 0 resources and 0xfff...f resources */
    tatas_lock(&l, 0);
    assert(l == 0);
    tatas_unlock(&l, 0);
    assert(l == 0);
    tatas_lock(&l, 1);
    assert(l == 1);
    tatas_unlock(&l, 1);
    assert(l == 0);
    tatas_lock(&l, 0xffffffffffffffffull);
    assert(l == 0xffffffffffffffffull);
    tatas_unlock(&l, 0xffffffffffffffffull);
    assert(l == 0);

    /* nesting test */
    tatas_lock(&l, 0xaaaaaaaaaaaaaaaaull);
    assert(l == 0xaaaaaaaaaaaaaaaaull);
    tatas_lock(&l, 0x5555555555555555ull);
    assert(l == 0xffffffffffffffffull);
    tatas_unlock(&l, 0xaaaaaaaaaaaaaaaaull);
    assert(l == 0x5555555555555555ull);
    tatas_unlock(&l, 0x5555555555555555ull);
    assert(l == 0);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void array1_basic_test(unsigned int num_cpus __unused)
{
    array1_lock_t l;

    printf("#array lock basic tests: ");
    fflush(stdout);

    array1_init(&l);

    /* dry runs: 0 resources and 0xfff...f resources */
    res_t r0 = RES_INIT32(0, 0);
    array1_lock(&l, r0);
    array1_unlock(&l, r0);

    res_t r1 = RES_INIT32(0, 1);
    array1_lock(&l, r1);
    array1_unlock(&l, r1);

    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    array1_lock(&l, rall);
    array1_unlock(&l, rall);

    /* nesting test */
    res_t raa = RES_INIT32(0xaaaaaaaa, 0xaaaaaaaa);
    res_t r55 = RES_INIT32(0x55555555, 0x55555555);
    array1_lock(&l, raa);
    array1_lock(&l, r55);
    array1_unlock(&l, raa);
    array1_unlock(&l, r55);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void array2_basic_test(unsigned int num_cpus __unused)
{
    array2_lock_t l;

    printf("#array lock basic tests: ");
    fflush(stdout);

    array2_init(&l);

    /* dry runs: 0 resources and 0xfff...f resources */
    res_t r0 = RES_INIT32(0, 0);
    array2_lock(&l, r0);
    array2_unlock(&l, r0);

    res_t r1 = RES_INIT32(0, 1);
    array2_lock(&l, r1);
    array2_unlock(&l, r1);

    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    array2_lock(&l, rall);
    array2_unlock(&l, rall);

    /* nesting test */
    res_t raa = RES_INIT32(0xaaaaaaaa, 0xaaaaaaaa);
    res_t r55 = RES_INIT32(0x55555555, 0x55555555);
    array2_lock(&l, raa);
    array2_lock(&l, r55);
    array2_unlock(&l, raa);
    array2_unlock(&l, r55);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void llab5_basic_test(unsigned int num_cpus __unused)
{
    llab5_lock_t l;

    printf("#llab5 lock basic tests: ");
    fflush(stdout);

    llab5_init(&l, num_cpus);

    /* dry runs: 0 resources and 0xfff...f resources */
    res_t r0 = RES_INIT32(0, 0);
    res_t r1 = RES_INIT32(0, 1);
    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    llab5_lock(&l, 0, r0);
    llab5_unlock(&l, 0);
    llab5_lock(&l, 0, r1);
    llab5_unlock(&l, 0);
    llab5_lock(&l, 0, rall);
    llab5_unlock(&l, 0);

    /* nesting test */
    /* NOTE: nesting is not really supported, as unlock frees everything! */
    res_t r55 = RES_INIT32(0x55555555, 0x55555555);
    res_t raa = RES_INIT32(0xaaaaaaaa, 0xaaaaaaaa);
    llab5_lock(&l, 0, raa);
    llab5_lock(&l, 0, r55);
    llab5_unlock(&l, 0);
    llab5_lock(&l, 0, r55);
    llab5_lock(&l, 0, raa);
    llab5_unlock(&l, 0);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void llab6_basic_test(unsigned int num_cpus __unused)
{
    llab6_lock_t l;

    printf("#llab6 lock basic tests: ");
    fflush(stdout);

    llab6_init(&l, num_cpus);

    /* dry runs: 0 resources and 0xfff...f resources */
    res_t r0 = RES_INIT32(0, 0);
    res_t r1 = RES_INIT32(0, 1);
    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    llab6_lock(&l, 0, r0);
    llab6_unlock(&l, 0);
    llab6_lock(&l, 0, r1);
    llab6_unlock(&l, 0);
    llab6_lock(&l, 0, rall);
    llab6_unlock(&l, 0);

    /* nesting test */
    /* NOTE: nesting is not really supported, as unlock frees everything! */
    res_t r55 = RES_INIT32(0x55555555, 0x55555555);
    res_t raa = RES_INIT32(0xaaaaaaaa, 0xaaaaaaaa);
    llab6_lock(&l, 0, raa);
    llab6_lock(&l, 0, r55);
    llab6_unlock(&l, 0);
    llab6_lock(&l, 0, r55);
    llab6_lock(&l, 0, raa);
    llab6_unlock(&l, 0);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void rwllab1_basic_test(unsigned int num_cpus __unused)
{
    rwllab1_lock_t l;

    printf("#rwllab1 lock basic tests: ");
    fflush(stdout);

    rwllab1_init(&l, 4);

    /* dry runs: 0 resources and 0xfff...f resources */
    res_t r0 = RES_INIT32(0, 0);
    res_t r1 = RES_INIT32(0, 1);
    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    rwllab1_lock(&l, 0, r0, r0);
    rwllab1_unlock(&l, 0);
    rwllab1_lock(&l, 0, r1, r0);
    rwllab1_unlock(&l, 0);
    rwllab1_lock(&l, 0, r0, r0);
    rwllab1_unlock(&l, 0);
    rwllab1_lock(&l, 0, rall, r0);
    rwllab1_unlock(&l, 0);
    rwllab1_lock(&l, 0, r0, rall);
    rwllab1_unlock(&l, 0);

    /* nesting test */
    /* NOTE: nesting is not really supported, as unlock frees everything! */
    res_t r55 = RES_INIT32(0x55555555, 0x55555555);
    res_t raa = RES_INIT32(0xaaaaaaaa, 0xaaaaaaaa);
    rwllab1_lock(&l, 0, raa, r0);
    rwllab1_lock(&l, 0, r55, r0);
    rwllab1_unlock(&l, 0);
    rwllab1_lock(&l, 0, r55, r0);
    rwllab1_lock(&l, 0, r0, raa);
    rwllab1_unlock(&l, 0);

    /* shared readers + mixed bits */
    res_t r3 = RES_INIT32(0, 3);
    res_t r7 = RES_INIT32(0, 7);
    res_t rf = RES_INIT32(0, 0xf);
    rwllab1_lock(&l, 0, r1, r0);
    rwllab1_lock(&l, 1, r3, r0);
    rwllab1_lock(&l, 2, r7, r0);
    rwllab1_lock(&l, 3, rf, r0);

    rwllab1_unlock(&l, 0);
    rwllab1_unlock(&l, 2);
    rwllab1_unlock(&l, 1);
    rwllab1_unlock(&l, 3);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void rwllab2_basic_test(unsigned int num_cpus __unused)
{
    rwllab2_lock_t l;

    printf("#rwllab2 lock basic tests: ");
    fflush(stdout);

    rwllab2_init(&l, 4);

    /* dry runs: 0 resources and 0xfff...f resources */
    res_t r0 = RES_INIT32(0, 0);
    res_t r1 = RES_INIT32(0, 1);
    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    rwllab2_lock(&l, 0, r0, r0);
    rwllab2_unlock(&l, 0);
    rwllab2_lock(&l, 0, r1, r0);
    rwllab2_unlock(&l, 0);
    rwllab2_lock(&l, 0, r0, r0);
    rwllab2_unlock(&l, 0);
    rwllab2_lock(&l, 0, rall, r0);
    rwllab2_unlock(&l, 0);
    rwllab2_lock(&l, 0, r0, rall);
    rwllab2_unlock(&l, 0);

    /* nesting test */
    /* NOTE: nesting is not really supported, as unlock frees everything! */
    res_t r55 = RES_INIT32(0x55555555, 0x55555555);
    res_t raa = RES_INIT32(0xaaaaaaaa, 0xaaaaaaaa);
    rwllab2_lock(&l, 0, raa, r0);
    rwllab2_lock(&l, 0, r55, r0);
    rwllab2_unlock(&l, 0);
    rwllab2_lock(&l, 0, r55, r0);
    rwllab2_lock(&l, 0, r0, raa);
    rwllab2_unlock(&l, 0);

    /* shared readers + mixed bits */
    res_t r3 = RES_INIT32(0, 3);
    res_t r7 = RES_INIT32(0, 7);
    res_t rf = RES_INIT32(0, 0xf);
    rwllab2_lock(&l, 0, r1, r0);
    rwllab2_lock(&l, 1, r3, r0);
    rwllab2_lock(&l, 2, r7, r0);
    rwllab2_lock(&l, 3, rf, r0);

    rwllab2_unlock(&l, 0);
    rwllab2_unlock(&l, 2);
    rwllab2_unlock(&l, 1);
    rwllab2_unlock(&l, 3);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void rnlp1_basic_test(unsigned int num_cpus __unused)
{
    rnlp1_global_t l;

    printf("#RNLP1 lock basic tests: ");
    fflush(stdout);

    rnlp1_init(&l);

    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    rnlp1_lock(&l, 0, rall);
    rnlp1_unlock(&l, 0, rall);

    /* hand-over-hand */
    res_t r55 = RES_INIT32(0x55555555, 0x55555555);
    res_t raa = RES_INIT32(0xaaaaaaaa, 0xaaaaaaaa);
    rnlp1_lock(&l, 0, r55);
    rnlp1_lock(&l, 0, raa);
    rnlp1_unlock(&l, 0, r55);
    rnlp1_unlock(&l, 0, raa);

    /* orderly nested */
    res_t r33 = RES_INIT32(0x33333333, 0x33333333);
    res_t rcc = RES_INIT32(0xcccccccc, 0xcccccccc);
    rnlp1_lock(&l, 0, r33);
    rnlp1_lock(&l, 2, rcc);
    rnlp1_unlock(&l, 2, rcc);
    rnlp1_unlock(&l, 0, r33);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void rnlp2_basic_test(unsigned int num_cpus __unused)
{
    rnlp2_global_t l;

    printf("#rnlp2 lock basic tests: ");
    fflush(stdout);

    rnlp2_init(&l);

    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    rnlp2_lock(&l, 0, rall);
    rnlp2_unlock(&l, 0, rall);

    /* hand-over-hand */
    res_t r55 = RES_INIT32(0x55555555, 0x55555555);
    res_t raa = RES_INIT32(0xaaaaaaaa, 0xaaaaaaaa);
    rnlp2_lock(&l, 0, r55);
    rnlp2_lock(&l, 0, raa);
    rnlp2_unlock(&l, 0, r55);
    rnlp2_unlock(&l, 0, raa);

    /* orderly nested */
    res_t r32 = RES_INIT32(0x32323232, 0x32323232);
    res_t rcc = RES_INIT32(0xcccccccc, 0xcccccccc);
    rnlp2_lock(&l, 0, r32);
    rnlp2_lock(&l, 3, rcc);
    rnlp2_unlock(&l, 3, rcc);
    rnlp2_unlock(&l, 0, r32);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void rnlp3_basic_test(unsigned int num_cpus __unused)
{
    rnlp3_global_t l;

    printf("#rnlp3 lock basic tests: ");
    fflush(stdout);

    rnlp3_init(&l);

    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    rnlp3_lock(&l, 0, rall);
    rnlp3_unlock(&l, 0, rall);

    /* hand-over-hand */
    res_t r55 = RES_INIT32(0x55555555, 0x55555555);
    res_t raa = RES_INIT32(0xaaaaaaaa, 0xaaaaaaaa);
    rnlp3_lock(&l, 0, r55);
    rnlp3_lock(&l, 1, raa);
    rnlp3_unlock(&l, 0, r55);
    rnlp3_unlock(&l, 1, raa);

    /* orderly nested */
    res_t r32 = RES_INIT32(0x32323232, 0x32323232);
    res_t rcc = RES_INIT32(0xcccccccc, 0xcccccccc);
    rnlp3_lock(&l, 0, r32);
    rnlp3_lock(&l, 3, rcc);
    rnlp3_unlock(&l, 3, rcc);
    rnlp3_unlock(&l, 0, r32);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void rnlp4_basic_test(unsigned int num_cpus __unused)
{
    rnlp4_global_t l;

    printf("#rnlp4 lock basic tests: ");
    fflush(stdout);

    rnlp4_init(&l);

    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    rnlp4_lock(&l, 0, rall);
    rnlp4_unlock(&l, 0, rall);

    /* hand-over-hand */
    res_t r55 = RES_INIT32(0x55555555, 0x55555555);
    res_t raa = RES_INIT32(0xaaaaaaaa, 0xaaaaaaaa);
    rnlp4_lock(&l, 0, r55);
    rnlp4_lock(&l, 1, raa);
    rnlp4_unlock(&l, 0, r55);
    rnlp4_unlock(&l, 1, raa);

    /* orderly nested */
    res_t r32 = RES_INIT32(0x32323232, 0x32323232);
    res_t rcc = RES_INIT32(0xcccccccc, 0xcccccccc);
    rnlp4_lock(&l, 0, r32);
    rnlp4_lock(&l, 3, rcc);
    rnlp4_unlock(&l, 3, rcc);
    rnlp4_unlock(&l, 0, r32);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void rwrnlp1_basic_test(unsigned int num_cpus __unused)
{
    rwrnlp1_global_t l;

    printf("#R/W-RNLP1 lock basic tests: ");
    fflush(stdout);

    rwrnlp1_init(&l);

    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    res_t r33 = RES_INIT32(0x33333333, 0x33333333);
    res_t rcc = RES_INIT32(0xcccccccc, 0xcccccccc);
    res_t r55 = RES_INIT32(0x55555555, 0x55555555);
    res_t raa = RES_INIT32(0xaaaaaaaa, 0xaaaaaaaa);

    rwrnlp1_write_lock(&l, 0, rall);
    rwrnlp1_write_unlock(&l, 0, rall);
    rwrnlp1_read_lock(&l, 0, raa);
    rwrnlp1_read_lock(&l, 0, r55);
    rwrnlp1_read_unlock(&l, 0, raa);
    rwrnlp1_read_unlock(&l, 0, r55);
    rwrnlp1_write_lock(&l, 0, r33);
    rwrnlp1_write_unlock(&l, 0, r33);
    rwrnlp1_read_lock(&l, 0, raa);
    rwrnlp1_read_lock(&l, 0, r55);
    rwrnlp1_read_unlock(&l, 0, r55);
    rwrnlp1_read_unlock(&l, 0, raa);
    rwrnlp1_write_lock(&l, 0, rcc);
    rwrnlp1_write_unlock(&l, 0, rcc);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void rwrnlp2_basic_test(unsigned int num_cpus __unused)
{
    rwrnlp2_global_t l;

    printf("#R/W-RNLP2 lock basic tests: ");
    fflush(stdout);

    rwrnlp2_init(&l);

    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    res_t r33 = RES_INIT32(0x33333333, 0x33333333);
    res_t rcc = RES_INIT32(0xcccccccc, 0xcccccccc);
    res_t r55 = RES_INIT32(0x55555555, 0x55555555);
    res_t raa = RES_INIT32(0xaaaaaaaa, 0xaaaaaaaa);

    rwrnlp2_write_lock(&l, 0, rall);
    rwrnlp2_write_unlock(&l, 0, rall);
    rwrnlp2_read_lock(&l, 0, raa);
    rwrnlp2_read_lock(&l, 0, r55);
    rwrnlp2_read_unlock(&l, 0, raa);
    rwrnlp2_read_unlock(&l, 0, r55);
    rwrnlp2_write_lock(&l, 0, r33);
    rwrnlp2_write_unlock(&l, 0, r33);
    rwrnlp2_read_lock(&l, 0, raa);
    rwrnlp2_read_lock(&l, 0, r55);
    rwrnlp2_read_unlock(&l, 0, r55);
    rwrnlp2_read_unlock(&l, 0, raa);
    rwrnlp2_write_lock(&l, 0, rcc);
    rwrnlp2_write_unlock(&l, 0, rcc);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void rwrnlp3_basic_test(unsigned int num_cpus __unused)
{
    rwrnlp3_global_t l;

    printf("#R/W-RNLP2 lock basic tests: ");
    fflush(stdout);

    rwrnlp3_init(&l);

    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    res_t r33 = RES_INIT32(0x33333333, 0x33333333);
    res_t rcc = RES_INIT32(0xcccccccc, 0xcccccccc);
    res_t r55 = RES_INIT32(0x55555555, 0x55555555);
    res_t raa = RES_INIT32(0xaaaaaaaa, 0xaaaaaaaa);

    rwrnlp3_write_lock(&l, 0, rall);
    rwrnlp3_write_unlock(&l, 0, rall);
    rwrnlp3_read_lock(&l, 0, raa);
    rwrnlp3_read_lock(&l, 0, r55);
    rwrnlp3_read_unlock(&l, 0, raa);
    rwrnlp3_read_unlock(&l, 0, r55);
    rwrnlp3_write_lock(&l, 0, r33);
    rwrnlp3_write_unlock(&l, 0, r33);
    rwrnlp3_read_lock(&l, 0, raa);
    rwrnlp3_read_lock(&l, 0, r55);
    rwrnlp3_read_unlock(&l, 0, r55);
    rwrnlp3_read_unlock(&l, 0, raa);
    rwrnlp3_write_lock(&l, 0, rcc);
    rwrnlp3_write_unlock(&l, 0, rcc);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void ucrnlp1_basic_test(unsigned int num_cpus __unused)
{
    ucrnlp1_global_t l;
    uint32_t next1, next2;

    printf("#U-C-RNLP1 lock (ticket) basic tests: ");
    fflush(stdout);

    ucrnlp1_init(&l);

    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    next1 = ucrnlp1_lock(&l, rall);
    ucrnlp1_unlock(&l, rall, next1);
    next1 = ucrnlp1_lock(&l, rall);
    ucrnlp1_unlock(&l, rall, next1);

    /* hand-over-hand */
    res_t r55 = RES_INIT32(0x55555555, 0x55555555);
    res_t raa = RES_INIT32(0xaaaaaaaa, 0xaaaaaaaa);
    next1 = ucrnlp1_lock(&l, r55);
    next2 = ucrnlp1_lock(&l, raa);
    ucrnlp1_unlock(&l, r55, next1);
    ucrnlp1_unlock(&l, raa, next2);

    /* orderly nested */
    res_t r32 = RES_INIT32(0x32323232, 0x32323232);
    res_t rcc = RES_INIT32(0xcccccccc, 0xcccccccc);
    next1 = ucrnlp1_lock(&l, r32);
    next2 = ucrnlp1_lock(&l, rcc);
    ucrnlp1_unlock(&l, rcc, next2);
    ucrnlp1_unlock(&l, r32, next1);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void ucrnlp2_basic_test(unsigned int num_cpus __unused)
{
    ucrnlp2_global_t l;
    uint32_t next1, next2;

    printf("#U-C-RNLP2 lock (MCS) basic tests: ");
    fflush(stdout);

    ucrnlp2_init(&l);

    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    next1 = ucrnlp2_lock(&l, rall);
    ucrnlp2_unlock(&l, rall, next1);
    next1 = ucrnlp2_lock(&l, rall);
    ucrnlp2_unlock(&l, rall, next1);

    /* hand-over-hand */
    res_t r55 = RES_INIT32(0x55555555, 0x55555555);
    res_t raa = RES_INIT32(0xaaaaaaaa, 0xaaaaaaaa);
    next1 = ucrnlp2_lock(&l, r55);
    next2 = ucrnlp2_lock(&l, raa);
    ucrnlp2_unlock(&l, r55, next1);
    ucrnlp2_unlock(&l, raa, next2);

    /* orderly nested */
    res_t r32 = RES_INIT32(0x32323232, 0x32323232);
    res_t rcc = RES_INIT32(0xcccccccc, 0xcccccccc);
    next1 = ucrnlp2_lock(&l, r32);
    next2 = ucrnlp2_lock(&l, rcc);
    ucrnlp2_unlock(&l, rcc, next2);
    ucrnlp2_unlock(&l, r32, next1);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

/** basic non-blocking test */
static void mrlock_basic_test(unsigned int num_cpus __unused)
{
    mrlock_global_t l;
    uint32_t h, h1, h2;

    printf("#MRLOCK lock basic tests: ");
    fflush(stdout);

    mrlock_init(&l);

    res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
    res_t r55 = RES_INIT32(0x55555555, 0x55555555);
    res_t raa = RES_INIT32(0xaaaaaaaa, 0xaaaaaaaa);

    h = mrlock_lock(&l, rall);
    mrlock_unlock(&l, h);

    h1 = mrlock_lock(&l, raa);
    h2 = mrlock_lock(&l, r55);
    mrlock_unlock(&l, h1);
    mrlock_unlock(&l, h2);

    h1 = mrlock_lock(&l, raa);
    h2 = mrlock_lock(&l, r55);
    mrlock_unlock(&l, h2);
    mrlock_unlock(&l, h1);

    printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

/// bit test iterators
static __noinline void uint64_bit_iterator(uint64_t r)
{
    for (unsigned int i = 0; i < sizeof(r)*8; i++) {
        if ((r & (1ull << i)) != 0) {
            __asm__ volatile ("nop" : : : "memory");
        }
    }
}

static __noinline void array_bit_iterator(const unsigned long *a, unsigned long n)
{
    for (unsigned long i = 0; i < n; i++) {
        unsigned long tmp = a[i];
        while (tmp != 0) {
            /* compiler builtin: finds last bit set; value most not be 0 */
            unsigned int bit = __builtin_ctzl(tmp);
            tmp &= ~1ul << bit;
            __asm__ volatile ("nop" : : : "memory");
        }
    }
}

////////////////////////////////////////////////////////////////////////////////

static void bench_uncontended_isolation(unsigned int num_cpus __unused)
{
    TIME_T before, after;
    unsigned int runs;

    printf("\n#Benching uncontended operations in isolation:\n\n");

    bench_series("uncontended_isolation", 0, 0);

    bench("it1a", "- 64-bit bit iterator (all zero)\t\t\t");
    fflush(stdout);
    {
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            uint64_bit_iterator(0);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("it1b", "- 64-bit bit iterator (all one)\t\t\t\t");
    fflush(stdout);
    {
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            uint64_bit_iterator(0xffffffffffffffffull);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("it2a", "- array bit iterator (all zero)\t\t\t\t");
    fflush(stdout);
    {
#if __SIZEOF_LONG__ == 8
        unsigned long a[1] = { 0ull };
#else
        unsigned long a[2] = { 0, 0 };
#endif
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            array_bit_iterator(a, 8 / __SIZEOF_LONG__);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("it2b", "- array bit iterator (all one)\t\t\t\t");
    fflush(stdout);
    {
#if __SIZEOF_LONG__ == 8
        unsigned long a[1] = { 0xfffffffffffffffful };
#else
        unsigned long a[2] = { 0xffffffff, 0xffffffff };
#endif
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            array_bit_iterator(a, 8 / __SIZEOF_LONG__);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("ticket", "- ticket lock->unlock\t\t\t\t\t");
    fflush(stdout);
    {
        ticket_spin_t l;

        ticket_spin_init(&l);

        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            ticket_spin_lock(&l);
            ticket_spin_unlock(&l);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("mcs", "- MCS lock->unlock\t\t\t\t\t");
    fflush(stdout);
    {
        mcs_node_t lock_node;
        mcs_lock_t l;

        mcs_init(&l);

        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            mcs_lock(&l, &lock_node);
            mcs_unlock(&l, &lock_node);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("pftr", "- PFT read lock->unlock\t\t\t\t\t");
    fflush(stdout);
    {
        pft_lock_t l;

        pft_init(&l);

        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            pft_read_lock(&l);
            pft_read_unlock(&l);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("pftw", "- PFT write lock->unlock\t\t\t\t");
    fflush(stdout);
    {
        pft_lock_t l;

        pft_init(&l);

        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            pft_write_lock(&l);
            pft_write_unlock(&l);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("pfcr", "- PFC read lock->unlock\t\t\t\t\t");
    fflush(stdout);
    {
        pfc_lock_t l;

        pfc_init(&l);

        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            pfc_read_lock(&l);
            pfc_read_unlock(&l);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("pfcw", "- PFC write lock->unlock\t\t\t\t");
    fflush(stdout);
    {
        pfc_lock_t l;

        pfc_init(&l);

        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            pfc_write_lock(&l);
            pfc_write_unlock(&l);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("tatas", "- TATAS lock->unlock\t\t\t\t\t");
    fflush(stdout);
    {
        tatas_lock_t l;

        tatas_init(&l);

        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            tatas_lock(&l, 0xffffffffffffffffull);
            tatas_unlock(&l, 0xffffffffffffffffull);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("array1a", "- array1 lock->unlock (no resources)\t\t\t");
    fflush(stdout);
    {
        array1_lock_t l;

        array1_init(&l);

        res_t r = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            array1_lock(&l, r);
            array1_unlock(&l, r);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("array1b", "- array1 lock->unlock (all resources)\t\t\t");
    fflush(stdout);
    {
        array1_lock_t l;

        array1_init(&l);

        res_t r = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            array1_lock(&l, r);
            array1_unlock(&l, r);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("array2a", "- array2 lock->unlock (no resources)\t\t\t");
    fflush(stdout);
    {
        array2_lock_t l;

        array2_init(&l);

        res_t r = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            array2_lock(&l, r);
            array2_unlock(&l, r);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("array2b", "- array2 lock->unlock (all resources)\t\t\t");
    fflush(stdout);
    {
        array2_lock_t l;

        array2_init(&l);

        res_t r = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            array2_lock(&l, r);
            array2_unlock(&l, r);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rnlp1a", "- RNLP1 lock->unlock (no resources)\t\t\t");
    fflush(stdout);
    {
        rnlp1_global_t l;

        rnlp1_init(&l);

        res_t rzero = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rnlp1_lock(&l, 0, rzero);
            rnlp1_unlock(&l, 0, rzero);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rnlp1b", "- RNLP1 lock->unlock (all resources)\t\t\t");
    fflush(stdout);
    {
        rnlp1_global_t l;

        rnlp1_init(&l);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rnlp1_lock(&l, 0, rall);
            rnlp1_unlock(&l, 0, rall);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rnlp2a", "- RNLP2 lock->unlock (no resources)\t\t\t");
    fflush(stdout);
    {
        rnlp2_global_t l;

        rnlp2_init(&l);

        res_t rzero = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rnlp2_lock(&l, 0, rzero);
            rnlp2_unlock(&l, 0, rzero);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rnlp2b", "- RNLP2 lock->unlock (all resources)\t\t\t");
    fflush(stdout);
    {
        rnlp2_global_t l;

        rnlp2_init(&l);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rnlp2_lock(&l, 0, rall);
            rnlp2_unlock(&l, 0, rall);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rnlp3a", "- rnlp3 lock->unlock (no resources)\t\t\t");
    fflush(stdout);
    {
        rnlp3_global_t l;

        rnlp3_init(&l);

        res_t rzero = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rnlp3_lock(&l, 0, rzero);
            rnlp3_unlock(&l, 0, rzero);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rnlp3b", "- rnlp3 lock->unlock (all resources)\t\t\t");
    fflush(stdout);
    {
        rnlp3_global_t l;

        rnlp3_init(&l);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rnlp3_lock(&l, 0, rall);
            rnlp3_unlock(&l, 0, rall);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rnlp4a", "- rnlp4 lock->unlock (no resources)\t\t\t");
    fflush(stdout);
    {
        rnlp4_global_t l;

        rnlp4_init(&l);

        res_t rzero = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rnlp4_lock(&l, 0, rzero);
            rnlp4_unlock(&l, 0, rzero);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rnlp4b", "- rnlp4 lock->unlock (all resources)\t\t\t");
    fflush(stdout);
    {
        rnlp4_global_t l;

        rnlp4_init(&l);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rnlp4_lock(&l, 0, rall);
            rnlp4_unlock(&l, 0, rall);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwrnlp1ra", "- RW-RNLP1 read:lock->unlock (no resources)\t\t");
    fflush(stdout);
    {
        rwrnlp1_global_t l;

        rwrnlp1_init(&l);

        res_t rnone = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwrnlp1_read_lock(&l, 0, rnone);
            rwrnlp1_read_unlock(&l, 0, rnone);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwrnlp1rb", "- RW-RNLP1 read:lock->unlock (all resources)\t\t");
    fflush(stdout);
    {
        rwrnlp1_global_t l;

        rwrnlp1_init(&l);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwrnlp1_read_lock(&l, 0, rall);
            rwrnlp1_read_unlock(&l, 0, rall);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwrnlp1wa", "- RW-RNLP1 write:lock->unlock (no resources)\t\t");
    fflush(stdout);
    {
        rwrnlp1_global_t l;

        rwrnlp1_init(&l);

        res_t rnone = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwrnlp1_write_lock(&l, 0, rnone);
            rwrnlp1_write_unlock(&l, 0, rnone);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwrnlp1wb", "- RW-RNLP1 write:lock->unlock (all resources)\t\t");
    fflush(stdout);
    {
        rwrnlp1_global_t l;

        rwrnlp1_init(&l);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwrnlp1_write_lock(&l, 0, rall);
            rwrnlp1_write_unlock(&l, 0, rall);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwrnlp2ra", "- RW-RNLP2 read:lock->unlock (no resources)\t\t");
    fflush(stdout);
    {
        rwrnlp2_global_t l;

        rwrnlp2_init(&l);

        res_t rnone = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwrnlp2_read_lock(&l, 0, rnone);
            rwrnlp2_read_unlock(&l, 0, rnone);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwrnlp2rb", "- RW-RNLP2 read:lock->unlock (all resources)\t\t");
    fflush(stdout);
    {
        rwrnlp2_global_t l;

        rwrnlp2_init(&l);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwrnlp2_read_lock(&l, 0, rall);
            rwrnlp2_read_unlock(&l, 0, rall);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwrnlp2wa", "- RW-RNLP2 write:lock->unlock (no resources)\t\t");
    fflush(stdout);
    {
        rwrnlp2_global_t l;

        rwrnlp2_init(&l);

        res_t rnone = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwrnlp2_write_lock(&l, 0, rnone);
            rwrnlp2_write_unlock(&l, 0, rnone);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwrnlp2wb", "- RW-RNLP2 write:lock->unlock (all resources)\t\t");
    fflush(stdout);
    {
        rwrnlp2_global_t l;

        rwrnlp2_init(&l);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwrnlp2_write_lock(&l, 0, rall);
            rwrnlp2_write_unlock(&l, 0, rall);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwrnlp3ra", "- RW-RNLP3 read:lock->unlock (no resources)\t\t");
    fflush(stdout);
    {
        rwrnlp3_global_t l;

        rwrnlp3_init(&l);

        res_t rnone = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwrnlp3_read_lock(&l, 0, rnone);
            rwrnlp3_read_unlock(&l, 0, rnone);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwrnlp3rb", "- RW-RNLP3 read:lock->unlock (all resources)\t\t");
    fflush(stdout);
    {
        rwrnlp3_global_t l;

        rwrnlp3_init(&l);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwrnlp3_read_lock(&l, 0, rall);
            rwrnlp3_read_unlock(&l, 0, rall);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwrnlp3wa", "- RW-RNLP3 write:lock->unlock (no resources)\t\t");
    fflush(stdout);
    {
        rwrnlp3_global_t l;

        rwrnlp3_init(&l);

        res_t rnone = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwrnlp3_write_lock(&l, 0, rnone);
            rwrnlp3_write_unlock(&l, 0, rnone);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwrnlp3wb", "- RW-RNLP3 write:lock->unlock (all resources)\t\t");
    fflush(stdout);
    {
        rwrnlp3_global_t l;

        rwrnlp3_init(&l);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwrnlp3_write_lock(&l, 0, rall);
            rwrnlp3_write_unlock(&l, 0, rall);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("sqdgl1a", "- SQDGL1 lock->unlock (no resources)\t\t\t");
    fflush(stdout);
    {
        sqdgl1_lock_t l;

        sqdgl1_init(&l);

        res_t rnone = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            sqdgl1_lock(&l, 0, rnone);
            sqdgl1_unlock(&l, 0);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("sqdgl1b", "- SQDGL1 lock->unlock (all resources)\t\t\t");
    fflush(stdout);
    {
        sqdgl1_lock_t l;

        sqdgl1_init(&l);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            sqdgl1_lock(&l, 0, rall);
            sqdgl1_unlock(&l, 0);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("sqdgl2a", "- sqdgl2 lock->unlock (no resources)\t\t\t");
    fflush(stdout);
    {
        sqdgl2_lock_t l;

        sqdgl2_init(&l);

        res_t rnone = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            sqdgl2_lock(&l, 0, rnone);
            sqdgl2_unlock(&l, 0);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("sqdgl2b", "- sqdgl2 lock->unlock (all resources)\t\t\t");
    fflush(stdout);
    {
        sqdgl2_lock_t l;

        sqdgl2_init(&l);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            sqdgl2_lock(&l, 0, rall);
            sqdgl2_unlock(&l, 0);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwsqdgl1a", "- rwsqdgl lock->unlock (no resources)\t\t\t");
    fflush(stdout);
    {
        rwsqdgl1_lock_t l;

        rwsqdgl1_init(&l);

        res_t rnone = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwsqdgl1_lock(&l, 0, rnone, rnone);
            rwsqdgl1_unlock(&l, 0, rnone, rnone);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwsqdgl1b", "- rwsqdgl lock->unlock (all resources)\t\t\t");
    fflush(stdout);
    {
        rwsqdgl1_lock_t l;

        rwsqdgl1_init(&l);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwsqdgl1_lock(&l, 0, rall, rall);
            rwsqdgl1_unlock(&l, 0, rall, rall);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwsqdgl2a", "- rwsqdgl lock->unlock (no resources)\t\t\t");
    fflush(stdout);
    {
        rwsqdgl2_lock_t l;

        rwsqdgl2_init(&l);

        res_t rnone = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwsqdgl2_lock(&l, 0, rnone, rnone);
            rwsqdgl2_unlock(&l, 0, rnone, rnone);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwsqdgl2b", "- rwsqdgl lock->unlock (all resources)\t\t\t");
    fflush(stdout);
    {
        rwsqdgl2_lock_t l;

        rwsqdgl2_init(&l);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwsqdgl2_lock(&l, 0, rall, rall);
            rwsqdgl2_unlock(&l, 0, rall, rall);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("llab5a", "- LLAB5 lock->unlock (no resources)\t\t\t");
    fflush(stdout);
    {
        llab5_lock_t l;

        llab5_init(&l, num_cpus);

        res_t rnone = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            llab5_lock(&l, 0, rnone);
            llab5_unlock(&l, 0);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("llab5b", "- LLAB5 lock->unlock (all resources)\t\t\t");
    fflush(stdout);
    {
        llab5_lock_t l;

        llab5_init(&l, num_cpus);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            llab5_lock(&l, 0, rall);
            llab5_unlock(&l, 0);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("llab6a", "- LLAB6 lock->unlock (no resources)\t\t\t");
    fflush(stdout);
    {
        llab6_lock_t l;

        llab6_init(&l, num_cpus);

        res_t rnone = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            llab6_lock(&l, 0, rnone);
            llab6_unlock(&l, 0);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("llab6b", "- LLAB6 lock->unlock (all resources)\t\t\t");
    fflush(stdout);
    {
        llab6_lock_t l;

        llab6_init(&l, num_cpus);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            llab6_lock(&l, 0, rall);
            llab6_unlock(&l, 0);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwllab1a", "- RWLLAB1 lock->unlock (no resources)\t\t\t");
    fflush(stdout);
    {
        rwllab1_lock_t l;

        rwllab1_init(&l, num_cpus);

        res_t rnone = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwllab1_lock(&l, 0, rnone, rnone);
            rwllab1_unlock(&l, 0);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwllab1b", "- RWLLAB1 lock->unlock (all resources)\t\t\t");
    fflush(stdout);
    {
        rwllab1_lock_t l;

        rwllab1_init(&l, num_cpus);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwllab1_lock(&l, 0, rall, rall);
            rwllab1_unlock(&l, 0);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwllab2a", "- RWLLAB2 lock->unlock (no resources)\t\t\t");
    fflush(stdout);
    {
        rwllab2_lock_t l;

        rwllab2_init(&l, num_cpus);

        res_t rnone = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwllab2_lock(&l, 0, rnone, rnone);
            rwllab2_unlock(&l, 0);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("rwllab2b", "- RWLLAB2 lock->unlock (all resources)\t\t\t");
    fflush(stdout);
    {
        rwllab2_lock_t l;

        rwllab2_init(&l, num_cpus);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            rwllab2_lock(&l, 0, rall, rall);
            rwllab2_unlock(&l, 0);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("ucrnlp1a", "- U-C-RNLP1 lock->unlock (no resources)\t\t\t");
    fflush(stdout);
    {
        ucrnlp1_global_t l;
        uint32_t next;

        ucrnlp1_init(&l);

        res_t rnone = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            next = ucrnlp1_lock(&l, rnone);
            ucrnlp1_unlock(&l, rnone, next);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("ucrnlp1b", "- U-C-RNLP1 lock->unlock (all resources)\t\t");
    fflush(stdout);
    {
        ucrnlp1_global_t l;
        uint32_t next;

        ucrnlp1_init(&l);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            next = ucrnlp1_lock(&l, rall);
            ucrnlp1_unlock(&l, rall, next);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("ucrnlp2a", "- U-C-RNLP2 lock->unlock (no resources)\t\t\t");
    fflush(stdout);
    {
        ucrnlp2_global_t l;
        uint32_t next;

        ucrnlp2_init(&l);

        res_t rnone = RES_INIT32(0, 0);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            next = ucrnlp2_lock(&l, rnone);
            ucrnlp2_unlock(&l, rnone, next);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("ucrnlp2b", "- U-C-RNLP2 lock->unlock (all resources)\t\t");
    fflush(stdout);
    {
        ucrnlp2_global_t l;
        uint32_t next;

        ucrnlp2_init(&l);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            next = ucrnlp2_lock(&l, rall);
            ucrnlp2_unlock(&l, rall, next);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("mrlocka", "- MRLOCK lock->unlock (one resources)\t\t\t");
    fflush(stdout);
    {
        mrlock_global_t l;
        uint32_t h;

        mrlock_init(&l);

        res_t rnone = RES_INIT32(0, 1);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            h = mrlock_lock(&l, rnone);
            mrlock_unlock(&l, h);
        }
        after = GETTS();
        delta(before, after);
    }

    bench("mrlockb", "- MRLOCK lock->unlock (all resources)\t\t\t");
    fflush(stdout);
    {
        mrlock_global_t l;
        uint32_t h;

        mrlock_init(&l);

        res_t rall = RES_INIT32(0xffffffff, 0xffffffff);
        before = GETTS();
        for (runs = 0; runs < RUNS; runs++) {
            h = mrlock_lock(&l, rall);
            mrlock_unlock(&l, h);
        }
        after = GETTS();
        delta(before, after);
    }
}

////////////////////////////////////////////////////////////////////////////////

struct shared_state {
    TIME_T (*func)(struct shared_state *l);
    volatile int thread_run;
    uint32_t released;
    unsigned long finished;
    uint32_t num_cpus;
    uint32_t cpu_spread;
    uint64_t cpu_bits;
    unsigned long cpu_mask;

    /* all locks use the same space (on the main thread's stack) */
    union {
        unsigned char aux_data[512];
        sspin_lock_t sspin;
        ticket_spin_t ticket;
        mcs_lock_t mcs;
        pft_lock_t pft;
        pfc_lock_t pfc;
        tatas_lock_t tatas;
        array1_lock_t array1;
        array2_lock_t array2;
        rnlp1_global_t rnlp1;
        rnlp2_global_t rnlp2;
        rnlp3_global_t rnlp3;
        rnlp4_global_t rnlp4;
        rwrnlp1_global_t rwrnlp1;
        rwrnlp2_global_t rwrnlp2;
        rwrnlp3_global_t rwrnlp3;
        sqdgl1_lock_t sqdgl1;
        sqdgl2_lock_t sqdgl2;
        rwsqdgl1_lock_t rwsqdgl1;
        rwsqdgl2_lock_t rwsqdgl2;
        llab5_lock_t llab5;
        llab6_lock_t llab6;
        rwllab1_lock_t rwllab1;
        rwllab2_lock_t rwllab2;
        ucrnlp1_global_t ucrnlp1;
        ucrnlp2_global_t ucrnlp2;
        mrlock_global_t mrlock;
    } __lock_aligned(LOCK_ALIGNMENT) l;
};


static void bench_thread(void *arg)
{
    struct shared_state *l = arg;

    while (atomic_load_acquire(&l->released) == 0) {
        /* idle hyperthread */
        atomic_relax();
    }

    while (l->thread_run) {
        l->func(l);
    }

    atomic_set_mask_release(&l->finished, 1ul << sys_cpu_get());

    sys_thread_exit();
}

static void bench_cpus_start(struct shared_state *l, TIME_T (*func)(struct shared_state *l))
{
    l->func = func;
    l->released = 0;
    l->finished = 0;
    l->thread_run = 1;

    for (unsigned int i = 1; i < l->num_cpus; i++) {
        create_thread_cpu(i, bench_thread, l, 100, i);
    }

    /* short break to let the threads on the other CPUs start */
    sys_sleep(sys_time_get() + 1000*1000);

    /* release other threads */
	atomic_store_release(&l->released, 1);
}

static void bench_cpus_stop(struct shared_state *l)
{
    assert(l->finished == 0);
    l->finished = 0x1ul;
    atomic_store_barrier();
    access_once(l->thread_run) = 0;

    do {
        /* short break to let the threads on the other CPUs stop */
        sys_sleep(sys_time_get() + 1000);
    } while (access_once(l->finished) != l->cpu_mask);

    for (unsigned int i = 1; i < l->num_cpus; i++) {
        join_thread(i);
    }
}

static inline void bench_resource_bits(res_t res, uint64_t bits)
{
#if __SIZEOF_LONG__ == 8
    res[0] = bits;
#else
    res[0] = (uint32_t)bits;
    res[1] = (uint32_t)(bits >> 32);
#endif
}

////////////////////////////////////////////////////////////////////////////////

static TIME_T single_sspin_tas(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        sspin_lock_tas(&l->l.sspin);
        sspin_unlock(&l->l.sspin);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_sspin_swap(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        sspin_lock_swap(&l->l.sspin);
        sspin_unlock(&l->l.sspin);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_sspin_cas(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        sspin_lock_cas(&l->l.sspin);
        sspin_unlock(&l->l.sspin);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_sspin_test_tas(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        sspin_lock_test_tas(&l->l.sspin);
        sspin_unlock(&l->l.sspin);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_sspin_test_swap(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        sspin_lock_test_swap(&l->l.sspin);
        sspin_unlock(&l->l.sspin);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_sspin_test_cas(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        sspin_lock_test_cas(&l->l.sspin);
        sspin_unlock(&l->l.sspin);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_ticket(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        ticket_spin_lock(&l->l.ticket);
        ticket_spin_unlock(&l->l.ticket);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rticket1(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        ticket_spin_lock(&l->l.ticket);
        __asm__ volatile ("" : : "r"(l->l.aux_data[0x44]));
        ticket_spin_unlock(&l->l.ticket);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rticket2(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        ticket_spin_lock(&l->l.ticket);
        __asm__ volatile ("" : : "r"(l->l.aux_data[0x44]));
        __asm__ volatile ("" : : "r"(l->l.aux_data[0x88]));
        ticket_spin_unlock(&l->l.ticket);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rticket3(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        ticket_spin_lock(&l->l.ticket);
        __asm__ volatile ("" : : "r"(l->l.aux_data[0x44]));
        __asm__ volatile ("" : : "r"(l->l.aux_data[0x88]));
        __asm__ volatile ("" : : "r"(l->l.aux_data[0xcc]));
        ticket_spin_unlock(&l->l.ticket);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rticket4(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        ticket_spin_lock(&l->l.ticket);
        __asm__ volatile ("" : : "r"(l->l.aux_data[0x44]));
        __asm__ volatile ("" : : "r"(l->l.aux_data[0x88]));
        __asm__ volatile ("" : : "r"(l->l.aux_data[0xcc]));
        __asm__ volatile ("" : : "r"(l->l.aux_data[0x100]));
        ticket_spin_unlock(&l->l.ticket);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_wticket1(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        ticket_spin_lock(&l->l.ticket);
        l->l.aux_data[0x44] = 1;
        ticket_spin_unlock(&l->l.ticket);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_wticket2(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        ticket_spin_lock(&l->l.ticket);
        l->l.aux_data[0x44] = 1;
        l->l.aux_data[0x88] = 2;
        ticket_spin_unlock(&l->l.ticket);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_wticket3(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        ticket_spin_lock(&l->l.ticket);
        l->l.aux_data[0x44] = 1;
        l->l.aux_data[0x88] = 2;
        l->l.aux_data[0xcc] = 3;
        ticket_spin_unlock(&l->l.ticket);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_wticket4(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        ticket_spin_lock(&l->l.ticket);
        l->l.aux_data[0x44] = 1;
        l->l.aux_data[0x88] = 2;
        l->l.aux_data[0xcc] = 3;
        l->l.aux_data[0x100] = 4;
        ticket_spin_unlock(&l->l.ticket);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rwticket1(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        ticket_spin_lock(&l->l.ticket);
        l->l.aux_data[0x44] += 1;
        ticket_spin_unlock(&l->l.ticket);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rwticket2(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        ticket_spin_lock(&l->l.ticket);
        l->l.aux_data[0x44] += 1;
        l->l.aux_data[0x88] += 2;
        ticket_spin_unlock(&l->l.ticket);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rwticket3(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        ticket_spin_lock(&l->l.ticket);
        l->l.aux_data[0x44] += 1;
        l->l.aux_data[0x88] += 2;
        l->l.aux_data[0xcc] += 3;
        ticket_spin_unlock(&l->l.ticket);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rwticket4(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        ticket_spin_lock(&l->l.ticket);
        l->l.aux_data[0x44] += 1;
        l->l.aux_data[0x88] += 2;
        l->l.aux_data[0xcc] += 3;
        l->l.aux_data[0x100] += 4;
        ticket_spin_unlock(&l->l.ticket);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_wdticket2(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned char x, y;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        ticket_spin_lock(&l->l.ticket);
        x = (l->l.aux_data[0x44] += 1);
        y = (l->l.aux_data[0x88] += x);
        __asm__ volatile ("" : : "r"(y));
        ticket_spin_unlock(&l->l.ticket);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_wdticket3(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned char x, y, z;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        ticket_spin_lock(&l->l.ticket);
        x = (l->l.aux_data[0x44] += 1);
        y = (l->l.aux_data[0x88] += x);
        z = (l->l.aux_data[0xcc] += y);
        __asm__ volatile ("" : : "r"(z));
        ticket_spin_unlock(&l->l.ticket);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_wdticket4(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned char x, y, z, zz;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        ticket_spin_lock(&l->l.ticket);
        x = (l->l.aux_data[0x44] += 1);
        y = (l->l.aux_data[0x88] += x);
        z = (l->l.aux_data[0xcc] += y);
        zz = (l->l.aux_data[0x100] += z);
        __asm__ volatile ("" : : "r"(zz));
        ticket_spin_unlock(&l->l.ticket);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_mcs(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    mcs_node_t lock_node;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        mcs_lock(&l->l.mcs, &lock_node);
        mcs_unlock(&l->l.mcs, &lock_node);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rmcs1(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    mcs_node_t lock_node;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        mcs_lock(&l->l.mcs, &lock_node);
        __asm__ volatile ("" : : "r"(l->l.aux_data[0x44]));
        mcs_unlock(&l->l.mcs, &lock_node);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rmcs2(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    mcs_node_t lock_node;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        mcs_lock(&l->l.mcs, &lock_node);
        __asm__ volatile ("" : : "r"(l->l.aux_data[0x44]));
        __asm__ volatile ("" : : "r"(l->l.aux_data[0x88]));
        mcs_unlock(&l->l.mcs, &lock_node);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rmcs3(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    mcs_node_t lock_node;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        mcs_lock(&l->l.mcs, &lock_node);
        __asm__ volatile ("" : : "r"(l->l.aux_data[0x44]));
        __asm__ volatile ("" : : "r"(l->l.aux_data[0x88]));
        __asm__ volatile ("" : : "r"(l->l.aux_data[0xcc]));
        mcs_unlock(&l->l.mcs, &lock_node);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rmcs4(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    mcs_node_t lock_node;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        mcs_lock(&l->l.mcs, &lock_node);
        __asm__ volatile ("" : : "r"(l->l.aux_data[0x44]));
        __asm__ volatile ("" : : "r"(l->l.aux_data[0x88]));
        __asm__ volatile ("" : : "r"(l->l.aux_data[0xcc]));
        __asm__ volatile ("" : : "r"(l->l.aux_data[0x100]));
        mcs_unlock(&l->l.mcs, &lock_node);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_wmcs1(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    mcs_node_t lock_node;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        mcs_lock(&l->l.mcs, &lock_node);
        l->l.aux_data[0x44] = 1;
        mcs_unlock(&l->l.mcs, &lock_node);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_wmcs2(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    mcs_node_t lock_node;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        mcs_lock(&l->l.mcs, &lock_node);
        l->l.aux_data[0x44] = 1;
        l->l.aux_data[0x88] = 2;
        mcs_unlock(&l->l.mcs, &lock_node);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_wmcs3(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    mcs_node_t lock_node;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        mcs_lock(&l->l.mcs, &lock_node);
        l->l.aux_data[0x44] = 1;
        l->l.aux_data[0x88] = 2;
        l->l.aux_data[0xcc] = 3;
        mcs_unlock(&l->l.mcs, &lock_node);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_wmcs4(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    mcs_node_t lock_node;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        mcs_lock(&l->l.mcs, &lock_node);
        l->l.aux_data[0x44] = 1;
        l->l.aux_data[0x88] = 2;
        l->l.aux_data[0xcc] = 3;
        l->l.aux_data[0x100] = 4;
        mcs_unlock(&l->l.mcs, &lock_node);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rwmcs1(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    mcs_node_t lock_node;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        mcs_lock(&l->l.mcs, &lock_node);
        l->l.aux_data[0x44] += 1;
        mcs_unlock(&l->l.mcs, &lock_node);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rwmcs2(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    mcs_node_t lock_node;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        mcs_lock(&l->l.mcs, &lock_node);
        l->l.aux_data[0x44] += 1;
        l->l.aux_data[0x88] += 2;
        mcs_unlock(&l->l.mcs, &lock_node);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rwmcs3(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    mcs_node_t lock_node;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        mcs_lock(&l->l.mcs, &lock_node);
        l->l.aux_data[0x44] += 1;
        l->l.aux_data[0x88] += 2;
        l->l.aux_data[0xcc] += 3;
        mcs_unlock(&l->l.mcs, &lock_node);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rwmcs4(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    mcs_node_t lock_node;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        mcs_lock(&l->l.mcs, &lock_node);
        l->l.aux_data[0x44] += 1;
        l->l.aux_data[0x88] += 2;
        l->l.aux_data[0xcc] += 3;
        l->l.aux_data[0x100] += 4;
        mcs_unlock(&l->l.mcs, &lock_node);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_wdmcs2(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    mcs_node_t lock_node;
    unsigned char x, y;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        mcs_lock(&l->l.mcs, &lock_node);
        x = (l->l.aux_data[0x44] += 1);
        y = (l->l.aux_data[0x88] += x);
        __asm__ volatile ("" : : "r"(y));
        mcs_unlock(&l->l.mcs, &lock_node);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_wdmcs3(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    mcs_node_t lock_node;
    unsigned char x, y, z;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        mcs_lock(&l->l.mcs, &lock_node);
        x = (l->l.aux_data[0x44] += 1);
        y = (l->l.aux_data[0x88] += x);
        z = (l->l.aux_data[0xcc] += y);
        __asm__ volatile ("" : : "r"(z));
        mcs_unlock(&l->l.mcs, &lock_node);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_wdmcs4(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    mcs_node_t lock_node;
    unsigned char x, y, z, zz;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        mcs_lock(&l->l.mcs, &lock_node);
        x = (l->l.aux_data[0x44] += 1);
        y = (l->l.aux_data[0x88] += x);
        z = (l->l.aux_data[0xcc] += y);
        zz = (l->l.aux_data[0x100] += z);
        __asm__ volatile ("" : : "r"(zz));
        mcs_unlock(&l->l.mcs, &lock_node);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_pft_r(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        pft_read_lock(&l->l.pft);
        pft_read_unlock(&l->l.pft);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_pft_w(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        pft_write_lock(&l->l.pft);
        pft_write_unlock(&l->l.pft);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_pfc_r(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        pfc_read_lock(&l->l.pfc);
        pfc_read_unlock(&l->l.pfc);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_pfc_w(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        pfc_write_lock(&l->l.pfc);
        pfc_write_unlock(&l->l.pfc);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_tatas(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    uint64_t r;
    r = l->cpu_bits << (sys_cpu_get() * l->cpu_spread);

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        tatas_lock(&l->l.tatas, r);
        tatas_unlock(&l->l.tatas, r);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_array1(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        array1_lock(&l->l.array1, r);
        array1_unlock(&l->l.array1, r);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_array2(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        array2_lock(&l->l.array2, r);
        array2_unlock(&l->l.array2, r);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rnlp1(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned int cpu_id = sys_cpu_get();

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        rnlp1_lock(&l->l.rnlp1, cpu_id, r);
        rnlp1_unlock(&l->l.rnlp1, cpu_id, r);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rnlp2(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned int cpu_id = sys_cpu_get();

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        rnlp2_lock(&l->l.rnlp2, cpu_id, r);
        rnlp2_unlock(&l->l.rnlp2, cpu_id, r);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rnlp3(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned int cpu_id = sys_cpu_get();

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        rnlp3_lock(&l->l.rnlp3, cpu_id, r);
        rnlp3_unlock(&l->l.rnlp3, cpu_id, r);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rnlp4(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned int cpu_id = sys_cpu_get();

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        rnlp4_lock(&l->l.rnlp4, cpu_id, r);
        rnlp4_unlock(&l->l.rnlp4, cpu_id, r);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rwrnlp1_reader(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned int cpu_id = sys_cpu_get();

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        rwrnlp1_read_lock(&l->l.rwrnlp1, cpu_id, r);
        rwrnlp1_read_unlock(&l->l.rwrnlp1, cpu_id, r);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rwrnlp1_writer(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned int cpu_id = sys_cpu_get();

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        rwrnlp1_write_lock(&l->l.rwrnlp1, cpu_id, r);
        rwrnlp1_write_unlock(&l->l.rwrnlp1, cpu_id, r);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rwrnlp2_reader(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned int cpu_id = sys_cpu_get();

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        rwrnlp2_read_lock(&l->l.rwrnlp2, cpu_id, r);
        rwrnlp2_read_unlock(&l->l.rwrnlp2, cpu_id, r);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rwrnlp2_writer(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned int cpu_id = sys_cpu_get();

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        rwrnlp2_write_lock(&l->l.rwrnlp2, cpu_id, r);
        rwrnlp2_write_unlock(&l->l.rwrnlp2, cpu_id, r);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rwrnlp3_reader(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned int cpu_id = sys_cpu_get();

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        rwrnlp3_read_lock(&l->l.rwrnlp3, cpu_id, r);
        rwrnlp3_read_unlock(&l->l.rwrnlp3, cpu_id, r);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rwrnlp3_writer(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned int cpu_id = sys_cpu_get();

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        rwrnlp3_write_lock(&l->l.rwrnlp3, cpu_id, r);
        rwrnlp3_write_unlock(&l->l.rwrnlp3, cpu_id, r);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_sqdgl1(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned int cpu_id = sys_cpu_get();

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        sqdgl1_lock(&l->l.sqdgl1, cpu_id, r);
        sqdgl1_unlock(&l->l.sqdgl1, cpu_id);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_sqdgl2(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned int cpu_id = sys_cpu_get();

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        sqdgl2_lock(&l->l.sqdgl2, cpu_id, r);
        sqdgl2_unlock(&l->l.sqdgl2, cpu_id);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rwsqdgl1(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned int cpu_id = sys_cpu_get();

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        rwsqdgl1_lock(&l->l.rwsqdgl1, cpu_id, r, r);
        rwsqdgl1_unlock(&l->l.rwsqdgl1, cpu_id, r, r);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rwsqdgl2(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned int cpu_id = sys_cpu_get();

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        rwsqdgl2_lock(&l->l.rwsqdgl2, cpu_id, r, r);
        rwsqdgl2_unlock(&l->l.rwsqdgl2, cpu_id, r, r);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_llab5(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned int cpu_id = sys_cpu_get();

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        llab5_lock(&l->l.llab5, cpu_id, r);
        llab5_unlock(&l->l.llab5, cpu_id);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_llab6(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned int cpu_id = sys_cpu_get();

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        llab6_lock(&l->l.llab6, cpu_id, r);
        llab6_unlock(&l->l.llab6, cpu_id);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rwllab1(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned int cpu_id = sys_cpu_get();

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        rwllab1_lock(&l->l.rwllab1, cpu_id, r, r);
        rwllab1_unlock(&l->l.rwllab1, cpu_id);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_rwllab2(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    unsigned int cpu_id = sys_cpu_get();

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        rwllab2_lock(&l->l.rwllab2, cpu_id, r, r);
        rwllab2_unlock(&l->l.rwllab2, cpu_id);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_ucrnlp1(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    uint32_t next;

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        next = ucrnlp1_lock(&l->l.ucrnlp1, r);
        ucrnlp1_unlock(&l->l.ucrnlp1, r, next);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_ucrnlp2(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    uint32_t next;

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        next = ucrnlp2_lock(&l->l.ucrnlp2, r);
        ucrnlp2_unlock(&l->l.ucrnlp2, r, next);
    }
    after = GETTS();
    return after - before;
}

static TIME_T single_mrlock(struct shared_state *l)
{
    TIME_T before, after;
    unsigned int runs;
    uint32_t h;

    res_t r;
    bench_resource_bits(r, l->cpu_bits << (sys_cpu_get() * l->cpu_spread));

    before = GETTS();
    for (runs = 0; runs < RUNS; runs++) {
        h = mrlock_lock(&l->l.mrlock, r);
        mrlock_unlock(&l->l.mrlock, h);
    }
    after = GETTS();
    return after - before;
}

static struct shared_state l;

static void bench_uncontended_parallel(unsigned int num_cpus, unsigned int resources)
{
    TIME_T after;

    printf("\n#Benching uncontended operations in parallel (%d CPUs, %d resources):\n\n",
           num_cpus, resources);

    l.num_cpus = num_cpus;
    l.cpu_spread = resources;
    l.cpu_bits = ((((1ull << ((resources) - 1)) -1) << 1) | 1ull);
    l.cpu_mask = ((((1ul << ((num_cpus) - 1)) -1) << 1) | 1ul);

    bench_series("uncontended_parallel", num_cpus, resources);

    bench("sspin_tas", "- spin TAS lock->unlock\t\t\t\t");
    fflush(stdout);
    sspin_init(&l.l.sspin);
    bench_cpus_start(&l, single_sspin_tas);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("sspin_swap", "- spin SWAP lock->unlock\t\t\t\t");
    fflush(stdout);
    sspin_init(&l.l.sspin);
    bench_cpus_start(&l, single_sspin_swap);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("sspin_cas", "- spin CAS lock->unlock\t\t\t\t");
    fflush(stdout);
    sspin_init(&l.l.sspin);
    bench_cpus_start(&l, single_sspin_cas);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("sspin_test_tas", "- spin test TAS lock->unlock\t\t\t\t");
    fflush(stdout);
    sspin_init(&l.l.sspin);
    bench_cpus_start(&l, single_sspin_test_tas);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("sspin_test_swap", "- spin test SWAP lock->unlock\t\t\t\t");
    fflush(stdout);
    sspin_init(&l.l.sspin);
    bench_cpus_start(&l, single_sspin_test_swap);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("sspin_test_cas", "- spin test CAS lock->unlock\t\t\t\t");
    fflush(stdout);
    sspin_init(&l.l.sspin);
    bench_cpus_start(&l, single_sspin_test_cas);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("ticket", "- TICKET lock->unlock\t\t\t\t\t");
    fflush(stdout);
    ticket_spin_init(&l.l.ticket);
    bench_cpus_start(&l, single_ticket);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rticket1", "- rticket1 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    ticket_spin_init(&l.l.ticket);
    bench_cpus_start(&l, single_rticket1);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rticket2", "- rticket2 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    ticket_spin_init(&l.l.ticket);
    bench_cpus_start(&l, single_rticket2);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rticket3", "- rticket3 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    ticket_spin_init(&l.l.ticket);
    bench_cpus_start(&l, single_rticket3);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rticket4", "- rticket4 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    ticket_spin_init(&l.l.ticket);
    bench_cpus_start(&l, single_rticket4);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("wticket1", "- wticket1 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    ticket_spin_init(&l.l.ticket);
    bench_cpus_start(&l, single_wticket1);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("wticket2", "- wticket2 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    ticket_spin_init(&l.l.ticket);
    bench_cpus_start(&l, single_wticket2);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("wticket3", "- wticket3 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    ticket_spin_init(&l.l.ticket);
    bench_cpus_start(&l, single_wticket3);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("wticket4", "- wticket4 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    ticket_spin_init(&l.l.ticket);
    bench_cpus_start(&l, single_wticket4);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rwticket1", "- rwticket1 lock->unlock\t\t\t\t");
    fflush(stdout);
    ticket_spin_init(&l.l.ticket);
    bench_cpus_start(&l, single_rwticket1);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rwticket2", "- rwticket2 lock->unlock\t\t\t\t");
    fflush(stdout);
    ticket_spin_init(&l.l.ticket);
    bench_cpus_start(&l, single_rwticket2);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rwticket3", "- rwticket3 lock->unlock\t\t\t\t");
    fflush(stdout);
    ticket_spin_init(&l.l.ticket);
    bench_cpus_start(&l, single_rwticket3);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rwticket4", "- rwticket4 lock->unlock\t\t\t\t");
    fflush(stdout);
    ticket_spin_init(&l.l.ticket);
    bench_cpus_start(&l, single_rwticket4);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("wdticket2", "- wdticket2 lock->unlock\t\t\t\t");
    fflush(stdout);
    ticket_spin_init(&l.l.ticket);
    bench_cpus_start(&l, single_wdticket2);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("wdticket3", "- wdticket3 lock->unlock\t\t\t\t");
    fflush(stdout);
    ticket_spin_init(&l.l.ticket);
    bench_cpus_start(&l, single_wdticket3);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("wdticket4", "- wdticket4 lock->unlock\t\t\t\t");
    fflush(stdout);
    ticket_spin_init(&l.l.ticket);
    bench_cpus_start(&l, single_wdticket4);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("mcs", "- MCS lock->unlock\t\t\t\t\t");
    fflush(stdout);
    mcs_init(&l.l.mcs);
    bench_cpus_start(&l, single_mcs);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rmcs1", "- rmcs1 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    mcs_init(&l.l.mcs);
    bench_cpus_start(&l, single_rmcs1);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rmcs2", "- rmcs2 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    mcs_init(&l.l.mcs);
    bench_cpus_start(&l, single_rmcs2);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rmcs3", "- rmcs3 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    mcs_init(&l.l.mcs);
    bench_cpus_start(&l, single_rmcs3);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rmcs4", "- rmcs4 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    mcs_init(&l.l.mcs);
    bench_cpus_start(&l, single_rmcs4);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("wmcs1", "- wmcs1 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    mcs_init(&l.l.mcs);
    bench_cpus_start(&l, single_wmcs1);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("wmcs2", "- wmcs2 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    mcs_init(&l.l.mcs);
    bench_cpus_start(&l, single_wmcs2);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("wmcs3", "- wmcs3 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    mcs_init(&l.l.mcs);
    bench_cpus_start(&l, single_wmcs3);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("wmcs4", "- wmcs4 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    mcs_init(&l.l.mcs);
    bench_cpus_start(&l, single_wmcs4);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rwmcs1", "- rwmcs1 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    mcs_init(&l.l.mcs);
    bench_cpus_start(&l, single_rwmcs1);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rwmcs2", "- rwmcs2 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    mcs_init(&l.l.mcs);
    bench_cpus_start(&l, single_rwmcs2);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rwmcs3", "- rwmcs3 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    mcs_init(&l.l.mcs);
    bench_cpus_start(&l, single_rwmcs3);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rwmcs4", "- rwmcs4 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    mcs_init(&l.l.mcs);
    bench_cpus_start(&l, single_rwmcs4);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("wdmcs2", "- wdmcs2 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    mcs_init(&l.l.mcs);
    bench_cpus_start(&l, single_wdmcs2);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("wdmcs3", "- wdmcs3 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    mcs_init(&l.l.mcs);
    bench_cpus_start(&l, single_wdmcs3);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("wdmcs4", "- wdmcs4 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    mcs_init(&l.l.mcs);
    bench_cpus_start(&l, single_wdmcs4);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("pftr", "- PFT read lock->unlock\t\t\t\t\t");
    fflush(stdout);
    pft_init(&l.l.pft);
    bench_cpus_start(&l, single_pft_r);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("pftw", "- PFT write lock->unlock\t\t\t\t");
    fflush(stdout);
    pft_init(&l.l.pft);
    bench_cpus_start(&l, single_pft_w);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("pfcr", "- PFC read lock->unlock\t\t\t\t\t");
    fflush(stdout);
    pfc_init(&l.l.pfc);
    bench_cpus_start(&l, single_pfc_r);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("pfcw", "- PFC write lock->unlock\t\t\t\t");
    fflush(stdout);
    pfc_init(&l.l.pfc);
    bench_cpus_start(&l, single_pfc_w);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("tatas", "- TATAS lock->unlock\t\t\t\t\t");
    fflush(stdout);
    tatas_init(&l.l.tatas);
    bench_cpus_start(&l, single_tatas);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("array1", "- array1 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    array1_init(&l.l.array1);
    bench_cpus_start(&l, single_array1);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("array2", "- array2 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    array2_init(&l.l.array2);
    bench_cpus_start(&l, single_array2);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rnlp1", "- RNLP1 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    rnlp1_init(&l.l.rnlp1);
    bench_cpus_start(&l, single_rnlp1);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rnlp2", "- RNLP2 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    rnlp2_init(&l.l.rnlp2);
    bench_cpus_start(&l, single_rnlp2);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rnlp3", "- RNLP3 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    rnlp3_init(&l.l.rnlp3);
    bench_cpus_start(&l, single_rnlp3);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rnlp4", "- RNLP4 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    rnlp4_init(&l.l.rnlp4);
    bench_cpus_start(&l, single_rnlp4);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rwrnlp1r", "- RW-RNLP1 read:lock->unlock\t\t\t\t");
    fflush(stdout);
    rwrnlp1_init(&l.l.rwrnlp1);
    bench_cpus_start(&l, single_rwrnlp1_reader);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rwrnlp1w", "- RW-RNLP1 write:lock->unlock\t\t\t\t");
    fflush(stdout);
    rwrnlp1_init(&l.l.rwrnlp1);
    bench_cpus_start(&l, single_rwrnlp1_writer);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rwrnlp2r", "- RW-RNLP2 read:lock->unlock\t\t\t\t");
    fflush(stdout);
    rwrnlp2_init(&l.l.rwrnlp2);
    bench_cpus_start(&l, single_rwrnlp2_reader);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rwrnlp2w", "- RW-RNLP2 write:lock->unlock\t\t\t\t");
    fflush(stdout);
    rwrnlp2_init(&l.l.rwrnlp2);
    bench_cpus_start(&l, single_rwrnlp2_writer);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rwrnlp3r", "- RW-RNLP2 read:lock->unlock\t\t\t\t");
    fflush(stdout);
    rwrnlp3_init(&l.l.rwrnlp3);
    bench_cpus_start(&l, single_rwrnlp3_reader);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rwrnlp3w", "- RW-RNLP2 write:lock->unlock\t\t\t\t");
    fflush(stdout);
    rwrnlp3_init(&l.l.rwrnlp3);
    bench_cpus_start(&l, single_rwrnlp3_writer);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("sqdgl1", "- sqdgl1 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    sqdgl1_init(&l.l.sqdgl1);
    bench_cpus_start(&l, single_sqdgl1);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("sqdgl2", "- sqdgl2 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    sqdgl2_init(&l.l.sqdgl2);
    bench_cpus_start(&l, single_sqdgl2);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rwsqdgl1", "- rwsqdgl1 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    rwsqdgl1_init(&l.l.rwsqdgl1);
    bench_cpus_start(&l, single_rwsqdgl1);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rwsqdgl2", "- rwsqdgl2 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    rwsqdgl2_init(&l.l.rwsqdgl2);
    bench_cpus_start(&l, single_rwsqdgl2);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("llab5", "- LLAB5 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    llab5_init(&l.l.llab5, num_cpus);
    bench_cpus_start(&l, single_llab5);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("llab6", "- LLAB6 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    llab6_init(&l.l.llab6, num_cpus);
    bench_cpus_start(&l, single_llab6);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rwllab1", "- RWLLAB1 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    rwllab1_init(&l.l.rwllab1, num_cpus);
    bench_cpus_start(&l, single_rwllab1);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("rwllab2", "- RWLLAB2 lock->unlock\t\t\t\t\t");
    fflush(stdout);
    rwllab2_init(&l.l.rwllab2, num_cpus);
    bench_cpus_start(&l, single_rwllab2);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("ucrnlp1", "- U-C-RNLP1 lock->unlock\t\t\t\t");
    fflush(stdout);
    ucrnlp1_init(&l.l.ucrnlp1);
    bench_cpus_start(&l, single_ucrnlp1);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("ucrnlp2", "- U-C-RNLP2 lock->unlock\t\t\t\t");
    fflush(stdout);
    ucrnlp2_init(&l.l.ucrnlp2);
    bench_cpus_start(&l, single_ucrnlp2);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);

    bench("mrlock", "- MRLOCK lock->unlock\t\t\t\t\t");
    fflush(stdout);
    mrlock_init(&l.l.mrlock);
    bench_cpus_start(&l, single_mrlock);
    after = l.func(&l);
    bench_cpus_stop(&l);
    delta(0, after);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void bench_mrlock(void)
{
    printf("###############################################################\n");
    printf("###      Benchmarking Multi-resource locking protocols      ###\n");
    printf("###############################################################\n");

    printf("\n### START: %d runs ###\n", RUNS);

#ifdef PRINT_SHORT
    _print_short = 1;
#endif

    /* some tests require an SMP system */
    unsigned int num_cpus = 0;
    for (unsigned long cpu = 0, cpu_mask = sys_cpu_mask();
         (cpu < sizeof(cpu_mask)*8) && ((cpu_mask & (1ul << cpu)) != 0);
         cpu++) {
        num_cpus++;
    }
    assert(num_cpus > 0);
    assert(num_cpus <= 64);

    /* primitive tests */
    spin_barrier_test();

    /* basic tests */
    sqdgl1_basic_test(num_cpus);
    sqdgl2_basic_test(num_cpus);
    rwsqdgl1_basic_test(num_cpus);
    rwsqdgl2_basic_test(num_cpus);
    tatas_basic_test(num_cpus);
    array1_basic_test(num_cpus);
    array2_basic_test(num_cpus);
    llab5_basic_test(num_cpus);
    llab6_basic_test(num_cpus);
    rwllab1_basic_test(num_cpus);
    rwllab2_basic_test(num_cpus);
    rnlp1_basic_test(num_cpus);
    rnlp2_basic_test(num_cpus);
    rnlp3_basic_test(num_cpus);
    rnlp4_basic_test(num_cpus);
    rwrnlp1_basic_test(num_cpus);
    rwrnlp2_basic_test(num_cpus);
    rwrnlp3_basic_test(num_cpus);
    ucrnlp1_basic_test(num_cpus);
    ucrnlp2_basic_test(num_cpus);
    mrlock_basic_test(num_cpus);

    /* uncontended benchmark */
    bench_uncontended_isolation(num_cpus);
    if (num_cpus > 20) {
        /* run benchmarks with only 1 resource */
        for (unsigned int c = 1; c <= num_cpus; c++) {
            bench_uncontended_parallel(c, 1);
        }
    } else {
        for (unsigned int c = 1; c <= num_cpus; c++) {
            for (unsigned int r = 1; r <= 16; r++) {
                if (c * r <= 64) {
                    bench_uncontended_parallel(c, r);
                }
            }
        }
    }

#ifdef RUN_ONCE
    printf("\n### END ###\n");
    sys_sleep(TIMEOUT_INFINITE);
#endif
}

////////////////////////////////////////////////////////////////////////////////

#ifdef LINUX_NATIVE
void bench_cpu(void);
int main(int argc, char *argv[])
{
    unsigned int limit_cpus = sizeof(long)*8;

    if (argc == 2) {
        limit_cpus = atoi(argv[1]);
    }

    marron_compat_init(limit_cpus);
    bench_cpu();
    bench_mrlock();
    return 0;
}
#endif
