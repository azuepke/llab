/* SPDX-License-Identifier: MIT */
/*
 * bench_mrlock_usecase.c
 *
 * Benchmark of generated Drone Scenario (for paper on multi-resource locks)
 *
 * azuepke, 2020-10-12: initial
 * azuepke, 2020-10-16: additional barrier synchronization for accurate timing
 * azuepke, 2020-10-18: cloned from other benchmark
 */

#ifdef LINUX_NATIVE
#include <stdint.h>
typedef uintptr_t addr_t;

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <atomic.h>
#include <compiler.h>
#include <assert.h>
#include "locks.h"
#include "bench.h"
#include <marron_compat.h>
#else
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <marron/api.h>
#include <atomic.h>
#include <arch_spin.h>
#include <compiler.h>
#include <assert.h>
#include <locks.h>
#include "app.h"
#include "bench.h"
#include "my_tls.h"
#endif


#pragma GCC diagnostic ignored "-Wdeclaration-after-statement"
//#pragma GCC diagnostic ignored "-Wunused-function"
//#pragma GCC diagnostic ignored "-Wunused-variable"

//#define RUN_ONCE

static unsigned int num_cpus;

////////////////////////////////////////////////////////////////////////////////

/// Xorshift32 using a global state variable (not actually atomic ...)
/// https://en.wikipedia.org/wiki/Xorshift

static uint32_t xorshift32_state __aligned(64);

static inline void random_init(uint32_t seed)
{
    assert(seed != 0);
    xorshift32_state = seed;
}

static inline uint32_t random(void)
{
    uint32_t val;

    val = xorshift32_state;
    val ^= val << 13;
    val ^= val >> 17;
    val ^= val << 5;
    xorshift32_state = val;

    return val;
}

////////////////////////////////////////////////////////////////////////////////

/* data model: a task comprises a set of different CS */

/* critical section (CS): resources and WCET */
#define NUM_CS 64
static struct cs {
	/* WCET of the CS in us */
	unsigned int wcet_us;
	/* bitmap of resources to write and read */
	uint32_t res_write;
	uint32_t res_read;
} csections[NUM_CS];

/* task: set of jobs + scheduling parameters */
#define NUM_TASK 4
static struct task {
	/* critical sections per task */
	struct cs *cs;
	unsigned int num_cs;
} tasks[NUM_TASK];

////////////////////////////////////////////////////////////////////////////////

static const unsigned int cs_distrib[16] = {
	1, 1, 1, 1, 2, 2, 2, 2, 4, 4, 4, 4, 6, 6, 7, 8,
};
static const unsigned int wcet_distrib[16] = {
	0, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 5, 7, 11, 13, 17,
};

static void generate_scenario(uint32_t seed)
{
	struct cs *cs;
	struct task *t;
	unsigned int cs_count = 0;

	random_init(seed);

	cs = &csections[cs_count];
	for (unsigned int i = 0; i < NUM_TASK; i++) {
		t = &tasks[i];
		t->cs = cs;
		t->num_cs = cs_distrib[random() & 0xf];
		for (unsigned int j = 0; j < t->num_cs; j++) {

			cs->wcet_us = wcet_distrib[random() & 0xf];
			cs->res_read = random() & random() & random();
			cs->res_write = random() & random() & random() & random();

			// FIXME: output garbled!?!?
			printf("#t:%d cs:%d wcet:%d rs:%08x ws:%08x\n", i, j, cs->wcet_us, cs->res_read, cs->res_write);

			cs++;
			assert(cs_count < NUM_CS);
			cs_count++;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

#define CALIBRATE_USECS (100*1000)

static unsigned long cycles_per_usec;

static void calibrate_usec(void)
{
	uint64_t t0, t1, t2;
	TIME_T before, after;

	printf("\n#calibrate TSC:");
	fflush(stdout);

	/* wait for first ticket increment */
	t0 = sys_time_get();
	do {
		t1 = sys_time_get();
	} while (t1 == t0);

	/* wait for at least 100ms */
	before = GETTS();
	do {
		t2 = sys_time_get();
	} while ((t2 - t1) < CALIBRATE_USECS*1000);
	after = GETTS();
	printf(" %lld nanoseconds takes about " FMT10 " cycles\n",
	       (unsigned long long)t2 - t1, after - before);

	cycles_per_usec = (after - before) / CALIBRATE_USECS;
}

/* sleep for given number of cycles */
static __noinline void csleep(unsigned long wait_cycles)
{
	TIME_T before, now;

	before = GETTS();
	do {
		now = GETTS();
	} while ((now - before) < wait_cycles);
}

static __noinline void usleep(unsigned long usec)
{
	csleep(usec * cycles_per_usec);
}

////////////////////////////////////////////////////////////////////////////////

struct shared_state {
	/* anonymous struct to keep the management data separated */
	struct {
		void (*do_cs)(struct shared_state *l, unsigned int cpu_id, const struct cs *cs);
		volatile int thread_run;
		volatile uint32_t released;
		uint32_t finished;
		uint64_t initial_release_time;
		spin_barrier_t sync_barrier;
		uint32_t seed;
	} __aligned(64);

	/* collected samples */
	struct samples {
#define NUM_SAMPLES 32
		TIME_T exec_time[NUM_SAMPLES];
	} __aligned(64) samples[countof(tasks)];

	/* all locks use the same space (on the main thread's stack) */
	union {
		ticket_spin_t ticket;
		mcs_lock_t mcs;
		tatas_lock_t tatas;
		rnlp2_global_t rnlp2;
		rnlp4_global_t rnlp4;
		ucrnlp2_global_t ucrnlp2;
		sqdgl2_lock_t sqdgl2;
		llab6_lock_t llab6;
		mrlock_global_t mrlock;

		//rwrnlp_global_t rwrnlp;
		rwsqdgl2_lock_t rwsqdgl2;
		rwllab2_lock_t rwllab2;
	} l;
};

static void do_cs_nolock(struct shared_state *l, unsigned int cpu_id, const struct cs *cs)
{
	/* simply wait */
	(void)l;
	(void)cpu_id;
	usleep(cs->wcet_us);
}

static void do_cs_ticket(struct shared_state *l, unsigned int cpu_id, const struct cs *cs)
{
	uint64_t res;

	(void)cpu_id;

	res = cs->res_write | cs->res_read;

	if (res != 0) {
		ticket_spin_lock(&l->l.ticket);
	}

	usleep(cs->wcet_us);

	if (res != 0) {
		ticket_spin_unlock(&l->l.ticket);
	}
}

static void do_cs_mcs(struct shared_state *l, unsigned int cpu_id, const struct cs *cs)
{
    mcs_node_t lock_node;
	uint64_t res;

	(void)cpu_id;

	res = cs->res_write | cs->res_read;

	if (res != 0) {
		mcs_lock(&l->l.mcs, &lock_node);
	}

	usleep(cs->wcet_us);

	if (res != 0) {
		mcs_unlock(&l->l.mcs, &lock_node);
	}
}

static void do_cs_tatas(struct shared_state *l, unsigned int cpu_id, const struct cs *cs)
{
	uint64_t res;

	(void)cpu_id;

	res = cs->res_write | cs->res_read;

	if (res != 0) {
		tatas_lock(&l->l.tatas, res);
	}

	usleep(cs->wcet_us);

	if (res != 0) {
		tatas_unlock(&l->l.tatas, res);
	}
}

static void do_cs_rnlp2(struct shared_state *l, unsigned int cpu_id, const struct cs *cs)
{
	res_t res = {0};

	res[0] = cs->res_write | cs->res_read;

	if (res[0] != 0) {
		rnlp2_lock(&l->l.rnlp2, cpu_id, res);
	}

	usleep(cs->wcet_us);

	if (res[0] != 0) {
		rnlp2_unlock(&l->l.rnlp2, cpu_id, res);
	}
}

static void do_cs_rnlp4(struct shared_state *l, unsigned int cpu_id, const struct cs *cs)
{
	res_t res = {0};

	res[0] = cs->res_write | cs->res_read;

	if (res[0] != 0) {
		rnlp4_lock(&l->l.rnlp4, cpu_id, res);
	}

	usleep(cs->wcet_us);

	if (res[0] != 0) {
		rnlp4_unlock(&l->l.rnlp4, cpu_id, res);
	}
}

static void do_cs_ucrnlp2(struct shared_state *l, unsigned int cpu_id, const struct cs *cs)
{
	res_t res = {0};
	uint32_t h = 0;
	(void)cpu_id;

	res[0] = cs->res_write | cs->res_read;

	if (res[0] != 0) {
		h = ucrnlp2_lock(&l->l.ucrnlp2, res);
	}

	usleep(cs->wcet_us);

	if (res[0] != 0) {
		ucrnlp2_unlock(&l->l.ucrnlp2, res, h);
	}
}

static void do_cs_sqdgl2(struct shared_state *l, unsigned int cpu_id, const struct cs *cs)
{
	res_t res = {0};

	res[0] = cs->res_write | cs->res_read;

	if (res[0] != 0) {
		sqdgl2_lock(&l->l.sqdgl2, cpu_id, res);
	}

	usleep(cs->wcet_us);

	if (res[0] != 0) {
		sqdgl2_unlock(&l->l.sqdgl2, cpu_id);
	}
}

static void do_cs_llab6(struct shared_state *l, unsigned int cpu_id, const struct cs *cs)
{
	res_t res = {0};

	res[0] = cs->res_write | cs->res_read;

	if (res[0] != 0) {
		llab6_lock(&l->l.llab6, cpu_id, res);
	}

	usleep(cs->wcet_us);

	if (res[0] != 0) {
		llab6_unlock(&l->l.llab6, cpu_id);
	}
}

static void do_cs_mrlock(struct shared_state *l, unsigned int cpu_id, const struct cs *cs)
{
	res_t res = {0};
	uint32_t h = 0;
	(void)cpu_id;

	res[0] = cs->res_write | cs->res_read;

	if (res[0] != 0) {
		h = mrlock_lock(&l->l.mrlock, res);
	}

	usleep(cs->wcet_us);

	if (res[0] != 0) {
		mrlock_unlock(&l->l.mrlock, h);
	}
}

static void do_cs_rwsqdgl2(struct shared_state *l, unsigned int cpu_id, const struct cs *cs)
{
	res_t res_r = {0};
	res_t res_w = {0};

	res_r[0] = cs->res_read;
	res_w[0] = cs->res_write;

	if ((res_r[0] | res_w[0]) != 0) {
		rwsqdgl2_lock(&l->l.rwsqdgl2, cpu_id, res_r, res_w);
	}

	usleep(cs->wcet_us);

	if ((res_r[0] | res_w[0]) != 0) {
		rwsqdgl2_unlock(&l->l.rwsqdgl2, cpu_id, res_r, res_w);
	}
}

static void do_cs_rwllab2(struct shared_state *l, unsigned int cpu_id, const struct cs *cs)
{
	res_t res_r = {0};
	res_t res_w = {0};

	res_r[0] = cs->res_read;
	res_w[0] = cs->res_write;

	if ((res_r[0] | res_w[0]) != 0) {
		rwllab2_lock(&l->l.rwllab2, cpu_id, res_r, res_w);
	}

	usleep(cs->wcet_us);

	if ((res_r[0] | res_w[0]) != 0) {
		rwllab2_unlock(&l->l.rwllab2, cpu_id);
	}
}

////////////////////////////////////////////////////////////////////////////////

// overall execution time
#define BENCH_RUN_TIME 20	/* ms */

/* iterate jobs and their critical sections */
static void run_task(void *_l)
{
	struct shared_state *l = _l;
	struct samples *s;
	unsigned int task_id = sys_thread_self() - 1;
	unsigned int cpu_id = sys_cpu_get();
	const struct task *task;
	unsigned int sample;
	uint64_t release_time;
	TIME_T before, after, delta;

	assert(task_id < countof(tasks));
	task = &tasks[task_id];

	s = &l->samples[task_id];

	while (atomic_load_acquire(&l->released) == 0) {
		/* idle hyperthread */
		atomic_relax();
	}

	release_time = l->initial_release_time;

	sample = 0;
	for (unsigned int steps = 0; steps < BENCH_RUN_TIME; steps++) {
			/* first bring high priority tasks in sync */
			sys_sleep(release_time);

			spin_barrier_wait(&l->sync_barrier, cpu_id);

			/* then add release jitter of up to 1/10 of an us (0x3f/600 MHz) */
			csleep(random() & 0x3f);

			before = GETTS();

			for (unsigned int i = 0; i < task->num_cs; i++) {
				const struct cs *cs = &task->cs[i];
				l->do_cs(l, cpu_id, cs);
			}

			after = GETTS();
			delta = after - before;

			if (sample < NUM_SAMPLES) {
				s->exec_time[sample] = delta;
				sample++;
			}

			release_time += 1000*1000;
	}

	atomic_add_release(&l->finished, 1);

	sys_thread_exit();
}

static void start_tasks(struct shared_state *l)
{
	l->released = 0;
	l->finished = 0;

	memset(l->samples, 0, sizeof(l->samples));
	atomic_release_barrier();

	for (unsigned int i = 0; i < countof(tasks); i++) {
		create_thread_cpu(i + 1, run_task, l, 100, i);
	}

	l->initial_release_time = sys_time_get() + 2*1000*1000;	/* 2ms */
	spin_barrier_init(&l->sync_barrier, num_cpus);

	/* release other threads */
	atomic_store_release(&l->released, 1);
}

static void stop_tasks(struct shared_state *l)
{
	//assert(l->finished == 0);

	do {
		/* short break to let the threads on the other CPUs stop */
		sys_sleep(sys_time_get() + 1*1000*1000);	/* 1ms */
	} while (access_once(l->finished) < countof(tasks));


	for (unsigned int i = 0; i < countof(tasks); i++) {
		join_thread(i + 1);
	}
}

static void print_tasks(struct shared_state *l, const char *lockname)
{
	unsigned int wcet_us_all;
	TIME_T exec_time_all;

	assert(l->finished == countof(tasks));

	//printf("* execution times:\n");
	wcet_us_all = 0;
	exec_time_all = 0;
	for (unsigned int i = 0; i < countof(tasks); i++) {
		const struct task *task = &tasks[i];

		for (unsigned int c = 0; c < task->num_cs; c++) {
			const struct cs *cs = &task->cs[c];

			wcet_us_all += cs->wcet_us;
		}

		// average
		const struct samples *s = &l->samples[i];
		for (unsigned int sample = 0; sample < NUM_SAMPLES; sample++) {
			exec_time_all += s->exec_time[sample];
		}
	}

	printf("drone2_%s,%d," FMT0 ",%ld\n", lockname, l->seed, exec_time_all,
	       wcet_us_all * cycles_per_usec * BENCH_RUN_TIME);
}

////////////////////////////////////////////////////////////////////////////////

static void drone_run(struct shared_state *l, uint32_t seed)
{
	printf("\n# *** seed: %d ***\n", seed);
	l->seed = seed;
	generate_scenario(seed);

	// FIXME: dummy print to ungarble output
	printf("\n");

	l->do_cs = do_cs_nolock;
	start_tasks(l);
	sys_sleep(sys_time_get() + (BENCH_RUN_TIME + 10)*1000*1000);
	stop_tasks(l);
	print_tasks(l, "nolock");

	ticket_spin_init(&l->l.ticket);
	l->do_cs = do_cs_ticket;
	start_tasks(l);
	sys_sleep(sys_time_get() + (BENCH_RUN_TIME + 10)*1000*1000);
	stop_tasks(l);
	print_tasks(l, "ticket");

	mcs_init(&l->l.mcs);
	l->do_cs = do_cs_mcs;
	start_tasks(l);
	sys_sleep(sys_time_get() + (BENCH_RUN_TIME + 10)*1000*1000);
	stop_tasks(l);
	print_tasks(l, "mcs");

	tatas_init(&l->l.tatas);
	l->do_cs = do_cs_tatas;
	start_tasks(l);
	sys_sleep(sys_time_get() + (BENCH_RUN_TIME + 10)*1000*1000);
	stop_tasks(l);
	print_tasks(l, "tatas");

	rnlp2_init(&l->l.rnlp2);
	l->do_cs = do_cs_rnlp2;
	start_tasks(l);
	sys_sleep(sys_time_get() + (BENCH_RUN_TIME + 10)*1000*1000);
	stop_tasks(l);
	print_tasks(l, "rnlp2");

	rnlp4_init(&l->l.rnlp4);
	l->do_cs = do_cs_rnlp4;
	start_tasks(l);
	sys_sleep(sys_time_get() + (BENCH_RUN_TIME + 10)*1000*1000);
	stop_tasks(l);
	print_tasks(l, "rnlp4");

	ucrnlp2_init(&l->l.ucrnlp2);
	l->do_cs = do_cs_ucrnlp2;
	start_tasks(l);
	sys_sleep(sys_time_get() + (BENCH_RUN_TIME + 10)*1000*1000);
	stop_tasks(l);
	print_tasks(l, "ucrnlp2");

	sqdgl2_init(&l->l.sqdgl2);
	l->do_cs = do_cs_sqdgl2;
	start_tasks(l);
	sys_sleep(sys_time_get() + (BENCH_RUN_TIME + 10)*1000*1000);
	stop_tasks(l);
	print_tasks(l, "sqdgl2");

	llab6_init(&l->l.llab6, 4);
	l->do_cs = do_cs_llab6;
	start_tasks(l);
	sys_sleep(sys_time_get() + (BENCH_RUN_TIME + 10)*1000*1000);
	stop_tasks(l);
	print_tasks(l, "llab6");

	mrlock_init(&l->l.mrlock);
	l->do_cs = do_cs_mrlock;
	start_tasks(l);
	sys_sleep(sys_time_get() + (BENCH_RUN_TIME + 10)*1000*1000);
	stop_tasks(l);
	print_tasks(l, "mrlock");

	rwsqdgl2_init(&l->l.rwsqdgl2);
	l->do_cs = do_cs_rwsqdgl2;
	start_tasks(l);
	sys_sleep(sys_time_get() + (BENCH_RUN_TIME + 10)*1000*1000);
	stop_tasks(l);
	print_tasks(l, "rwsqdgl2");

	rwllab2_init(&l->l.rwllab2, 4);
	l->do_cs = do_cs_rwllab2;
	start_tasks(l);
	sys_sleep(sys_time_get() + (BENCH_RUN_TIME + 10)*1000*1000);
	stop_tasks(l);
	print_tasks(l, "rwllab2");
}

////////////////////////////////////////////////////////////////////////////////

static struct shared_state _l;

void bench_mrlock_usecase(void)
{
	/* some tests require an SMP system */
	num_cpus = 0;
	for (unsigned long cpu = 0, cpu_mask = sys_cpu_mask();
			(cpu < sizeof(cpu_mask)*8) && ((cpu_mask & (1ul << cpu)) != 0);
			cpu++) {
		num_cpus++;
	}
	assert(num_cpus > 0);
	assert(num_cpus <= 64);

	printf("### drone scenario START\n");

	if (num_cpus < 4) {
		printf("skipping drone scenario, need at least 4 CPUs\n");
		return;
	}

	calibrate_usec();

	random_init(GETTS());
	printf("#testing random: %d\n", random());

	for (unsigned int seed = 1; seed <= 32; seed++) {
		drone_run(&_l, seed);
	}

	printf("### drone scenario DONE\n");

#ifdef RUN_ONCE
	printf("\n### END ###\n");
	sys_sleep(TIMEOUT_INFINITE);
#endif
}

////////////////////////////////////////////////////////////////////////////////

#ifdef LINUX_NATIVE
int main(int argc, char *argv[])
{
    unsigned int limit_cpus = sizeof(long)*8;

    if (argc == 2) {
        limit_cpus = atoi(argv[1]);
    }

    marron_compat_init(limit_cpus);
    bench_mrlock_usecase();
    return 0;
}
#endif
